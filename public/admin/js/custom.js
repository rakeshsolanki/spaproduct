var baseurl = $('#baseurl').val();
function userinactive(userid){
    $.ajax({
        type: "POST",
        url:  baseurl + "admin/users/userIsactive",
        data:{status:0,userid:userid},
        success: function(data){
            if(data)
            {
                $('#inactive_'+userid).removeClass('d-none');
                $('#active_'+userid).addClass('d-none');
            }
        }
    });
}

function useractive(userid){
    $.ajax({
        type: "POST",
        url: baseurl+ "admin/users/userIsactive",
        data:{status:1,userid:userid},
        success: function(data){
            if(data)
            {
                $('#active_'+userid).removeClass('d-none');
                $('#inactive_'+userid).addClass('d-none');
            }
        }
    });
}

function deletegeneraldata(expid) {
    var r = confirm("Are you sure delete this data!");
    if (r == true) {
      $.ajax({
          type: "POST",
          url: baseurl + "expense/general/delete",
          data:{expid:expid},
          success: function(data){
              if(data)
              {
                  $('#lnr_'+expid).hide();
              }
          }
      });
    } 
}

function deleteinvoicedata(expid) {
    var r = confirm("Are you sure delete this data!");
    if (r == true) {
      $.ajax({
          type: "POST",
          url: "<?php echo base_url('invoice/delete') ?>",
          data:{expid:expid},
          success: function(response){
              if(response==1)
              {
                  $('#lnr_'+expid).hide();
              }
          }
      });
    } 
  }

function deleteUserdata(userid) {
    var r = confirm("Are you sure delete this data!");
    if (r == true) {
      $.ajax({
          type: "POST",
          url: baseurl +"users/delete",
          data:{userid:userid},
          success: function(data){
              if(data)
              {
                  $('#lnr_'+userid).hide();
              }
          }
      });
    } 
}

function deleteloundrydata(expid) {
    var r = confirm("Are you sure delete this data!");
    if (r == true) {
      $.ajax({
          type: "POST",
          url: baseurl+ "expense/laundry/delete",
          data:{expid:expid},
          success: function(data){
              if(data)
              {
                  $('#lnr_'+expid).hide();
              }
          }
      });
    } 
  }

function deletecustomerdata(customerid) {
    var r = confirm("Are you sure delete this data!");
    if (r == true) {
      $.ajax({
          type: "POST",
          url: baseurl + "customers/delete",
          data:{customerid:customerid},
          success: function(data){
              if(data)
              {
                  $('#lnr_'+customerid).hide();
              }
          }
      });
    } 
  }

  function custActive(userid){
    $.ajax({
        type: "POST",
        url: baseurl + "users/userSmsPermision",
        data:{status:1,userid:userid},
        success: function(data){
            if(data)
            {
                $('#active_'+userid).removeClass('d-none');
                $('#inactive_'+userid).addClass('d-none');
            }
        }
    });
}

function custInactive(userid){
    $.ajax({
        type: "POST",
        url: baseurl+  "users/userSmsPermision",
        data:{status:0,userid:userid},
        success: function(data){
            if(data)
            {
                $('#inactive_'+userid).removeClass('d-none');
                $('#active_'+userid).addClass('d-none');
            }
        }
    });
}

;
(function ($, window, document, undefined) {
    "use strict";
    $('document').ready(function () {
        
        if (window.File && window.FileList && window.FileReader) {
            $("#upload").on("change", function(e) {
                var files = e.target.files,
                    filesLength = files.length;
                for (var i = 0; i < filesLength; i++) {
                    var f = files[i]
                    var fileReader = new FileReader();
                    fileReader.onload = (function(e) {
                        var file = e.target;
                        var menuName = '';
                        var newHtml = '<div class="pimg col-md-2 text-center ">' +
                            '<div class="" style="background: url(' + e.target.result + ');  background-position: center;background-size: contain;background-repeat: no-repeat;height: 150px;"></div>' +
                            '<span class="removeportfolio btn btn-danger btn-sm"> Remove </span>' +
                            '</div>';
                        $('#portfolioSection').html(newHtml);

                        $(".removeportfolio").click(function() {
                            $(this).parent(".pimg").remove();
                        }); 
                    });
                    fileReader.readAsDataURL(f);
                }
            });
            $(".removeportfolio").click(function() {
                $(this).parent(".pimg").remove();
            });
        } else {
            alert("Your browser doesn't support to File API")
        }
 
        $('.datePicker').datepicker({
            format: 'd-m-yyyy',
        });

    });
})(jQuery, window, document);      