-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `tblcustomer`;
CREATE TABLE `tblcustomer` (
  `customerID` int(11) NOT NULL AUTO_INCREMENT,
  `membershipID` varchar(50) DEFAULT NULL,
  `managerID` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `gender` enum('Male','Female','Other') NOT NULL DEFAULT 'Male',
  `dob` date DEFAULT NULL,
  `address` text DEFAULT NULL,
  `validity` int(11) DEFAULT NULL COMMENT 'Month',
  `totalTreatment` int(11) NOT NULL DEFAULT 0,
  `remainingTreatment` int(11) NOT NULL DEFAULT 0,
  `yslip` varchar(50) DEFAULT NULL,
  `paymentMode` varchar(100) NOT NULL DEFAULT 'Other',
  `amount` decimal(10,2) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `membershipDate` varchar(20) DEFAULT NULL,
  `expireDate` varchar(20) DEFAULT NULL,
  `msgStatus` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0=>No, 1=>Yes',
  `isActive` tinyint(2) NOT NULL DEFAULT 1 COMMENT '0=>Inactive, 1=>Active',
  `isDelete` tinyint(2) NOT NULL DEFAULT 0 COMMENT '0=>No, 1=>Yes',
  `createdTime` datetime DEFAULT NULL,
  `updatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`customerID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `tblemployee`;
CREATE TABLE `tblemployee` (
  `employeeID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `altMobile` varchar(20) DEFAULT NULL,
  `gender` enum('Male','Female') NOT NULL DEFAULT 'Female',
  `dob` date DEFAULT NULL,
  `address` text DEFAULT NULL,
  `specialist` text DEFAULT NULL,
  `joinDate` date DEFAULT NULL,
  `isActive` tinyint(2) NOT NULL DEFAULT 1 COMMENT '0=>Inactive, 1=>Active',
  `isDelete` tinyint(2) NOT NULL DEFAULT 0 COMMENT '0=>No, 1=>Yes',
  `createdTime` datetime DEFAULT NULL,
  `updatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`employeeID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `tblemployee_salary`;
CREATE TABLE `tblemployee_salary` (
  `salID` int(11) NOT NULL AUTO_INCREMENT,
  `managerID` int(11) DEFAULT NULL,
  `employeeID` int(11) DEFAULT NULL,
  `payDate` varchar(25) DEFAULT NULL,
  `voucher` varchar(100) DEFAULT NULL,
  `salaryMonth` varchar(20) DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `desc` text DEFAULT NULL,
  `isActive` tinyint(2) NOT NULL DEFAULT 1 COMMENT '0=>Inactive, 1=>Active',
  `isDelete` tinyint(2) NOT NULL DEFAULT 0 COMMENT '0=>No, 1=>Yes',
  `createdTime` datetime DEFAULT NULL,
  `updatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`salID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `tblexpense_value`;
CREATE TABLE `tblexpense_value` (
  `expID` int(11) NOT NULL AUTO_INCREMENT,
  `managerID` int(11) DEFAULT NULL,
  `tea` int(11) NOT NULL DEFAULT 0,
  `waterJug` int(11) NOT NULL DEFAULT 0,
  `towel` int(11) NOT NULL DEFAULT 0,
  `napkin` int(11) NOT NULL DEFAULT 0,
  `dress` int(11) NOT NULL DEFAULT 0,
  `bedsheet` int(11) NOT NULL DEFAULT 0,
  `footmate` int(11) NOT NULL DEFAULT 0,
  `isActive` tinyint(2) NOT NULL DEFAULT 1 COMMENT '0=>Inactive, 1=>Active',
  `isDelete` tinyint(2) NOT NULL DEFAULT 0 COMMENT '0=>No, 1=>Yes',
  `createdTime` datetime DEFAULT NULL,
  `updatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`expID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `tblexpense_value` (`expID`, `managerID`, `tea`, `waterJug`, `towel`, `napkin`, `dress`, `bedsheet`, `footmate`, `isActive`, `isDelete`, `createdTime`, `updatedTime`) VALUES
(1,	1,	5,	20,	15,	5,	10,	30,	15,	0,	0,	NULL,	'2019-12-31 11:39:56');

DROP TABLE IF EXISTS `tblexp_type`;
CREATE TABLE `tblexp_type` (
  `typeID` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL DEFAULT '0',
  `isActive` tinyint(2) NOT NULL DEFAULT 1 COMMENT '0=>Inactive, 1=>Active',
  `isDelete` tinyint(2) NOT NULL DEFAULT 0 COMMENT '0=>No, 1=>Yes',
  `createdTime` datetime DEFAULT NULL,
  `updatedTime` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`typeID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `tblexp_type` (`typeID`, `type`, `isActive`, `isDelete`, `createdTime`, `updatedTime`) VALUES
(1,	'OFFICE RENT',	1,	0,	'2019-12-25 16:01:38',	'2019-12-25 16:01:38'),
(2,	'MAINTENANCE',	1,	0,	'2019-12-25 16:01:50',	'2019-12-25 16:01:50'),
(3,	'ELECTRICITY BILL',	1,	0,	'2019-12-25 16:01:56',	'2019-12-25 16:01:56'),
(4,	'NEWS PAPER BILL',	1,	0,	'2019-12-25 16:02:17',	'2019-12-25 16:02:17'),
(5,	'MOBILE PHONE BILL',	1,	0,	'2019-12-25 16:02:17',	'2019-12-25 16:02:17'),
(6,	'TEA/WATER BILL',	1,	0,	'2019-12-25 16:02:26',	'2019-12-25 16:02:26'),
(7,	'LAUNDRY BILL',	1,	0,	'2019-12-25 16:02:35',	'2019-12-25 16:02:35'),
(8,	'FLOWER\'S BILL',	1,	0,	'2019-12-25 16:02:43',	'2019-12-25 16:02:43'),
(9,	'TV-INTERNET',	1,	0,	'2019-12-25 16:02:52',	'2019-12-25 16:02:52'),
(10,	'STATIONARY',	1,	0,	'2019-12-25 16:03:05',	'2019-12-25 16:03:05'),
(11,	'SPA PRODUCT',	1,	0,	'2019-12-25 16:03:15',	'2019-12-25 16:03:15'),
(12,	'COSMETIC PRODUCT',	1,	0,	'2019-12-25 16:03:27',	'2019-12-25 16:03:27'),
(13,	'OTHER',	1,	0,	'2019-12-25 16:03:36',	'2019-12-25 16:03:36');

DROP TABLE IF EXISTS `tblgeneral_expense`;
CREATE TABLE `tblgeneral_expense` (
  `genExpID` int(11) NOT NULL AUTO_INCREMENT,
  `managerID` int(11) DEFAULT NULL,
  `mtea` int(11) NOT NULL DEFAULT 0,
  `etea` int(11) NOT NULL DEFAULT 0,
  `waterJug` int(11) NOT NULL DEFAULT 0,
  `total` decimal(10,2) NOT NULL DEFAULT 0.00,
  `date` varchar(20) DEFAULT NULL,
  `isActive` tinyint(2) NOT NULL DEFAULT 1 COMMENT '0=>Inactive, 1=>Active',
  `isDelete` tinyint(2) NOT NULL DEFAULT 0 COMMENT '0=>No, 1=>Yes',
  `createdTime` datetime DEFAULT NULL,
  `updatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`genExpID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `tblinvoice`;
CREATE TABLE `tblinvoice` (
  `invoiceID` int(11) NOT NULL AUTO_INCREMENT,
  `managerID` int(11) DEFAULT NULL,
  `membershipID` varchar(50) DEFAULT NULL,
  `customerName` varchar(255) DEFAULT NULL,
  `customerMobile` varchar(20) DEFAULT NULL,
  `payMode` varchar(255) NOT NULL DEFAULT 'Other',
  `amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `yslip` varchar(100) DEFAULT NULL,
  `invoiceDate` varchar(25) DEFAULT NULL,
  `duration` varchar(25) DEFAULT NULL,
  `inTime` varchar(20) DEFAULT NULL,
  `outTime` varchar(20) DEFAULT NULL,
  `block` varchar(100) DEFAULT NULL,
  `treatmentID` int(11) DEFAULT NULL,
  `product` varchar(255) DEFAULT NULL,
  `employeeID` varchar(100) DEFAULT NULL,
  `isActive` tinyint(2) NOT NULL DEFAULT 1 COMMENT '0=>Inactive, 1=>Active',
  `isDelete` tinyint(2) NOT NULL DEFAULT 0 COMMENT '0=>No, 1=>Yes',
  `createdTime` datetime DEFAULT NULL,
  `updatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`invoiceID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `tbllanguage`;
CREATE TABLE `tbllanguage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_key` varchar(255) DEFAULT NULL,
  `language_value` text DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT 1 COMMENT '0=>Inactive, 1=>Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbllanguage` (`id`, `language_key`, `language_value`, `status`) VALUES
(1,	'invalidLogin',	'Invalid Email or Password!',	1),
(2,	'pwdChangeSuc',	'Password has been changed successfully!',	1),
(3,	'recAddSuc',	'Record is Added Successfully!',	1),
(4,	'recUpSuc',	'Record is Updated Successfully!',	1),
(5,	'recDelSuc',	'Record is Deleted Successfully!',	1),
(6,	'recNotFound',	'Record not found',	1),
(7,	'passMissingParam',	'Please enter Missing parameter.',	1),
(8,	'passEmail',	'Please enter Email.',	1),
(9,	'passFname',	'Please enter Name.',	1),
(10,	'passFamilyName',	'Please enter Family Name.',	1),
(12,	'passLangType',	'Please pass Language type.',	1),
(13,	'passMobile',	'Please enter Mobile Number.',	1),
(14,	'passPwd',	'Please enter password.',	1),
(16,	'passDeviceType',	'Please pass device type.',	1),
(17,	'passDeviceToken',	'Please pass device token.',	1),
(19,	'emailExist',	'This Email Already Exist',	1),
(20,	'regSuccess',	'User register Successfully.',	1),
(22,	'regFail',	'Fail to register User.',	1),
(23,	'userNotFound',	'User not found',	1),
(24,	'loginSuccess',	'Member Login Successfully!',	1),
(25,	'passUserId',	'Please pass User ID.',	1),
(26,	'passToken',	'Please pass Access Token.',	1),
(27,	'passUsername',	'Please enter Username.',	1),
(28,	'usernameExist',	'Username already exist.',	1),
(29,	'profileUpdateSuccess',	'Profile update Successfully.',	1),
(30,	'profileUpdateFail',	'Fail to update Profile.',	1),
(31,	'passwordNotMatch',	'Password not match, please enter Correct password.',	1),
(32,	'passOldPwd',	'Please enter old password.',	1),
(33,	'passNewPwd',	'Please enter new password.',	1),
(34,	'pwdChangeSuccess',	'Change Password Successfully!',	1),
(35,	'logoutSuccess',	'Logout Successfully!',	1),
(39,	'fullname',	'Please Pass full name',	1),
(40,	'address_1',	'Please Fill address line 1',	1),
(42,	'city',	'Please fill city name',	1),
(45,	'country',	'Select Country',	1),
(46,	'mobileExist',	'Mobile already exist.',	1),
(47,	'correctReferralCode',	'Please enter Correct Referral Code.',	1),
(48,	'passVerifyCode',	'Please enter Verification Code.',	1),
(49,	'passotp',	'Please enter OTP.',	1),
(50,	'userVerifySuccess',	'User Verify Successfully.',	1),
(51,	'codeNotMatch',	'Code not match, please enter Correct Code.',	1),
(52,	'tokenExpire',	'token expired, Please login.',	1),
(57,	'verifyAccount',	'Please verify Account.',	1),
(60,	'codeVerifySuccess',	'Code Verify Successfully.',	1),
(61,	'codeSendSuccess',	'Verification Code send Successfully. Please check.',	1),
(62,	'somethingWrong',	'something went wrong!',	1),
(63,	'loginFirst',	'Please login first!',	1),
(64,	'passNick',	'Please enter Nick Name.',	1),
(65,	'emailSendSuccess',	'Email send Successfully.',	1),
(71,	'countryListSuccess',	'Country list successfully.',	1),
(72,	'countryNotFound',	'Country not found',	1),
(73,	'passGender',	'Please Select your gender.',	1),
(74,	'passDob',	'Please enter Date of birth .',	1),
(75,	'passIDtype',	'Select ID Type',	1),
(76,	'passAmount',	'Please enter Amount.',	1),
(77,	'passPageKey',	'Please enter Page Key.',	1),
(78,	'recListSuc',	'Record List Successfully.',	1),
(79,	'passFriendID',	'Please pass Friend User ID.',	1),
(80,	'alreadyFriend',	'Already friend.',	1),
(81,	'passKeyword',	'Please enter keyword.',	1),
(82,	'selectPlatform',	'Please select Platform.',	1),
(83,	'selectGame',	'Please select Game.',	1),
(84,	'passBetID',	'Please pass Bet ID.',	1),
(85,	'resultExist',	'Result already Declare.',	1),
(86,	'recordExist',	'Record Already Exist.',	1),
(87,	'betComplete',	'The bet already Completed.',	1),
(88,	'betAccept',	'The bet already Accepted.',	1),
(89,	'passResult',	'Please select Result.',	1),
(90,	'passReqField',	'Please enter required fields.',	1),
(91,	'lowBalance',	'You have insufficient balance, kindly recharge account.',	1),
(92,	'makePay',	'Payment done Successfully.',	1),
(93,	'Credited',	'Amount Credited successfully.',	1),
(94,	'Debited',	'Amount Debited successfully.',	1),
(95,	'emailNotSend',	'mail sending fail.',	1),
(96,	'pwdNotChange',	'Password not Change.',	1),
(97,	'otpVerifySuccess',	'OTP Verify Successfully.',	1),
(98,	'passScheduleDate',	'Please Select Schedule Date.',	1),
(99,	'passScheduleTime',	'Please Select Schedule Time.',	1),
(100,	'passAddress',	'Please enter address.',	1),
(101,	'appBookSuc',	'Appointment booked Successfully',	1),
(102,	'passOrderID',	'Please pass Order ID.',	1),
(103,	'passOrderSum',	'Please enter Order Summary.',	1),
(104,	'passRating',	'Please select rating.',	1),
(105,	'reviewSaveSuc',	'Review saved Successfully',	1),
(106,	'passComment',	'Please enter comments.',	1),
(107,	'passSubject',	'Please enter Subject.',	1),
(109,	'passEmployeeID',	'Please pass employee ID.',	1),
(110,	'passStatus',	'Please pass status.',	1),
(111,	'memberShipOver',	'Membership treatment is over.',	1),
(112,	'memberShipExpire',	'This user Membership Expire.',	1);

DROP TABLE IF EXISTS `tbllaundry_expense`;
CREATE TABLE `tbllaundry_expense` (
  `expenseID` int(11) NOT NULL AUTO_INCREMENT,
  `managerID` int(11) DEFAULT NULL,
  `towel` int(11) NOT NULL DEFAULT 0,
  `napkin` int(11) NOT NULL DEFAULT 0,
  `dress` int(11) NOT NULL DEFAULT 0,
  `bedsheet` int(11) NOT NULL DEFAULT 0,
  `footmate` int(11) NOT NULL DEFAULT 0,
  `total` decimal(10,2) NOT NULL DEFAULT 0.00,
  `date` varchar(25) DEFAULT NULL,
  `isActive` tinyint(2) NOT NULL DEFAULT 1 COMMENT '0=>Inactive, 1=>Active',
  `isDelete` tinyint(2) NOT NULL DEFAULT 0 COMMENT '0=>No, 1=>Yes',
  `createdTime` datetime DEFAULT NULL,
  `updatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`expenseID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `tbloffice_expense`;
CREATE TABLE `tbloffice_expense` (
  `expID` int(11) NOT NULL AUTO_INCREMENT,
  `managerID` int(11) DEFAULT NULL,
  `expDate` varchar(25) DEFAULT NULL,
  `voucher` varchar(100) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT 0,
  `desc` text DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `isActive` tinyint(2) NOT NULL DEFAULT 1 COMMENT '0=>Inactive, 1=>Active',
  `isDelete` tinyint(2) NOT NULL DEFAULT 0 COMMENT '0=>No, 1=>Yes',
  `createdTime` datetime DEFAULT NULL,
  `updatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`expID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `tblpages`;
CREATE TABLE `tblpages` (
  `pageID` int(11) NOT NULL AUTO_INCREMENT,
  `pageKey` varchar(100) DEFAULT NULL,
  `pageTitle` varchar(100) DEFAULT NULL,
  `pageDesc` text DEFAULT NULL,
  `isActive` tinyint(2) NOT NULL DEFAULT 1 COMMENT '0=>Inactive, 1=>Active',
  `isDelete` tinyint(2) NOT NULL DEFAULT 0 COMMENT '0=>No, 1=>Yes',
  `createdTime` datetime DEFAULT NULL,
  `updatedTime` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`pageID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `tblpages` (`pageID`, `pageKey`, `pageTitle`, `pageDesc`, `isActive`, `isDelete`, `createdTime`, `updatedTime`) VALUES
(1,	'about',	'About Us',	'Lorem Ipsum is simply dummied text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting. Remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummied text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.',	1,	0,	'2019-07-01 19:45:40',	'2019-07-01 19:45:40'),
(3,	'privacy',	'Privacy and Policy',	'<div><div><div><div><h1>The bold interior group</h1>Privacy Policy of The bold interior group</div></div></div></div><div><div><div><div><div><h1>Privacy Policy</h1>Effective date: July 07, 2019The bold interior group (\"us\", \"we\", or \"our\") operates The bold interior group mobile application (the \"Service\").This page informs you of our policies regarding the collection, use, and disclosure of personal data when you use our Service and the choices you have associated with that data.&nbsp;We use your data to provide and improve the Service. By using the Service, you agree to the collection and use of information in accordance with this policy. Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings as in our Terms and Conditions.<h2>Information Collection And Use</h2>We collect several different types of information for various purposes to provide and improve our Service to you.<h3>Types of Data Collected</h3>Personal DataWhile using our Service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you (\"Personal Data\"). Personally identifiable information may include, but is not limited to:<ul><li>Email address</li><li>First name and last name</li><li>Phone number</li><li>Address, State, Province, ZIP/Postal code, City</li><li>Cookies and Usage Data</li></ul>Usage DataWhen you access the Service by or through a mobile device, we may collect certain information automatically, including, but not limited to, the type of mobile device you use, your mobile device unique ID, the IP address of your mobile device, your mobile operating system, the type of mobile Internet browser you use, unique device identifiers and other diagnostic data (\"Usage Data\"). Tracking &amp; Cookies DataWe use cookies and similar tracking technologies to track the activity on our Service and hold certain information. Cookies are files with small amount of data which may include an anonymous unique identifier. Cookies are sent to your browser from a website and stored on your device. Tracking technologies also used are beacons, tags, and scripts to collect and track information and to improve and analyze our Service. You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our Service. Examples of Cookies we use:<ul><li>Session Cookies.&nbsp;We use Session Cookies to operate our Service.</li><li>Preference Cookies.&nbsp;We use Preference Cookies to remember your preferences and various settings.</li><li>Security Cookies.&nbsp;We use Security Cookies for security purposes.</li></ul><h2>Use of Data</h2>The bold interior group uses the collected data for various purposes:<ul><li>To provide and maintain the Service</li><li>To notify you about changes to our Service</li><li>To allow you to participate in interactive features of our Service when you choose to do so</li><li>To provide customer care and support</li><li>To provide analysis or valuable information so that we can improve the Service</li><li>To monitor the usage of the Service</li><li>To detect, prevent and address technical issues</li></ul><h2>Transfer Of Data</h2>Your information, including Personal Data, may be transferred to — and maintained on — computers located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction.If you are located outside United States and choose to provide information to us, please note that we transfer the data, including Personal Data, to the United States and process it there.Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.The bold interior group will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this Privacy Policy and no transfer of your Personal Data will take place to an organization or a country unless there are adequate controls in place including the security of your data and other personal information.<h2>Disclosure Of Data</h2><h3>Legal Requirements</h3>The bold interior group may disclose your Personal Data in the good faith belief that such action is necessary to:<ul><li>To comply with a legal obligation</li><li>To protect and defend the rights or property of The bold interior group</li><li>To prevent or investigate possible wrongdoing in connection with the Service</li><li>To protect the personal safety of users of the Service or the public</li><li>To protect against legal liability</li></ul><h2>Security Of Data</h2>The security of your data is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Data, we cannot guarantee its absolute security.<h2>Service Providers</h2>We may employ third party companies and individuals to facilitate our Service (\"Service Providers\"), to provide the Service on our behalf, to perform Service-related services or to assist us in analyzing how our Service is used. These third parties have access to your Personal Data only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose.<h2>Links To Other Sites</h2>Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party\'s site. We strongly advise you to review the Privacy Policy of every site you visit. We have no control over and assume no responsibility for the content, privacy policies or practices of any third party sites or services.<h2>Children\'s Privacy</h2>Our Service does not address anyone under the age of 18 (\"Children\"). We do not knowingly collect personally identifiable information from anyone under the age of 18. If you are a parent or guardian and you are aware that your Children has provided us with Personal Data, please contact us. If we become aware that we have collected Personal Data from children without verification of parental consent, we take steps to remove that information from our servers.<h2>Changes To This Privacy Policy</h2>We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page.We will let you know via email and/or a prominent notice on our Service, prior to the change becoming effective and update the \"effective date\" at the top of this Privacy Policy.You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.<h2>Contact Us</h2>If you have any questions about this Privacy Policy, please contact us:<ul><li>By email: contact@boldinteriorgroup.com</li></ul></div></div></div></div></div>',	1,	0,	'2019-07-08 19:26:14',	'2019-07-08 19:26:14'),
(4,	'terms',	'Terms and Condition',	'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting.\r\n\r\nRemaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.',	1,	0,	'2019-03-05 14:43:00',	'2019-03-05 14:43:00');

DROP TABLE IF EXISTS `tblproduct`;
CREATE TABLE `tblproduct` (
  `productID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `isActive` tinyint(2) NOT NULL DEFAULT 1 COMMENT '0=>Inactive, 1=>Active',
  `isDelete` tinyint(2) NOT NULL DEFAULT 0 COMMENT '0=>No, 1=>Yes',
  `createdTime` datetime DEFAULT NULL,
  `updatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`productID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `tblproduct` (`productID`, `name`, `isActive`, `isDelete`, `createdTime`, `updatedTime`) VALUES
(1,	'Oil',	1,	0,	'2019-12-25 17:50:16',	'2019-12-25 17:50:16'),
(2,	'Cream',	1,	0,	'2019-12-25 17:50:16',	'2019-12-25 17:50:16'),
(3,	'Scrub',	1,	0,	'2019-12-25 17:50:16',	'2019-12-25 17:50:16'),
(4,	'Chocolate',	1,	0,	'2019-12-25 17:50:16',	'2019-12-25 17:50:16'),
(5,	'Dry',	1,	0,	'2019-12-25 17:50:16',	'2019-12-25 17:50:16');

DROP TABLE IF EXISTS `tblsitesetting`;
CREATE TABLE `tblsitesetting` (
  `settingId` int(11) NOT NULL AUTO_INCREMENT,
  `settingKey` varchar(255) DEFAULT NULL,
  `settingValue` text DEFAULT NULL,
  `isActive` tinyint(2) NOT NULL DEFAULT 1,
  `isDelete` tinyint(2) NOT NULL DEFAULT 0,
  `createdTime` datetime DEFAULT NULL,
  `updatedTime` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`settingId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tblsitesetting` (`settingId`, `settingKey`, `settingValue`, `isActive`, `isDelete`, `createdTime`, `updatedTime`) VALUES
(1,	'currency',	'€',	1,	0,	'2019-03-09 10:14:04',	'2018-12-05 15:13:35'),
(2,	'contact-phone',	'1234567890',	1,	0,	'2019-01-11 09:36:59',	'2018-12-10 16:08:56'),
(3,	'contact-email',	'contact@project.com',	1,	0,	'2019-01-11 09:36:59',	'2018-12-10 16:08:56'),
(4,	'contact-time',	'Monday to Friday 9.00am - 7.30pm',	1,	0,	'2018-12-10 16:08:56',	'2018-12-10 16:08:56'),
(5,	'conflictUserCharge',	'1',	1,	0,	'2019-03-11 11:47:06',	'2019-03-11 11:47:06');

DROP TABLE IF EXISTS `tbltreatment`;
CREATE TABLE `tbltreatment` (
  `treatmentID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `duration` int(11) NOT NULL DEFAULT 0,
  `isActive` tinyint(2) NOT NULL DEFAULT 1 COMMENT '0=>Inactive, 1=>Active',
  `isDelete` tinyint(2) NOT NULL DEFAULT 0 COMMENT '0=>No, 1=>Yes',
  `createdTime` datetime DEFAULT NULL,
  `updatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`treatmentID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `tbltreatment` (`treatmentID`, `name`, `duration`, `isActive`, `isDelete`, `createdTime`, `updatedTime`) VALUES
(5,	'CLASSICAL AYURVEDIC-45',	45,	1,	0,	'2020-01-04 05:33:11',	'2020-01-04 05:33:11'),
(6,	'CLASSICAL AYURVEDIC-60',	60,	1,	0,	'2020-01-04 05:33:23',	'2020-01-04 05:33:23'),
(7,	'THE BLISS AROMA-45',	45,	1,	0,	'2020-01-04 05:33:39',	'2020-01-04 05:33:39'),
(8,	'THE BLISS AROMA-60',	60,	1,	0,	'2020-01-04 05:33:48',	'2020-01-04 05:33:48'),
(9,	'HOLISTIC SWEDISH-45',	45,	1,	0,	'2020-01-04 05:34:03',	'2020-01-04 05:34:03'),
(10,	'HOLISTIC SWEDISH-60',	60,	1,	0,	'2020-01-04 05:34:14',	'2020-01-04 05:34:14'),
(11,	'SPRING OF INDONESIA-45',	45,	1,	0,	'2020-01-04 05:34:31',	'2020-01-04 05:34:31'),
(12,	'SPRING OF INDONESIA-60',	60,	1,	0,	'2020-01-04 05:34:42',	'2020-01-04 05:34:42'),
(13,	'DEEP TISSUE-45',	45,	1,	0,	'2020-01-04 05:37:36',	'2020-01-04 05:37:36'),
(14,	'DEEP TISSUE-60',	60,	1,	0,	'2020-01-04 05:37:45',	'2020-01-04 05:37:45'),
(15,	'BODY SCRUB-45',	45,	1,	0,	'2020-01-04 05:37:55',	'2020-01-04 05:37:55'),
(16,	'BODY SCRUB-60',	60,	1,	0,	'2020-01-04 05:38:04',	'2020-01-04 05:38:04'),
(17,	'SOUL OF THAILAND-45',	45,	1,	0,	'2020-01-04 05:38:35',	'2020-01-04 05:38:35'),
(18,	'SOUL OF THAILAND-60',	60,	1,	0,	'2020-01-04 05:38:45',	'2020-01-04 05:38:45');

DROP TABLE IF EXISTS `tbluser`;
CREATE TABLE `tbluser` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `branch` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `gender` enum('Male','Female') DEFAULT NULL,
  `dob` varchar(255) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `picture` text DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `visible_pass` varchar(255) DEFAULT NULL,
  `langType` int(11) NOT NULL DEFAULT 1 COMMENT '1=>English',
  `userRole` int(11) NOT NULL DEFAULT 1 COMMENT '1=>Manager, 3=>Admin',
  `deviceType` int(11) DEFAULT NULL COMMENT '1=>Android, 2=>iOS, 3=>Web',
  `deviceToken` varchar(255) DEFAULT NULL,
  `accessToken` varchar(255) DEFAULT NULL,
  `authProvider` enum('Facebook','Google') DEFAULT NULL,
  `authID` varchar(100) DEFAULT NULL,
  `otp` varchar(10) DEFAULT NULL,
  `isSocial` tinyint(2) NOT NULL DEFAULT 0,
  `isVerified` tinyint(2) NOT NULL DEFAULT 1 COMMENT '0=>No, 1=>Yes',
  `isActive` tinyint(2) NOT NULL DEFAULT 1 COMMENT '0=>Inactive, 1=>Active',
  `isDelete` tinyint(2) NOT NULL DEFAULT 0 COMMENT '0=>No, 1=>Yes',
  `createdTime` datetime DEFAULT NULL,
  `updatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `tbluser` (`userID`, `branch`, `username`, `name`, `email`, `mobile`, `gender`, `dob`, `address`, `picture`, `password`, `visible_pass`, `langType`, `userRole`, `deviceType`, `deviceToken`, `accessToken`, `authProvider`, `authID`, `otp`, `isSocial`, `isVerified`, `isActive`, `isDelete`, `createdTime`, `updatedTime`) VALUES
(1,	'ADMIN',	'admin',	'Spa System',	'spasystem@gmail.com',	'9123456789',	'Male',	'16-12-1985',	'',	NULL,	'81dc9bdb52d04dc20036dbd8313ed055',	'',	1,	3,	NULL,	'',	NULL,	NULL,	NULL,	NULL,	0,	1,	1,	0,	'2019-03-08 17:51:58',	'2018-11-29 12:35:17'),
(10,	'MAIN BRANCH',	'NFS67964',	'JIGNESH BHAI',	'mainbranch@spasystem.com',	'1234567890',	NULL,	'3-12-1993',	NULL,	NULL,	'e10adc3949ba59abbe56e057f20f883e',	'123456',	1,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	1,	1,	0,	'2020-01-04 04:09:41',	'2020-01-04 06:30:11');

-- 2020-01-22 13:37:32
