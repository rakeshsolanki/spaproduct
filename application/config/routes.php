<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = 'auth';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//route for front side module
$route['login'] = 'home/login';
$route['register'] = 'home/register';
$route['forgot-password'] = 'home/forgotPassword';
$route['verify-user'] = 'home/verifyUser';
$route['logout'] = 'auth/logout';
$route['manager/logout'] = 'manager/auth/logout';
$route['verify-forgotcode'] = 'home/verifyForgotcode';
$route['reset-password'] = 'home/resetPassword';
$route['about-us'] = 'home/about';
$route['privacy-policy'] = 'home/privacyPolicy';

// route for admin
$route['admin'] = 'auth';
$route['category/edit/(:any)'] = 'category/add/$1';
$route['pages/edit/(:any)'] = 'pages/add/$1';
$route['users/edit/(:any)'] = 'users/add/$1';

$route['employee'] = 'users/employeeList';
$route['employee/add'] = 'users/addemployee';
$route['employee/edit'] = 'users/addemployee';
$route['employee/edit/(:any)'] = 'users/addEmployee/$1';
$route['employee/delete/(:any)'] = 'users/deleteEmployee/$1';
$route['employee/delete'] = 'users/deleteEmployee';
$route['treatments'] = 'dashboard/treatments';
$route['treatments/add'] = 'dashboard/addTreatment';
$route['treatments/edit'] = 'dashboard/addTreatment';
$route['treatments/edit/(:any)'] = 'dashboard/addTreatment/$1';
$route['customers/edit/(:any)'] = 'customers/add/$1';
$route['treatments/delete/(:any)'] = 'dashboard/deleteTreatment/$1';
$route['treatments/delete'] = 'dashboard/deleteTreatment';

$route['expense/general/add'] = 'expense/general_edit/';
$route['expense/general/edit/(:any)'] = 'expense/general_edit/$1';
$route['expense/general/delete'] = 'expense/general_delete';

$route['expense/laundry/add'] = 'expense/laundry_edit/';
$route['expense/laundry/edit/(:any)'] = 'expense/laundry_edit/$1';
$route['expense/laundry/delete'] = 'expense/laundry_delete';

$route['expense/monthlyExpense/add'] = 'expense/monthlyExpense_edit/';
$route['expense/monthlyExpense/edit/(:any)'] = 'expense/monthlyExpense_edit/$1';
$route['expense/monthlyExpense/delete'] = 'expense/monthlyExpense_delete';

$route['expense/employeeSalary/add'] = 'expense/employeeSalary_edit/';
$route['expense/employeeSalary/edit/(:any)'] = 'expense/employeeSalary_edit/$1';
$route['expense/employeeSalary/delete'] = 'expense/employeeSalary_delete';

$route['invoice/edit/(:any)'] = 'invoice/add/$1';
$route['manager'] = 'manager/auth';