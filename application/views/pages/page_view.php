<section class="content">

    <?php if(isset($msg) || validation_errors() !== ''): ?>

        <div class="alert alert-warning alert-dismissible">

            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            <h4><i class="icon fa fa-warning"></i> Alert!</h4>

            <?= validation_errors();?>

            <?= isset($msg)? $msg: ''; ?>

        </div>

    <?php endif; ?>

    <div class="row">

    <div class="col-md-3">

      <div class="box box-primary">

        <div class="box-body box-profile">

          <img class="profile-user-img img-responsive img-circle" src="<?= base_url() ?>public/dist/img/user4-128x128.jpg" alt="User profile picture">



          <h3 class="profile-username text-center">Nina Mcintire</h3>



          <p class="text-muted text-center">Software Engineer</p>



          <a href="javascript:void(0)" class="btn btn-primary btn-block"><b>Follow</b></a>

        </div>

        <!-- /.box-body -->

      </div>

      <!-- /.box -->



      <!-- About Me Box -->

      <div class="box box-primary">

        <div class="box-header with-border">

          <h3 class="box-title">About Me</h3>

        </div>

        <!-- /.box-header -->

        <div class="box-body">

          <strong><i class="fa fa-book margin-r-5"></i> Education</strong>



          <p class="text-muted">

            B.S. in Computer Science from the University of Tennessee at Knoxville

          </p>



          <hr>



          <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>



          <p class="text-muted">Malibu, California</p>



          <hr>



          <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>



          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>

        </div>

        <!-- /.box-body -->

      </div>

      <!-- /.box -->

    </div>

        <div class="col-md-9">

          <div class="nav-tabs-custom">

              <h3>Content</h3>

          </div>

        </div>

    </div>

</section>
