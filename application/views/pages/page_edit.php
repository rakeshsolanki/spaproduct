<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="box-header with-border">
                        <h3 class="card-title""> <a class="btn btn-info" href="<?= base_url('admin/pages'); ?>"><i class="fa fa-chevron-left"></i> Back </a></h3>
                    </div>
                    
                    <div class="box-body my-form-body">
                        <?php
                        $action = base_url('admin/pages/add');
                        if (isset($pages['pageID']) && $pages['pageID'] != "") {
                            $action = base_url('admin/pages/edit/' . $pages['pageID']);
                        } ?> 
                        <form  action="<?php echo $action; ?>" class="form-material" method="post">
                            <div class="form-group">
                                <label for="pageKey" class="col-sm-2 control-label">Page Key</label>
                                <div class="col-sm-5">
                                    <input type="text" name="pageKey" value="<?php
                                    if (isset($pages['pageKey'])) {
                                        echo $pages['pageKey'];
                                    } ?>" class="form-control form-control-line" id="pageKey" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="pageTitle" class="col-sm-2 control-label">Page Title</label>
                                <div class="col-sm-5">
                                    <input type="text" name="pageTitle" value="<?php
                                    if (isset($pages['pageTitle'])) {
                                        echo $pages['pageTitle'];
                                    } ?>" class="form-control form-control-line" id="pageTitle" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="pageDesc" class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <textarea class="textarea_editor form-control" name="pageDesc" rows="15" placeholder="Enter text ..."><?php if (isset($pages['pageDesc'])) {echo $pages['pageDesc'];} ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="role" class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-5">
                                    <select name="isActive" class="form-control">
                                        <option value="">Select Status</option>
                                        <option value="1" <?php
                                        if (isset($pages['isActive']) && $pages['isActive'] == '1') {
                                            echo 'selected'; } ?> > Active </option>
                                            <option value="0" <?php
                                        if (isset($pages['isActive']) && $pages['isActive'] == '0') {
                                            echo 'selected';
                                        } ?>> Inactive </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-7">
                                    <input type="submit" name="btnSubmit" value="Save" class="btn btn-info">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</section> 
