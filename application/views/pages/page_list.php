<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title"><a class="btn btn-info" href="<?= base_url('admin/pages/add'); ?>"><i class="fa fa-plus"></i> Add</a></h4>
                <div class="table-responsive m-t-40">
                    <table id="myTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Page Key</th>
                                <th>Title</th>
                                <th>Status</th>
                                <th width="150"  class="text-right">Option</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($totalPage)){
                              foreach($totalPage as $key => $row): ?>
                              <tr>
                                  <td><?= $key +1; ?></td>
                                  <td><?= $row['pageKey']; ?></td>
                                  <td><?= $row['pageTitle']; ?></td>
                                  <td>
                                      <?php if($row['isActive']==1){ ?>
                                      <span class="label label-primary">Active</span>
                                      <?php }else{ ?>
                                        <span class="label label-primary">Inactive</span>
                                      <?php } ?>
                                  </td>
                                  <td class="">
                                      <a href="<?= base_url('admin/pages/edit/'.$row['pageID']); ?>" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                                      <a href="<?= base_url('admin/pages/del/'.$row['pageID']); ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                  </td>
                              </tr>
                            <?php endforeach; 
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
