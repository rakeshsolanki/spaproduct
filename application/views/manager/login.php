<!DOCTYPE html>
<html lang="en" >


<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!-- Favicon icon -->
<link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>public/admin/assets/images/favicon.png">
<title>Login | <?php echo $this->config->item('site_title'); ?></title>
<!-- Bootstrap Core CSS -->
<link href="<?= base_url() ?>public/admin/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?= base_url() ?>public/admin/css/style.css" rel="stylesheet">
<!-- You can change the theme colors from here -->
<link href="<?= base_url() ?>public/admin/css/colors/blue.css" id="theme" rel="stylesheet">
<link href="<?php echo $this->config->item('admin_path'); ?>css/custom.css" rel="stylesheet">

<?php
$btnBg = $this->config->item('admin_path').'assets/images/btn_2.png';
?>
<style>
.loginBgBtn {
    height: 50px;
    width: 100%;  
    background-image:url('<?php echo $btnBg; ?>');
    background-repeat:no-repeat;
    background-size: cover;
    background-position: center;
    color: #fff;
    font-weight: bolder;
}
.loginbg{
    background-image: url('<?php echo $this->config->item('admin_path').'assets/images/background/2874926.png'; ?>'); padding-top: 100px;
}
</style>
</head>
<body>
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <section id="wrapper">
        <div class="login-register loginbg" id="login">
            
            <div class="login-box card">
                <div class="card-body">
                    <div class="text-center">
                        <img src="<?php echo $this->config->item('admin_path') . 'assets/images/logo.png'; ?>" width="200" />
                    </div>
                    <?php echo form_open(base_url('manager/auth'), 'class="login-form"'); ?>
                    <?php if (isset($msg) || validation_errors() !== '') : ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                            <?= validation_errors(); ?>
                            <?= isset($msg) ? $msg : ''; ?>
                        </div>
                    <?php endif; ?>
                    
                    <div class="form-group ">
                        <div class="col-xs-12">
                        <input class="form-control" type="text" name="username" id="username" required="" placeholder="Username"> </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                        <input class="form-control" type="password" name="password" id="password" required="" placeholder="Password"> </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button type="submit" name="submit" id="submit" class="btn btn-info btn-lg  text-uppercase waves-effect waves-light " value="Submit">Manager Log In </button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </section>
    
    <script src="<?= base_url() ?>public/admin/assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?= base_url() ?>public/admin/assets/plugins/popper/popper.min.js"></script>
    <script src="<?= base_url() ?>public/admin/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?= base_url() ?>public/admin/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?= base_url() ?>public/admin/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?= base_url() ?>public/admin/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?= base_url() ?>public/admin/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="<?= base_url() ?>public/admin/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?= base_url() ?>public/admin/js/custom.min.js"></script>
</body>

</html> 