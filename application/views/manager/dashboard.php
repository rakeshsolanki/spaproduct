<section class="content">
    <div class="row" id="dashboard">
        <div class="col-lg-3 col-md-6">
            <a href="<?php echo base_url() . 'employee'; ?>">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="round round-lg align-self-center round-success"><i class="fas fa-user-md"></i></div>
                            <div class="m-l-10 align-self-center">
                                <h3 class="m-b-0 font-light"><?php echo (isset($totalEmployeeCount))? $totalEmployeeCount :0; ?></h3>
                                <h5 class="m-b-0">Total Employee</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-6">
            <a href="<?php echo base_url() . 'customers'; ?>">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="round round-lg align-self-center round-success"><i class="fas fa-users"></i></div>
                            <div class="m-l-10 align-self-center">
                                <h3 class="m-b-0 font-light"><?php echo (isset($totalCustomersCount))? $totalCustomersCount :0; ?></h3>
                                <h5 class="m-b-0">Total Membership</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-6">
            <a href="<?php echo base_url() . 'invoice'; ?>">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="round round-lg align-self-center round-success"><i class="fas fa-chart-pie"></i></div>
                            <div class="m-l-10 align-self-center">
                                <h3 class="m-b-0 font-light"><?php echo (isset($totalInvoiceCount))? $totalInvoiceCount :0; ?></h3>
                                <h5 class="m-b-0">Total Invoice</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</section>
