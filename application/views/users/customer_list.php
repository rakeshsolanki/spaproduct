<?php
$userRole = $this->session->userdata('user_type');
?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body bg-info">
                <h4 class="text-white card-title">
                    <a class="btn btn-info" href="<?= base_url('customers/add'); ?>"><i class="fa fa-plus"></i> Add</a> &nbsp;&nbsp;&nbsp; Member List
                </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="myTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>M.ID</th>
                                <th>Name</th>
                                <th>Contact</th>
                                <th>Join Date</th>
                                <th width="70">Expiry</th>
                                <th>Validity</th>
                                <th>Duration</th>
                                <th>Treatment</th>
                                <th>Balance</th>
                                <th>Slip Number</th>
                                <th>Amount</th>
                                <th>Branch</th>
                                <?php if($userRole==3){ ?>
                                    <th>SMS PERMI.</th>
                                    <th>Action</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($resultList)) {
                                foreach ($resultList as $key => $row) : 
                                    $remaining = $this->Service->getRemainingTreatment($row['membershipID']);
                                    $today = date('Y-m-d');
                                    if($remaining!=0 && $row['expireDate'] > $today){ ?>
                                        <tr id="lnr_<?= $row['customerID']; ?>">
                                            <td><?= $row['membershipID']; ?></td>
                                            <td><?= (isset($row['name'])) ? $row['name'] : "-"; ?></td>
                                            <td><?= (isset($row['mobile'])) ? $row['mobile'] : "-"; ?></td>
                                            <td><?= (isset($row['membershipDate'])) ? date('d-m-Y',strtotime( $row['membershipDate'])) : "-"; ?></td>
                                            <td><?= (isset($row['expireDate'])) ? date('d-m-Y',strtotime( $row['expireDate'])) : "-"; ?></td>
                                            <td><?= (isset($row['validity'])) ? $row['validity'].'Month' : "-"; ?></td>
                                            <td><?= (isset($row['duration'])) ? $row['duration'].' MIN' : ""; ?></td>
                                            <td><?= (isset($row['totalTreatment'])) ? $row['totalTreatment'] : "0"; ?></td>
                                            <td><?= (isset($row['membershipID'])) ? $this->Service->getRemainingTreatment($row['membershipID']) : "0"; ?></td>
                                            <td><?= (isset($row['yslip'])) ? $row['yslip'] : "-"; ?></td>
                                            <td><?= (isset($row['amount'])) ? $row['amount'] : 0; ?><br>
                                            <label class="label label-info"><?= (isset($row['paymentMode'])) ? $row['paymentMode'] : ""; ?></label>
                                            </td>
                                            <td><?= (isset($row['managerID'])) ? $this->Service->getBranceName($row['managerID']) : "-"; ?></td>
                                            <?php if($userRole==3){ ?>
                                                <td>
                                                <?php 
                                                if($row['msgStatus'] == 1){?>
                                                        <span class="btn btn-primary btn-flat btn-xs" onclick="custInactive(<?= $row['customerID'] ?>)" id="active_<?= $row['customerID'] ?>" >ACTIVE</span>
                                                        <span class="btn btn-danger btn-flat btn-xs  d-none" onclick="custActive(<?= $row['customerID'] ?>)" id="inactive_<?= $row['customerID'] ?>">INACTIVE</span>
                                                <?php }else{ ?> 
                                                        <span class="btn btn-danger btn-flat btn-xs" onclick="custActive(<?= $row['customerID'] ?>)" id="inactive_<?= $row['customerID'] ?>">INACTIVE</span>
                                                        <span class="btn btn-primary btn-flat btn-xs d-none"  onclick="custInactive(<?= $row['customerID'] ?>)" id="active_<?= $row['customerID'] ?>">ACTIVE</span>
                                                <?php } ?>
                                                </td>
                                                <td class="">
                                                    <a href="<?= base_url('customers/edit/' . $row['customerID']); ?>" class="btn btn-info btn-sm "><i class="fa fa-edit"></i></a>
                                                    <span  onclick="deletecustomerdata('<?= $row['customerID']; ?>');" class="btn btn-sm btn-danger "><i class="fa fa-trash"></i></span>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                    <?php }
                                endforeach;
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> 

