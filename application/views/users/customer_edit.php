<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body bg-info">
                    <h4 class="text-white card-title"> Membership Form </h4>
                </div>
                <div class="card-body">
                    <div class="box-body my-form-body">
                        <?php
                        $action = base_url('customers/add');
                        if (isset($rowData['customerID']) && $rowData['customerID'] != "") {
                            $action = base_url('customers/edit/' . $rowData['customerID']);
                        } ?>
                        <form  action="<?php echo $action; ?>" class="" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-3 form-group">
                                <label for="name" class="control-label">Name</label>
                                <input type="text" name="name" value="<?php if (isset($rowData['name'])) { echo $rowData['name'];}else{ echo set_value('name'); } ?>" class="form-control form-control-line" id="name" placeholder="">
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="mobile" class="control-label">Mobile</label>
                                <input type="text" name="mobile" value="<?php if (isset($rowData['mobile'])) { echo $rowData['mobile']; }else{ echo set_value('mobile'); } ?>" class="form-control" id="mobile" placeholder="">
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="membershipDate" class="control-label">Buy Date</label>
                                <input type="text" name="membershipDate" class="form-control datePicker" placeholder="" id="membershipDate" value="<?php if (isset($rowData['membershipDate'])) { echo date('d-m-Y',strtotime($rowData['membershipDate'])); }else{ echo date('d-m-Y'); } ?>" />
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="validity" class="control-label">Validity</label>
                                <select name="validity" class="form-control">
                                    <option>Select Validity</option>
                                    <?php
                                    $validity = (isset($rowData['validity']) && $rowData['validity'])?$rowData['validity']:"";
                                    for($i=1;$i<=12;$i++){ ?>
                                        <option value="<?= $i; ?>" <?php echo ($validity==$i)?"selected":""; ?>><?= $i; ?> Month</option>
                                    <?php } ?>
                                    <option value="24" <?php echo ($validity==24)?"selected":""; ?>>2 Year</option>
                                    <option value="60" <?php echo ($validity==60)?"selected":""; ?>>5 Year</option>
                                    <option value="120" <?php echo ($validity==120)?"selected":""; ?>>10 Year</option>
                                </select>
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="totalTreatment" class="control-label">Total Treatment</label>
                                <input type="number" name="totalTreatment" value="<?php if (isset($rowData['totalTreatment'])) { echo $rowData['totalTreatment']; }else{ echo set_value('totalTreatment'); } ?>" class="form-control" id="totalTreatment" placeholder="">
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="gender" class="control-label">Gender</label>
                                <select class="form-control" id="gender" name="gender">
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                    <option value="Other">Other</option>
                                </select>
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="yslip" class="control-label">Slip No.</label>
                                <input type="text" name="yslip" value="<?php if (isset($rowData['yslip'])) { echo $rowData['yslip']; }else{ echo set_value('yslip'); } ?>" class="form-control" id="yslip" placeholder="">
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="amount" class="control-label">Amount</label>
                                <input type="text" name="amount" value="<?php if (isset($rowData['amount'])) { echo $rowData['amount']; }else{ echo set_value('amount'); } ?>" class="form-control" id="amount" placeholder="">
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="duration" class="control-label">Duration</label>
                                <?php $duration = (isset($rowData['duration']) && $rowData['duration'])?$rowData['duration']:""; ?>
                                <select name="duration" class="form-control">
                                    <option value="">Select Duration</option>
                                    <option value="45" <?php echo ($duration==45)?"selected":""; ?>>45 Minute</option>
                                    <option value="60" <?php echo ($duration==60)?"selected":""; ?>>60 Minute</option>
                                </select>
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="paymentMode" class="control-label">Payment Mode</label>
                                <select class="form-control" id="paymode" name="paymentMode">
                                    <option value="">Select Payment Mode</option>
                                    <option value="CASH"<?php if (isset($rowData['paymentMode']) && $rowData['paymentMode']== 'CASH') { echo 'selected'; } ?>>CASH</option>
                                    <option value="CARD"<?php if (isset($rowData['paymentMode']) && $rowData['paymentMode']== 'CARD') { echo 'selected'; } ?>>CARD</option> 
                                    <option value="PAYTM"<?php if (isset($rowData['paymentMode']) && $rowData['paymentMode']== 'PAYTM') { echo 'selected'; } ?>>PAYTM</option>
                                    <option value="PHONE PAY"<?php if (isset($rowData['paymentMode']) && $rowData['paymentMode']== 'PHONE PAY') { echo 'selected'; } ?>>PHONE PE</option>
                                    <option value="GOOGLE PAY"<?php if (isset($rowData['paymentMode']) && $rowData['paymentMode']== 'GOOGLE PAY') { echo 'selected'; } ?>>GOOGLE PAY</option>
                                    <option value="NET BANKING"<?php if (isset($rowData['paymentMode']) && $rowData['paymentMode']== 'NET BANKING') { echo 'selected'; } ?>>NET BANKING</option>
                                    <option value="OTHER"<?php if (isset($rowData['paymentMode']) && $rowData['paymentMode']== 'OTHER') { echo 'selected'; } ?>>OTHER</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 form-group">
                            <input type="submit" name="submit" value="Save" class="btn btn-info" />
                            <a class="btn btn-danger" href="<?php echo base_url('customers'); ?>">Cancel</a>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

