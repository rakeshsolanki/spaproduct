<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body bg-info">
                <h4 class="text-white card-title">
                <a class="btn btn-info" href="<?= base_url('users/add'); ?>"><i class="fa fa-plus"></i> Add</a> &nbsp; &nbsp; &nbsp; Branch List
                </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="myTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Branch</th>
                                <th>Username</th>
                                <th>Password</th>
                                <th>Full Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Status</th>
                                <th>Option</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($all_users)) {
                                foreach ($all_users as  $key => $row) : ?>
                                    <tr id="lnr_<?= $row['userID']; ?>">
                                        <td><?= (isset($row['branch'])) ? $row['branch'] : "-"; ?></td>
                                        <td><?= (isset($row['username'])) ? $row['username'] : "-"; ?></td>
                                        <td><?= (isset($row['visible_pass'])) ? $row['visible_pass'] : "-"; ?></td>
                                        <td><?= (isset($row['name'])) ? $row['name'] : "-"; ?></td>
                                        <td><?= (isset($row['email'])) ? $row['email'] : "-"; ?></td>
                                        <td><?= (isset($row['mobile'])) ? $row['mobile'] : "-"; ?></td>
                                        <td><?php 
                                            if($row['isActive'] == 1){?>
                                                <span class="btn btn-primary btn-flat btn-xs" onclick="inactive(<?= $row['userID'] ?>)" id="active_<?= $row['userID'] ?>" >Active</span>
                                                <span class="btn btn-danger btn-flat btn-xs  d-none"  onclick="active(<?= $row['userID'] ?>)" id="inactive_<?= $row['userID'] ?>">Inactive</span>
                                        <?php }else{ ?> 
                                                <span class="btn btn-danger btn-flat btn-xs" onclick="active(<?= $row['userID'] ?>)" id="inactive_<?= $row['userID'] ?>">Inactive</span>
                                                <span class="btn btn-primary btn-flat btn-xs d-none"  onclick="inactive(<?= $row['userID'] ?>)" id="active_<?= $row['userID'] ?>">Active</span>
                                        <?php } ?>
                                        </td>
                                        <td class="">
                                            <a href="<?= base_url('users/edit/' . $row['userID']); ?>" class="btn btn-info btn-sm "><i class="fa fa-edit"></i></a>
                                            <span  onclick="deleteUserdata('<?= $row['userID']; ?>');" class="btn btn-sm btn-danger "><i class="fa fa-trash"></i></span>
                                        </td>
                                    </tr>
                                <?php endforeach;
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> 

<script>


function inactive(userid){
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('users/userIsactive') ?>",
        data:{status:0,userid:userid},
        success: function(data){
            if(data)
            {
                $('#inactive_'+userid).removeClass('d-none');
                $('#active_'+userid).addClass('d-none');
            }
        }
    });
}

function active(userid){
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('users/userIsactive') ?>",
        data:{status:1,userid:userid},
        success: function(data){
            if(data)
            {
                $('#active_'+userid).removeClass('d-none');
                $('#inactive_'+userid).addClass('d-none');
            }
        }
    });
}

</script>