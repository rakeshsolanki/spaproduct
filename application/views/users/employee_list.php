<?php
$userRole = $this->session->userdata('user_type');
?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body bg-info">
                <h4 class="text-white card-title">
                <?php
                    if($userRole==3){ ?>
                    <a class="btn btn-info" href="<?= base_url('employee/add'); ?>"><i class="fa fa-plus"></i> Add</a>
                    <?php } ?> &nbsp;&nbsp;&nbsp; Employee List
                    
                </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="myTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Join Date</th>
                                <th>Name</th>
                                <th>Gender</th>
                                <th>Specialist</th>
                                <?php if($userRole==3){ ?><th>Mobile</th><?php } ?>
                                <th>DOB</th>
                                <th>Address</th>
                                <?php if($userRole==3){ ?><th>Option</th><?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($resultList)) {
                                foreach ($resultList as $key => $row) : ?>
                                    <tr id="lnr_<?= $row['employeeID']; ?>">
                                        <td><?= (isset($row['joinDate'])) ?  date('d-m-Y',strtotime($row['joinDate'])) : "-"; ?></td>
                                        <td><?= (isset($row['name'])) ? $row['name'] : "-"; ?></td>
                                        <td><?= (isset($row['gender'])) ? $row['gender'] : "-"; ?></td>
                                        <td><?= (isset($row['specialist'])) ? $row['specialist'] : "-"; ?></td>
                                        <?php if($userRole==3){ ?><td><?= (isset($row['mobile'])) ? $row['mobile'] : "-"; ?></td><?php } ?>
                                        <td><?= (isset($row['dob'])) ? date('d-m-Y',strtotime($row['dob'])) : "-"; ?></td>
                                        <td><?= (isset($row['address'])) ? $row['address'] : "-"; ?></td>
                                        <?php if($userRole==3){ ?>
                                        <td class="">
                                            <a href="<?= base_url('employee/edit/' . $row['employeeID']); ?>" class="btn btn-info btn-sm "><i class="fa fa-edit"></i></a>
                                            <span  onclick="deletedata('<?= $row['employeeID']; ?>');" class="btn btn-sm btn-danger "><i class="fa fa-trash"></i></span>
                                        </td>
                                        <?php } ?>
                                    </tr>
                                <?php endforeach;
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> 

<script>
function deletedata(employeeid) {
  var r = confirm("Are you sure delete this data!");
  if (r == true) {
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('employee/delete') ?>",
        data:{employeeid:employeeid},
        success: function(data){
            if(data)
            {
                $('#lnr_'+employeeid).hide();
            }
        }
    });
  } 
}
</script>