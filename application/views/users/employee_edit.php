<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body bg-info">
                    <h4 class="text-white card-title"> Employee Form </h4>
                </div>
                <div class="card-body">
                    <div class="box-body my-form-body">
                        <?php
                        $action = base_url('employee/add');
                        if (isset($rowData['employeeID']) && $rowData['employeeID'] != "") {
                            $action = base_url('employee/edit/' . $rowData['employeeID']);
                        } ?>
                        <form class="row" action="<?php echo $action; ?>" class="" method="post" enctype="multipart/form-data">
                            <div class="col-sm-3 form-group">
                                <label for="name" class="control-label">Name</label>
                                <input type="text" name="name" value="<?php if (isset($rowData['name'])) { echo $rowData['name'];}else{ echo set_value('name'); } ?>" class="form-control form-control-line" id="name" placeholder="">
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="email" class="control-label">Email</label>
                                <input type="email" name="email" value="<?php if (isset($rowData['email'])) { echo $rowData['email']; }else{ echo set_value('email'); } ?>" class="form-control" id="email" placeholder="">
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="mobile" class="control-label">Mobile</label>
                                <input type="number" name="mobile" value="<?php if (isset($rowData['mobile'])) { echo $rowData['mobile']; }else{ echo set_value('mobile'); } ?>" class="form-control" id="mobile" placeholder="">
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="gender" class="control-label">Gender:</label><br>
                                <input name="gender" type="radio" id="radio_1" value="Male"  />
                                <label for="radio_1"> Male </label>
                                <input name="gender" type="radio" id="radio_2" value="Female" checked />
                                <label for="radio_2"> Female</label>
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="dob" class="control-label">DOB</label>
                                <input type="text" name="dob" class="form-control datePicker" placeholder="" id="dob" value="<?php if (isset($rowData['dob'])) { echo date('d-m-Y',strtotime($rowData['dob'])); }else{ echo date('d-m-Y');} ?>" />
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="address" class="control-label">Address</label>
                                <textarea name="address" class="form-control" id="address" placeholder=""><?php if (isset($rowData['address'])) { echo $rowData['address']; }else{ echo set_value('address'); } ?></textarea>
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="specialist" class="control-label">Specialist</label>
                                <input type="text" name="specialist" value="<?php if (isset($rowData['specialist'])) { echo $rowData['specialist'];}else{ echo set_value('specialist'); } ?>" class="form-control form-control-line" id="specialist" placeholder="">
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="joinDate" class="control-label">Join Date</label>
                                <input type="text" name="joinDate" class="form-control datePicker" placeholder="" id="joinDate" value="<?php if (isset($rowData['joinDate'])) { echo date('d-m-Y',strtotime($rowData['joinDate'])); }else{ echo date('d-m-Y');} ?>" />
                            </div>
                            <div class="col-sm-12 form-group">
                                <input type="submit" name="submit" value="Save" class="btn btn-info" />
                                <a href="<?php echo base_url('employee'); ?>" class="btn btn-danger">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

