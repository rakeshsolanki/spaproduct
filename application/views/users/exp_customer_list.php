<?php
$userRole = $this->session->userdata('user_type');
?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body bg-info">
                <h4 class="text-white card-title"> Member List </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="myTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>M.ID</th>
                                <th>Name</th>
                                <th>Contact</th>
                                <th>Join Date</th>
                                <th>Expiry</th>
                                <th>Validity</th>
                                <th>Duration</th>
                                <th>Treatment</th>
                                <th>Balance</th>
                                <th>Slip Number</th>
                                <th>Amount</th>
                                <th>Branch</th>
                                <?php if($userRole==3){ ?>
                                    <th>Action</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($resultList)) {
                                foreach ($resultList as $key => $row) : 
                                    $remaining = $this->Service->getRemainingTreatment($row['membershipID']);
                                    $today = date('Y-m-d');
                                    if($remaining==0 || $row['expireDate'] < $today){ ?>
                                        <tr id="lnr_<?= $row['customerID']; ?>">
                                            <td><?= $row['membershipID']; ?></td>
                                            <td><?= (isset($row['name'])) ? $row['name'] : "-"; ?></td>
                                            <td><?= (isset($row['mobile'])) ? $row['mobile'] : "-"; ?></td>
                                            <td><?= (isset($row['membershipDate'])) ? date('d-m-Y',strtotime( $row['membershipDate'])) : "-"; ?></td>
                                            <td><?= (isset($row['expireDate'])) ? date('d-m-Y',strtotime( $row['expireDate'])) : "-"; ?></td>
                                            <td><?= (isset($row['validity'])) ? $row['validity'].'Month' : "-"; ?></td>
                                            <td><?= (isset($row['duration'])) ? $row['duration'].' MIN' : ""; ?></td>
                                            <td><?= (isset($row['totalTreatment'])) ? $row['totalTreatment'] : "0"; ?></td>
                                            <td><?= (isset($row['membershipID'])) ? $this->Service->getRemainingTreatment($row['membershipID']) : "0"; ?></td>
                                            <td><?= (isset($row['yslip'])) ? $row['yslip'] : "-"; ?></td>
                                            <td><?= (isset($row['amount'])) ? $row['amount'] : 0; ?><br>
                                            <label class="label label-info"><?= (isset($row['paymentMode'])) ? $row['paymentMode'] : ""; ?></label>
                                            </td>
                                            <td><?= (isset($row['managerID'])) ? $this->Service->getBranceName($row['managerID']) : "-"; ?></td>
                                            
                                            <?php if($userRole==3){ ?>
                                                <td class="">
                                                    <a href="<?= base_url('customers/edit/' . $row['customerID']); ?>" class="btn btn-info btn-sm "><i class="fa fa-edit"></i></a>
                                                    <span  onclick="deletedata('<?= $row['customerID']; ?>');" class="btn btn-sm btn-danger "><i class="fa fa-trash"></i></span>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                    <?php } ?>
                                    
                                <?php endforeach;
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> 

<script>
function deletedata(customerid) {
  var r = confirm("Are you sure delete this data!");
  if (r == true) {
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('customers/delete') ?>",
        data:{customerid:customerid},
        success: function(data){
            if(data)
            {
                $('#lnr_'+customerid).hide();
            }
        }
    });
  } 
}

function active(userid){
    // var userid = $('#userID').val();
    //alert(userid);
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('users/userSmsPermision') ?>",
        data:{status:1,userid:userid},
        success: function(data){
            if(data)
            {
                $('#active_'+userid).removeClass('d-none');
                $('#inactive_'+userid).addClass('d-none');
            }
        }
    });
}

function inactive(userid){
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('users/userSmsPermision') ?>",
        data:{status:0,userid:userid},
        success: function(data){
            if(data)
            {
                $('#inactive_'+userid).removeClass('d-none');
                $('#active_'+userid).addClass('d-none');
            }
        }
    });
}
</script>