<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body bg-info">
                    <h4 class="text-white card-title"> General Expense </h4>
                </div>
                <div class="card-body">
                    <div class="box-body my-form-body">
                        <?php
                        $action = base_url('users/add');
                        if (isset($user['userID']) && $user['userID'] != "") {
                            $action = base_url('users/edit/' . $user['userID']);
                        } ?>
                        <form  action="<?php echo $action; ?>" class="" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-4 form-group">
                                    <label for="name" class="control-label">Name</label>
                                    <input type="text" name="name" value="<?php if (isset($user['name'])) { echo $user['name'];}else{ echo set_value('name'); } ?>" class="form-control form-control-line" id="name" placeholder="">
                                </div>

                                <div class="col-sm-4 form-group">
                                    <label for="branch" class="control-label">Branch Name</label>
                                    <input type="text" name="branch" value="<?php if (isset($user['branch'])) { echo $user['branch']; }else{ echo set_value('branch'); } ?>" class="form-control form-control-line" id="branch" placeholder="">
                                </div>
                                <div class="col-sm-4 form-group">
                                    <label for="dob" class="control-label">DOB</label>
                                    <input type="text" name="dob" class="form-control datePicker" placeholder="" id="dob" value="<?php if (isset($user['dob'])) { echo $user['dob']; } ?>" />
                                </div>

                                <div class="col-sm-4 form-group">
                                    <label for="email" class="control-label">Email</label>
                                    <input type="email" name="email" value="<?php if (isset($user['email'])) { echo $user['email']; }else{ echo set_value('email'); } ?>" class="form-control" id="email" placeholder="">
                                </div>

                                <div class="col-sm-4 form-group">
                                    <label for="mobile" class="control-label">Mobile</label>
                                    <input type="number" name="mobile" value="<?php if (isset($user['mobile'])) { echo $user['mobile']; }else{ echo set_value('mobile'); } ?>" class="form-control" id="mobile" placeholder="">
                                </div>

                                <div class="col-sm-4 form-group">
                                    <label for="password" class="control-label">Password</label>
                                    <input type="password" name="password" class="form-control" id="password" placeholder="" />
                                </div>

                                <input type="hidden" name="userRole" value="1" />
                                <div class="col-sm-4 form-group">
                                    <label for="role" class="control-label">Status</label>
                                    <select name="isActive" class="form-control">
                                        <option value="">Select Status</option>
                                        <option value="1" <?php
                                        if (isset($user['isActive']) && $user['isActive'] == '1') {
                                            echo 'selected';
                                        } ?>> Active </option>
                                        <option value="0" <?php
                                        if (isset($user['isActive']) && $user['isActive'] == '0') {
                                            echo 'selected';
                                        } ?>> Inactive </option>
                                    </select>
                                </div>

                                <div class="col-sm-12 form-group">
                                     <input type="submit" name="submit" value="Save" class="btn btn-info" />
                                    <a href="<?php echo base_url('users'); ?>" class="btn btn-danger">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
