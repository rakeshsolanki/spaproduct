<?php if(validation_errors() !== '' || $this->session->flashdata('error_msg') != "" || $this->session->flashdata('success_msg') != ""): ?>
    <?php if($this->session->flashdata('success_msg') != ""){ ?>
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $this->session->flashdata('success_msg'); ?>
        </div>
    <?php }else{ ?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?= validation_errors(); ?>
            <?= $this->session->flashdata('error_msg'); ?>
        </div>
    <?php } ?>
<?php endif; ?>