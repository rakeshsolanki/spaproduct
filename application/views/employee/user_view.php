
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="ribbon-wrapper card">
            <div class="ribbon ribbon-bookmark  ribbon-default"><?php echo ($user['name']) ? $user['name'] : "" ?></div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-body bg-info">
                <h4 class="text-white card-title">Assign User <b><?php //echo ($user['chocoFlouz']) ? $user['chocoFlouz'] : "0" ?> </b> </h4>
            </div>
            <div class="card-body">
                <form action="" method="post">
                    <div class="form-group">
                        <label>
                            <h3>Assign User</h3>
                        </label>
                        <select class="select2 m-b-10 select2-multiple width100per"  multiple="multiple" name="user[]" data-placeholder="Choose">
                            <option>Select user</option>
                            <?php foreach($assignUser as $row){  ?>
                                <option value="<?= isset($row['userID']) ? $row['userID'] :''; ?>"<?php if(in_array($row['userID'],$userID)){ echo 'selected';} ?>><?= $this->user->getUsername($row['userID']); ?> </option>
                            <?php }
                            foreach($getUnassignUserList as $row){  ?>
                                <option value="<?= isset($row['userID']) ? $row['userID'] :''; ?>"><?= $this->user->getUsername($row['userID']); ?> </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success waves-effect waves-light" type="submit" value="12" formaction="<?php echo base_url('admin/employee/assignuser/'.$user['userID']); ?>"><span class="btn-label"><i class="mdi mdi-credit-card-plus"></i></span>Save Changes</button>
                        
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body bg-info">
                <h4 class="text-white card-title">Assign User List</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example" class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>User Name</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if(!empty($assignUser)){
                            foreach($assignUser as $key=> $userData){ ?>
                            <tr>
                                <td><?= $key +1 ;?></td>
                                <td><?= $this->user->getUsername($userData['userID']); ?></td>
                                <td><?php 
                                    if($userData['status'] == 1){ ?>
                                        <label class="label label-primary" id="active_<?php echo $userData['userID'] ?>">Assign</span>
                                   <?php }else{ ?>
                                        <label class="label label-danger" id="inactive_<?php echo $userData['userID'] ?>">Inactive</span>
                                   <?php } ?>
                                </td>
                            </tr>
                            <?php } } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body bg-info">
                <h4 class="text-white card-title">Reviews List</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example2" class="table">
                        <thead>
                            <tr>
                                <th>OrderID</th>
                                <th>User</th>
                                <th>Rating</th>
                                <th>Review</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if(!empty($reviewList)){
                            foreach($reviewList as $key=>$review){ ?>
                            <tr>
                                <td><?php
                                    if(isset($review['orderID']) && $review['orderID']!=""){ ?>
                                        <a class="label label-primary" href="<?php echo base_url().'admin/order/view/'.$review['orderID']; ?>"><?php echo $review['orderID']; ?></a>
                                    <?php }else{
                                        echo "-";
                                    } ?>
                                </td>
                                <td><?= (isset($review['userID']))?$this->user->getUsername($review['userID']):"-"; ?></td>
                                <td><?php
                                    if(isset($review['rating']) && $review['rating']!=""){
                                        for($i=0; $i<$review['rating']; $i++){ ?>
                                            <i class="mdi mdi-star text-success"></i>
                                        <?php }
                                    } ?>
                                </td>
                                <td><?= (isset($review['review']))?$review['review']:"-"; ?></td>
                            </tr>
                            <?php } } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    
    $(function() {
        $('#example').DataTable();
        $('#example1').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "pageLength": 5,
            "bInfo": false
        });
        $('#example2').dataTable({ });
    });
</script>  