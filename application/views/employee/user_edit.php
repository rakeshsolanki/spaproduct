<style>
    .bgadd{
        background: url(<?php echo base_url(PROFILE.$user['picture']); ?>);  background-position: center;background-size: contain;background-repeat: no-repeat;height: 150px;
    }
</style>
<section class="content">

    <div class="row">

        <div class="col-md-12">

            <div class="card">

                <div class="card-body">

                    <div class="box-header with-border">

                        <h3 class="card-title""> <a class="btn btn-info" href="<?= base_url('admin/employee'); ?>"><i class="fa fa-chevron-left"></i> Back </a></h3>

                    </div>

                    <div class="box-body my-form-body">

                        <?php

                        $action = base_url('admin/employee/add');

                        if (isset($user['userID']) && $user['userID'] != "") {

                            $action = base_url('admin/employee/edit/' . $user['userID']);

                        }

                        ?> 

                        <form  action="<?php echo $action; ?>" class="" method="post"enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-5">
                                    <input type="text" name="name" value="<?php if (isset($user['name'])) { echo $user['name'];}else{ echo set_value('name'); } ?>" class="form-control form-control-line" id="name" placeholder="">
                                </div>
                            </div>

                            <div class="form-group">

                                <label for="gender" class="col-sm-2 control-label">Gender</label>
                                <div class="col-sm-5">
                                    <input name="gender" type="radio" id="radio_1" value="Male" checked />
                                    <label for="radio_1"> Male </label>
                                    <input name="gender" type="radio" id="radio_2" value="Female" />
                                    <label for="radio_2"> Female</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dob" class="col-sm-2 control-label">DOB</label>
                                <div class="col-sm-5">
                                    <input type="text" name="dob" class="form-control" placeholder="" id="dob" value="<?php if (isset($user['dob'])) { echo $user['dob']; } ?>" />
                                </div>
                            </div>

                            <div class="form-group">

                                <label for="email" class="col-sm-2 control-label">Email</label>

                                <div class="col-sm-5">

                                    <input type="email" name="email" value="<?php if (isset($user['email'])) { echo $user['email']; }else{ echo set_value('email'); } ?>" class="form-control" id="email" placeholder="">

                                </div>

                            </div>

                            <div class="form-group">

                                <label for="mobile" class="col-sm-2 control-label">Mobile</label>

                                <div class="col-sm-5">

                                    <input type="number" name="mobile" value="<?php if (isset($user['mobile'])) { echo $user['mobile']; }else{ echo set_value('mobile'); } ?>" class="form-control" id="mobile" placeholder="">

                                </div>

                            </div>

                            <?php //if (empty($user)) { ?>

                                <div class="form-group">

                                    <label for="password" class="col-sm-2 control-label">Password</label>

                                    <div class="col-sm-5">

                                        <input type="text" name="password" value="<?php echo isset($user['visible_pass']) ? $user['visible_pass'] :''; ?>" class="form-control" id="password" placeholder="" />

                                    </div>

                                </div>

                            <?php // } ?>

                            <input type="hidden" name="userRole" value="1" />
                            <div class="form-group">
                                <div class="col-md-12 divPostService">
                                    <div class="submit-field">
                                        <div class="uploadButton">
                                            <input class="uploadButton-input" type="file" name="picture" accept="image/*, application/pdf" id="upload" >
                                        </div>
                                    </div>
                                    <div class="row" id="portfolioSection">
                                        <?php if(!empty($user['picture'])){ ?>
                                            <div class="pimg col-md-2 text-center "> 
                                                <div class="bgadd" ></div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">

                                <label for="role" class="col-sm-6 control-label">Is Default Employee</label>

                                <div class="col-sm-5">

                                    <select name="isDefaultEmployee" class="form-control">

                                        <option value="">Select Default Status</option>

                                        <option value="1" <?php

                                        if (isset($user['isDefaultEmployee']) && $user['isDefaultEmployee'] == '1') {

                                            echo 'selected';

                                        } ?> > Yes </option>

                                        <option value="0" <?php

                                        if (isset($user['isDefaultEmployee']) && $user['isDefaultEmployee'] == '0') {

                                            echo 'selected';

                                        } ?> > No </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="role" class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-5">
                                    <select name="isActive" class="form-control">
                                        <option value="">Select Status</option>
                                        <option value="1" <?php
                                        if (isset($user['isActive']) && $user['isActive'] == '1') {
                                            echo 'selected';
                                        } ?> > Active </option>
                                        <option value="0" <?php
                                        if (isset($user['isActive']) && $user['isActive'] == '0') {
                                            echo 'selected';
                                        } ?> > Inactive </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-7">
                                    <input type="submit" name="submit" value="<?php
                                    if (isset($user['userID']) && $user['userID'] != "") {
                                        echo "Update";
                                    } else {
                                        echo "Add";
                                    } ?> User" class="btn btn-info">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</section> 

