<div class="row">

    <div class="col-12">

        <div class="card">
        <div class="card-body bg-info">
                <h4 class="text-white card-title">
                     Employee List
                    <div class="pull-right">
                        <h4 class="card-title"><a class="btn btn-info" href="<?= base_url('admin/employee/add'); ?>"><i class="fa fa-plus"></i> Add</a></h4>
                    </div>
                </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="myTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>User ID</th>
                                <th>Profile</th>
                                <th>Full Name</th>
                                <th>Email</th>
                                <th>Password</th>
                                <th>Mobile</th>
                                <th>Status</th>
                                <th width="150">Option</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($all_users)) {
                                foreach ($all_users as $row) : ?>
                            <tr>
                                <td><?= $row['userID']; ?></td>
                                <td >
                                    <?php if(!empty($row['picture'])){ ?>
                                        <a href="<?= base_url('admin/users/edit/' . $row['userID']); ?>" target="blank">
                                            <img src="<?php echo base_url(PROFILE.$row['picture']);  ?>" width="100" height="100"/>
                                        </a>
                                    <?php } ?>
                                </td>
                                <td ><a href="<?php echo base_url('admin/employee/view/'.$row['userID']); ?>"><b><?= (isset($row['name'])) ? $row['name'] : "-"; ?></b></a></td>

                                <td><?= (isset($row['email'])) ? $row['email'] : "-"; ?></td>
                                <td><?= (isset($row['visible_pass'])) ? $row['visible_pass'] : "-"; ?></td>

                                <td><?= (isset($row['mobile'])) ? $row['mobile'] : "-"; ?></td>

                                <td><?php
                                    if($row['isActive'] == 1){?>
                                        <span class="btn btn-primary btn-flat btn-xs" onclick="userinactive(<?= $row['userID'] ?>)" id="useractive<?= $row['userID'] ?>" >Active</span>
                                        <span class="btn btn-danger btn-flat btn-xs hidden d-none" onclick="useractive(<?= $row['userID'] ?>)" id="userinactive_<?= $row['userID'] ?>">Inactive</span>
                                   <?php }else{ ?> 
                                        <span class="btn btn-danger btn-flat btn-xs" onclick="useractive(<?= $row['userID'] ?>)" id="userinactive_<?= $row['userID'] ?>">Inactive</span>
                                        <span class="btn btn-primary btn-flat btn-xs d-none"  onclick="userinactive(<?= $row['userID'] ?>)" id="useractive<?= $row['userID'] ?>">Active</span>
                                   <?php } ?>
                                </td>
                                <td class="">
                                    <a href="<?= base_url('admin/employee/view/' . $row['userID']); ?>" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                                    <a href="<?= base_url('admin/employee/edit/' . $row['userID']); ?>" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                    <a href="<?= base_url('admin/employee/del/' . $row['userID']); ?>" class="btn btn-danger btn-sm <?= ($row['userRole'] == 3) ? 'disabled' : '' ?>"><i class="fa fa-trash"></i></a>
                                </td>

                            </tr>

                            <?php  endforeach;

                                } ?>

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</div> 

