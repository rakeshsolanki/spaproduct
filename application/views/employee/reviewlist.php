<div class="row">

    <div class="col-12">

        <div class="card">
        <div class="card-body bg-info">
                <h4 class="text-white card-title"> Reviews </h4>
            </div>
            <div class="card-body">
                
                <div class="table-responsive">

                    <table id="myTable" class="table table-bordered table-striped">

                        <thead>

                            <tr>

                                <th>Review ID</th>
                                <th>User Name</th>
                                <th>Employee Name</th>
                                <th>Review</th>
                                <th>Rating</th>
                                <th>Status</th>
                                <th width= "150">Option</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($reviewList)) {
                                foreach ($reviewList as $row) : ?>
                            <tr>
                                <td><?= $row['reviewID']; ?></td>
                                <td >
                                    <a href="<?php echo base_url('admin/users/edit/'.$row['userID']); ?>"><?php echo $this->user->getUsername($row['userID']); ?></a>
                                </td>
                                <td ><a href="<?php echo base_url('admin/employee/view/'.$row['employeeID']); ?>"><b><?= !empty($this->user->getUsername($row['employeeID'])) ? $this->user->getUsername($row['employeeID']) : "-"; ?></b></a></td>

                                <td><?= (isset($row['review'])) ? $row['review'] : "-"; ?></td>
                                <td>
                                    <?php
                                    if(isset($row['rating']) && $row['rating']!=""){
                                        for($i=0; $i<$row['rating']; $i++){ ?>
                                            <i class="mdi mdi-star text-success"></i>
                                        <?php }
                                    } ?>
                                    
                                </td>


                                <td><?php //echo (isset($row['isActive']) && $row['isActive'] == 1) ? "Active" : "Inactive"; 
                                    if($row['isActive'] == 1){?>
                                        <span class="btn btn-primary btn-flat btn-xs" >Active</span>
                                   <?php }else{ ?> 
                                        <span class="btn btn-danger btn-flat btn-xs" >Inactive</span>
                                   <?php } ?>
                                </td>

                                <td class="">
                                    <a href="<?= base_url('admin/review/del/' . $row['reviewID']); ?>" class="btn btn-danger btn-sm "><i class="fa fa-trash"></i></a>
                                </td>

                            </tr>

                            <?php  endforeach;

                                } ?>

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</div> 

