<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body bg-info">
                    <h4 class="text-white card-title"> Profile </h4>
                </div>
                <div class="card-body">
                    <div class="box-body my-form-body">
                        <form class="" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-4 form-group">
                                    <label for="name" class="control-label">Name</label>
                                    <input type="text" name="name" value="<?php if (isset($user['name'])) { echo $user['name'];}else{ echo set_value('name'); } ?>" class="form-control form-control-line" id="name" placeholder="">
                                </div>

                                <div class="col-sm-4 form-group">
                                    <label for="branch" class="control-label">Branch Name</label>
                                    <input type="text" name="branch" value="<?php if (isset($user['branch'])) { echo $user['branch']; }else{ echo set_value('branch'); } ?>" class="form-control form-control-line" id="branch" placeholder="">
                                </div>
                                
                                <div class="col-sm-4 form-group">
                                    <label for="dob" class="control-label">DOB</label>
                                    <input type="text" name="dob" class="form-control datePicker" placeholder="" id="dob" value="<?php if (isset($user['dob'])) { echo $user['dob']; } ?>" />
                                </div>

                                <div class="col-sm-4 form-group">
                                    <label for="email" class="control-label">Email</label>
                                    <input type="email" name="email" value="<?php if (isset($user['email'])) { echo $user['email']; }else{ echo set_value('email'); } ?>" class="form-control" id="email" placeholder="">
                                </div>

                                <div class="col-sm-4 form-group">
                                    <label for="mobile" class="control-label">Mobile</label>
                                    <input type="number" name="mobile" value="<?php if (isset($user['mobile'])) { echo $user['mobile']; }else{ echo set_value('mobile'); } ?>" class="form-control" id="mobile" placeholder="">
                                </div>

                                <div class="col-sm-12 form-group">
                                    <input type="submit" name="submit" value="Save" class="btn btn-info" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php if(!empty($userRole==3)){ ?>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Yearly Income</h4>
                        <form id="frmGraph">
                            <div class="row">
                                <div class="col-sm-3 form-group">
                                    <select name="graphYear" id="graphYear" class="form-control">
                                        <option value="">Select Year</option>
                                        <?php
                                        $currYear = date('Y');
                                        for($i=2018;$i<=$currYear;$i++){ ?>
                                            <option value="<?php echo $i; ?>" <?php echo (isset($_GET['graphYear']) && $_GET['graphYear']==$i)?"selected":""; ?>><?php echo $i; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </form>
                        <div id="bar-chart" class="chartsize"></div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</section>

<script src="<?php echo $this->config->item('admin_assets_path'); ?>plugins/echarts/echarts-all.js"></script>
<script>
;(function ($, window, document, undefined) {
"use strict";
$('document').ready(function () {

    $('.datePicker').bootstrapMaterialDatePicker({
        format: 'DD-MM-YYYY',
        time: false,
        date: true
    });
    
    var incomeJson = <?php echo (isset($yearIncomeJson))?$yearIncomeJson:[]; ?>;
    // console.log(incomeJson);
    var myChart = echarts.init(document.getElementById('bar-chart'));
    var option = {
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: ['Income']
        },
        toolbox: {
            show: true,
            feature: {
                // magicType: { show: true, type: ['line', 'bar'] },
                // restore: { show: true },
                // mark : {show: true},
                saveAsImage: { show: true }
            }
        },
        color: ["#55ce63", "#009efb"],
        calculable: true,
        xAxis: [{
            type: 'category',
            data: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']
        }],
        yAxis: [{
            type: 'value'
        }],
        series: [{
                name: 'Income',
                type: 'bar',
                data: incomeJson,
                /*markPoint: {
                    data: [
                        { type: 'all', name: 'All' },
                        { type: 'max', name: 'Max' },
                        { type: 'min', name: 'Min' }
                    ]
                },*/
            }
        ]
    };
    
    // use configuration item and data specified to show chart
    myChart.setOption(option, true), $(function() {
        function resize() {
            setTimeout(function() {
                myChart.resize()
            }, 100)
        }
        // $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
    });
    
    /*option2 = {
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: ['Income', 'Expenses']
        },
        toolbox: {
            show: true,
            feature: {
                // magicType: { show: true, type: ['line', 'bar'] },
                // restore: { show: true },
                saveAsImage: { show: true }
            }
        },
        color: ["#55ce63", "#009efb"],
        calculable: true,
        xAxis: [{
            type: 'category',
            data: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']
        }],
        yAxis: [{
            type: 'value'
        }],
        series: [{
                name: 'Income',
                type: 'bar',
                data: [2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3],
                markPoint: {
                    data: [
                        { type: 'max', name: 'Max' },
                        { type: 'min', name: 'Min' }
                    ]
                },
                markLine: {
                    data: [
                        { type: 'average', name: 'Average' }
                    ]
                }
            },
            {
                name: 'Expenses',
                type: 'bar',
                data: [2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3],
                markPoint: {
                    data: [
                        { name: 'The highest year', value: 182.2, xAxis: 7, yAxis: 183, symbolSize: 18 },
                        { name: 'Year minimum', value: 2.3, xAxis: 11, yAxis: 3 }
                    ]
                },
                markLine: {
                    data: [
                        { type: 'average', name: 'Average' }
                    ]
                }
            }
        ]
    };*/

    $("#graphYear").change(function() {
        $('#frmGraph').submit();
    });
    
});
})(jQuery, window, document);
</script> 

