<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body bg-info">
                    <h5 class="text-white card-title"><i class="fa fa-filter"></i> Filter Invoice Date Wise </h5>
                </div>
                <div class="card-body">
                    <div class="box-body my-form-body">
                        <form class="" action="<?php echo base_url('report/exportInvoice'); ?>" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label for="dob" class="control-label">Start Date*</label>
                                    <div class="input-group">
                                        <input type="text" name="invoiceStartDate" class="form-control datePicker" placeholder="" id="invoiceStartDate" required>
                                        <div class="input-group-append">
                                            <button class="btn btn-info" type="button"><i class="icon-calender"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="dob" class="control-label">End Date*</label>
                                    <div class="input-group">
                                        <input type="text" name="invoiceEndDate" class="form-control datePicker" placeholder="" id="invoiceEndDate">
                                        <div class="input-group-append">
                                            <button class="btn btn-info" type="button"><i class="icon-calender"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 text-center form-group">
                                    <input type="submit" value="Export Seleceted" class="btn btn-info" />
                                    <a href="<?php echo base_url('report/exportInvoice'); ?>" class="btn btn-info">Export All Invoice <i class="fa fa-arrow-circle-down"></i></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-body bg-info">
                    <h5 class="text-white card-title"><i class="fa fa-filter"></i> Filter Membership Date Wise </h5>
                </div>
                <div class="card-body">
                    <div class="box-body my-form-body">
                        <form class="" action="<?php echo base_url('report/exportMemberShip'); ?>" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label for="dob" class="control-label">Start Date*</label>
                                    <div class="input-group">
                                        <input type="text" name="membershipStartDate" class="form-control datePicker" placeholder="" id="membershipStartDate" required>
                                        <div class="input-group-append">
                                            <button class="btn btn-info" type="button"><i class="icon-calender"></i></button>
                                        </div>
                                    </div> 
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="dob" class="control-label">End Date*</label>
                                    <div class="input-group">
                                        <input type="text" name="membershipEndDate" class="form-control datePicker" placeholder="" id="membershipEndDate">
                                        <div class="input-group-append">
                                            <button class="btn btn-info" type="button"><i class="icon-calender"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 text-center form-group">
                                    <input type="submit" value="Export Selected" class="btn btn-info" />
                                    <a href="<?php echo base_url('report/exportMemberShip'); ?>" class="btn btn-info">Export All Membership <i class="fa fa-arrow-circle-down"></i></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-body bg-info">
                    <h5 class="text-white card-title"><i class="fa fa-filter"></i> Filter General Expense Date Wise </h5>
                </div>
                <div class="card-body">
                    <div class="box-body my-form-body">
                        <form class="" action="<?php echo base_url('report/exportGeneralExpense'); ?>" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label for="dob" class="control-label">Start Date*</label>
                                    <div class="input-group">
                                        <input type="text" name="genExpStartDate" class="form-control datePicker" placeholder="" id="genExpStartDate" required />
                                        <div class="input-group-append">
                                            <button class="btn btn-info" type="button"><i class="icon-calender"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="dob" class="control-label">End Date*</label>
                                    <div class="input-group">
                                        <input type="text" name="genExpEndDate" class="form-control datePicker" placeholder="" id="genExpEndDate" />
                                        <div class="input-group-append">
                                            <button class="btn btn-info" type="button"><i class="icon-calender"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 text-center form-group">
                                    <input type="submit" value="Export Seleceted" class="btn btn-info" />
                                    <a href="<?php echo base_url('report/exportGeneralExpense'); ?>" class="btn btn-info">Export All Expense <i class="fa fa-arrow-circle-down"></i></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-body bg-info">
                    <h5 class="text-white card-title"><i class="fa fa-filter"></i> Filter Laundry Expense Date Wise </h5>
                </div>
                <div class="card-body">
                    <div class="box-body my-form-body">
                        <form class="" action="<?php echo base_url('report/exportLaundryExpense'); ?>" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label for="dob" class="control-label">Start Date*</label>
                                    <div class="input-group">
                                        <input type="text" name="lanExpStartDate" class="form-control datePicker" placeholder="" id="lanExpStartDate" required />
                                        <div class="input-group-append">
                                            <button class="btn btn-info" type="button"><i class="icon-calender"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="dob" class="control-label">End Date*</label>
                                    <div class="input-group">
                                        <input type="text" name="lanExpEndDate" class="form-control datePicker" placeholder="" id="lanExpEndDate" />
                                        <div class="input-group-append">
                                            <button class="btn btn-info" type="button"><i class="icon-calender"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 text-center form-group">
                                    <input type="submit" value="Export Selected" class="btn btn-info" />
                                    <a href="<?php echo base_url('report/exportLaundryExpense'); ?>" class="btn btn-info">Export All Expense <i class="fa fa-arrow-circle-down"></i></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

