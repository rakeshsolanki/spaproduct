<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body bg-info">
                    <h4 class="text-white card-title"> Treatment Form </h4>
                </div>
                <div class="card-body">
                    <div class="box-body my-form-body">
                        <?php
                        $action = base_url('treatments/add');
                        if (isset($rowData['treatmentID']) && $rowData['treatmentID'] != "") {
                            $action = base_url('treatments/edit/' . $rowData['treatmentID']);
                        } ?>
                        <form class="row" action="<?php echo $action; ?>" class="" method="post" enctype="multipart/form-data">
                            <div class="col-sm-4 form-group">
                                <label for="name" class=" control-label">Therapy Name</label>
                                <input type="text" name="name" value="<?php if (isset($rowData['name'])) { echo $rowData['name'];}else{ echo set_value('name'); } ?>" class="form-control form-control-line" id="name" placeholder="">
                            </div>
                            <div class="col-sm-4 form-group">
                                <label for="duration" class="control-label">Duration (in Minute)</label>
                                <input type="number" name="duration" value="<?php if (isset($rowData['duration'])) { echo $rowData['duration']; }else{ echo set_value('duration'); } ?>" class="form-control" id="duration" placeholder="">
                            </div>
                            <div class="col-sm-12 form-group">
                                <input type="submit" name="submit" value="Save" class="btn btn-info" />
                                <a href="<?= base_url('treatments'); ?>" class="btn btn-danger"> Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
;(function ($, window, document, undefined) {
$('document').ready(function () {
    "use strict";
    $('#dob').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD',
        time: false,
        date: true
    });
});
})(jQuery, window, document);
</script> 

