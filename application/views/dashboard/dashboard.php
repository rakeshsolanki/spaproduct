<section class="content">
    <div class="row" id="dashboard">
        <div class="col-lg-3 col-md-6">
            <a href="<?php echo base_url() . 'users'; ?>">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="round round-lg align-self-center round-success"><i class="fas fa-user-circle"></i></div>
                            <div class="m-l-10 align-self-center">
                                <h3 class="m-b-0 font-light"><?php echo (isset($totalManagerCount)) ? $totalManagerCount : 0; ?></h3>
                                <h5 class="m-b-0">Total Manager</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-6">
            <a href="<?php echo base_url() . 'employee'; ?>">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="round round-lg align-self-center round-success"><i class="fas fa-user-md"></i></div>
                            <div class="m-l-10 align-self-center">
                                <h3 class="m-b-0 font-light"><?php echo (isset($totalEmployeeCount)) ? $totalEmployeeCount : 0; ?></h3>
                                <h5 class="m-b-0">Total Employee</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-6">
            <a href="<?php echo base_url() . 'customers'; ?>">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="round round-lg align-self-center round-success"><i class="fas fa-users"></i></div>
                            <div class="m-l-10 align-self-center">
                                <h3 class="m-b-0 font-light"><?php echo (isset($totalCustomersCount)) ? $totalCustomersCount : 0; ?></h3>
                                <h5 class="m-b-0">Total Members</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-6">
            <a href="<?php echo base_url() . 'invoice'; ?>">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="round round-lg align-self-center round-success"><i class="fas fa-chart-pie"></i></div>
                            <div class="m-l-10 align-self-center">
                                <h3 class="m-b-0 font-light"><?php echo (isset($totalInvoiceCount)) ? $totalInvoiceCount : 0; ?></h3>
                                <h5 class="m-b-0">Total Invoice</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>

    <div class="row" id="">
        <?php
        $today = date('Y-m-d');
        $yesterday = date('Y-m-d', strtotime("-1 days"));
        if (!empty($managerList)) {
            foreach ($managerList as $manager) {
                $branchName = (isset($manager['branch'])) ? $manager['branch'] : "";
                $todayMemberCustomer = count($this->Service->getMembershipinvoice($manager['userID'], $today));
                $todayCustomer = count($this->Service->get_all(TBL_INVOICE, '*', array('managerID' => $manager['userID'], 'invoiceDate' => $today, 'membershipID' => '')));
                $todayIncome = $this->user->getTotalIncome('', true, $manager["userID"]);
                $totalIncome = $this->user->getTotalIncome('', false, $manager["userID"]);
        ?>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body bg-info">
                            <a href="<?= base_url('invoice?branch=' . $manager["userID"]); ?>">
                                <h4 class="text-white card-title text-center">Branch of <?= $branchName; ?> </h4>
                            </a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table stylish-table">
                                    <tr class="">
                                        <th>Total Invoice</th>
                                        <th class="text-right">
                                            <h4><span class="label label-info"><?php echo ($todayMemberCustomer + $todayCustomer); ?></span></h4>
                                        </th>
                                    </tr>
                                    <tr class="">
                                        <th>Today Membership Customer</th>
                                        <th class="text-right">
                                            <h4><span class="label label-success"><?php echo $todayMemberCustomer; ?></span></h4>
                                        </th>
                                    </tr>
                                    <tr class="">
                                        <th>Today Payee Customer</th>
                                        <th class="text-right">
                                            <h4><span class="label label-success"><?php echo $todayCustomer; ?></span></h4>
                                        </th>
                                    </tr>
                                    <tr class="">
                                        <th>Today Income</th>
                                        <th class="text-right">
                                            <h4><span class="label label-danger"><?php echo $todayIncome; ?></span></h4>
                                        </th>
                                    </tr>
                                    <tr class="">
                                        <th>Total Income</th>
                                        <th class="text-right">
                                            <h4><span class="label label-danger"><?php echo $totalIncome; ?></span></h4>
                                        </th>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        <?php }
        } ?>
    </div>
</section>

<script>
 
 function clientsearch(dd){
    $.ajax({
            type: "post",
            url: "<?= base_url('dashboard'); ?>",
            data: {mobile : dd},
            success: function(result){
                // alert(result);
                // $('#ipclean_'+changeIP).hide();
            }
        })
    // alert(dd);
 }
 
</script>