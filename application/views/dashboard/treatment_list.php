<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body bg-info">
                <h4 class="text-white card-title">
                <a class="btn btn-info" href="<?= base_url('treatments/add'); ?>"><i class="fa fa-plus"></i> Add</a> &nbsp; &nbsp; &nbsp; 
                    Treatment List
                </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="myTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#ID</th>
                                <th>Name</th>
                                <th>Duration</th>
                                <th>Status</th>
                                <th>Option</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($resultList)) {
                                foreach ($resultList as $key => $row) : ?>
                                    <tr  id="lnr_<?= $row['treatmentID']; ?>">
                                        <td><?= $row['treatmentID']; ?></td>
                                        <td><?= (isset($row['name'])) ? $row['name'] : "-"; ?></td>
                                        <td><?= (isset($row['duration'])) ? $row['duration'] : "-"; ?></td>
                                        <td><?php 
                                            if($row['isActive'] == 1){?>
                                                <span class="btn btn-primary btn-flat btn-xs" onclick="inactive(<?= $row['treatmentID'] ?>)" id="active_<?= $row['treatmentID'] ?>" >Active</span>
                                                <span class="btn btn-danger btn-flat btn-xs hidden d-none"  onclick="active(<?= $row['treatmentID'] ?>)" id="inactive_<?= $row['treatmentID'] ?>">Inactive</span>
                                        <?php }else{ ?> 
                                                <span class="btn btn-danger btn-flat btn-xs" onclick="active(<?= $row['treatmentID'] ?>)" id="inactive_<?= $row['treatmentID'] ?>">Inactive</span>
                                                <span class="btn btn-primary btn-flat btn-xs d-none "  onclick="inactive(<?= $row['treatmentID'] ?>)" id="active_<?= $row['treatmentID'] ?>">Active</span>
                                        <?php } ?>
                                        </td>
                                        
                                        <td class="">
                                            <a href="<?= base_url('treatments/edit/' . $row['treatmentID']); ?>" class="btn btn-info btn-sm "><i class="fa fa-edit"></i></a>
                                            <span  onclick="deletedata('<?= $row['treatmentID']; ?>');" class="btn btn-sm btn-danger "><i class="fa fa-trash"></i></span>
                                        </td>
                                    </tr>
                                <?php endforeach;
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> 

<script>
function deletedata(treatmentid) {
  var r = confirm("Are you sure delete this data!");
  if (r == true) {
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('treatments/delete') ?>",
        data:{treatmentid:treatmentid},
        success: function(data){
            if(data)
            {
                $('#lnr_'+treatmentid).hide();
            }
        }
    });
  } 
}

function active(treatmentid){
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('dashboard/treatmentIsactive') ?>",
        data:{status:1,treatmentid:treatmentid},
        success: function(res){
            if(res==1)
            {
                $('#active_'+treatmentid).removeClass('d-none');
                $('#inactive_'+treatmentid).addClass('d-none');
            }
        }
    });
}

function inactive(treatmentid){
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('dashboard/treatmentIsactive') ?>",
        data:{status:0,treatmentid:treatmentid},
        success: function(res){
            if(res==1)
            {
                $('#inactive_'+treatmentid).removeClass('d-none');
                $('#active_'+treatmentid).addClass('d-none');
            }
        }
    });
}
</script>