
<div class="row">
    <div class="col-lg-4 col-md-6">

        <div class="ribbon-wrapper card">
            <div class="ribbon ribbon-bookmark  ribbon-default">USER</div>
            <center class="">
                <?php if(!empty($user['picture'])){ ?>
                    <img src="<?php echo base_url(PROFILE.$user['picture']); ?>" class="img-circle" width="150" />
                <?php } ?>
                <h4 class="card-title m-t-10"><?php echo ($user['name']) ? $user['name'] : "" ?></h4>
            </center>
            <div>
                <hr>
            </div>
            <div class="card-body">
                <h6 class="text-muted"><b>Email</b> : <?php echo ($user['email']) ? $user['email'] : "" ?></h6>
                <hr>
                <h6 class="text-muted"><b>Phone</b> : <?php echo ($user['mobile']) ? $user['mobile'] : "" ?></h6>
                <hr>
                <h6 class="text-muted"><b>Gender</b> : <?php echo ($user['gender']) ? $user['gender'] : "" ?></h6>
                <hr>
                <h6 class="text-muted"><b>DOB</b> : <?php echo ($user['dob']) ? $user['dob'] : "" ?></h6>
            </div>
        </div>
    </div>
</div>
