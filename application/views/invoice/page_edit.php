
<style>
    main {
      margin: 10px auto;
      padding: 20px;
    }
    @media only screen and (min-width: 680px) {
      main {
        width: 600px;
        box-shadow: 2px 2px 8px rgba(0, 0, 0, 0.4);
        margin-left: auto;
        margin-right: auto;
      }
    }
  </style>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body bg-info">
                    <h4 class="text-white card-title">
                     Invoice
                    </h4>
                </div>
                <div class="card-body">
                    
                    <div class="box-body my-form-body">
                        <?php
                        $currentDate = date("d-m-Y");
                        $action = base_url('invoice/add');
                        if (isset($rowData['invoiceID']) && $rowData['invoiceID'] != "") {
                            $action = base_url('invoice/edit/' . $rowData['invoiceID']);
                        } ?>
                        <form class="row myForm" action="<?php echo $action; ?>"  method="post" enctype="multipart/form-data">
                        <input type="hidden" name="branch" value="<?= isset($_GET['branch']) ? $_GET['branch'] :'';  ?>" >
                            <div class="col-sm-3 form-group">
                                <label for="invoiceType" class="control-label">Select Type</label>
                                <?php if(empty($rowData)){ ?>
                                    <select id="invoiceType" name="invoiceType" class="form-control form-control-line" onchange="checkInvoiceType()">
                                        <option value="0">Payee</option>
                                        <option value="1">Membership</option>
                                    </select>
                                <?php }elseif(empty($rowData['membershipID'])){  ?>
                                    <input class="form-control" type="text" value="Payee" readonly />
                                <?php }else{ ?>
                                    <input class="form-control" type="text" value="Membership" readonly />
                                <?php } ?>
                            </div>
                            <div class="col-sm-3 form-group d-none" id="membershipDiv" >
                                <label for="membershipID" class="control-label">Membership ID</label>
                                <input type="text" name="membershipID" onkeyup="getMembershipData()" value="<?php if (isset($rowData['membershipID'])) { echo $rowData['membershipID'];}else{ echo set_value('membershipID'); } ?>" class="form-control form-control-line" id="membershipID" placeholder="">
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="customerName" class="control-label">Client Name</label>
                                <input type="text" name="customerName" value="<?php if (isset($rowData['customerName'])) { echo $rowData['customerName'];}else{ echo set_value('customerName'); } ?>" class="form-control form-control-line" id="customerName" placeholder="">
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="customerMobile" class="control-label">Mobile No.</label>
                                <input type="number" name="customerMobile" value="<?php if (isset($rowData['customerMobile'])) { echo $rowData['customerMobile']; }else{ echo set_value('customerMobile'); } ?>" class="form-control" id="customerMobile" placeholder="">
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="inTime" class="control-label">In Time</label>
                                <div class='input-group mb-3'>
                                    <input type='text' name="inTime" value="<?php if (isset($rowData['inTime'])) { echo $rowData['inTime']; }else{ echo set_value('inTime'); } ?>" class="form-control timePicker" id="inTime" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <span class="ti-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                
                            </div>
                            
                            <div class="col-sm-3 form-group">
                                <label for="block" class="control-label">Block</label>
                                <input type="text" name="block" value="<?php if (isset($rowData['block'])) { echo $rowData['block']; }else{ echo set_value('block'); } ?>" class="form-control" id="block" placeholder="">
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="employeeID" class="control-label">Therapist</label>
                                <select name="employeeID[]" id="employeeID" class="form-control " >
                                    <option value="" >Select Therapist</option>
                                    <?php
                                    if(!empty($employeeList)){
                                        foreach($employeeList as $employee){ ?>
                                            <option value="<?php echo $employee['employeeID']; ?>" <?php echo (!empty($rowData['employeeID']) && ($rowData['employeeID'] == $employee['employeeID'])) ? "selected":""; ?>><?php echo $employee['name']; ?></option>
                                        <?php }
                                    } ?>
                                </select>
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="treatmentID" class="control-label">Treatment</label>
                                <select name="treatmentID[]" id="treatmentID" class="form-control">
                                    <option value="">Select Treatment</option>
                                    <?php $treatmentID = (isset($rowData['treatmentID']) && $rowData['treatmentID'])?$rowData['treatmentID']:"";
                                    if(!empty($treatmentList)){
                                        foreach($treatmentList as $treatment){ ?>
                                            <option class="treat_<?php echo $treatment['duration']; ?>" value="<?php echo $treatment['treatmentID']; ?>"<?= (isset($treatmentID) && $treatment['treatmentID'] == $treatmentID ) ? 'selected' :'' ?>><?php echo $treatment['name']; ?></option>
                                        <?php }
                                    } ?>
                                </select>
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="product" class="control-label">Product</label>
                                <select name="product[]" id="product" class="form-control " >
                                    <option value="" >Select Product</option>
                                    <?php //$productData = (isset($rowData['product']) && $rowData['product']) ? explode(',',$rowData['product']) :"";
                                    if(!empty($productList)){
                                        foreach($productList as $product){ ?>
                                            <option value="<?php echo $product['productID']; ?>"<?php echo (!empty($rowData['product']) && ($rowData['product'] == $product['productID'])) ? "selected":""; ?>><?php echo $product['name']; ?></option>
                                        <?php }
                                    } ?>
                                </select>
                            </div>
                            <input type="hidden" id="duration" name="duration" value="<?= (isset($rowData['duration']) && $rowData['duration'])?$rowData['duration']:"" ?>">

                            <div class="col-sm-3 form-group">
                                <label for="yslip" class="control-label">Slip Number</label>
                                <input type="text" name="yslip" value="<?php if (isset($rowData['yslip'])) { echo $rowData['yslip']; }else{ echo set_value('yslip'); } ?>" class="form-control" id="yslip" placeholder="">
                            </div>
                            <div class="col-sm-3 form-group Amount">
                                <label for="amount" class="control-label">Amount</label>
                                <input type="text" name="amount" value="<?php if (isset($rowData['amount'])) { echo $rowData['amount']; }else{ echo set_value('amount'); } ?>" class="form-control" id="amount" placeholder="">
                            </div>
                            <div class="col-sm-3 form-group paymentMode">
                                <label for="paymentMode" class="control-label">Payment Mode</label>
                                <?php $payModetData = (isset($rowData['payMode']) && !empty($rowData['payMode'])) ? explode(',',$rowData['payMode']) :""; ?>
                                <select class="form-control select2" id="paymode" name="payMode[]" multiple>
                                    <option value="" disabled>Select Payment Mode</option>
                                        <option value="CASH"<?php echo (!empty($payModetData) && in_array("CASH" , $payModetData)) ? "selected":""; ?>>CASH</option>
                                        <option value="CARD"<?php echo (!empty($payModetData) && in_array("CARD" , $payModetData)) ? "selected":""; ?>>CARD</option> 
                                        <option value="PAYTM"<?php echo (!empty($payModetData) && in_array("PAYTM" , $payModetData)) ? "selected":""; ?>>PAYTM</option>
                                        <option value="PHONE PAY"<?php echo (!empty($payModetData) && in_array("PHONE PAY" , $payModetData)) ? "selected":""; ?>>PHONE PE</option>
                                        <option value="GOOGLE PAY"<?php echo (!empty($payModetData) && in_array("GOOGLE PAY" , $payModetData)) ? "selected":""; ?>>GOOGLE PAY</option>
                                        <option value="NET BANKING"<?php echo (!empty($payModetData) && in_array("NET BANKING" , $payModetData)) ? "selected":""; ?>>NET BANKING</option>
                                        <option value="OTHER"<?php echo (!empty($payModetData) && in_array("OTHER" , $payModetData)) ? "selected":""; ?>>OTHER</option>
                                </select>
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="invoiceDate" class="control-label">Invoice Date</label>
                                <input type="text" name="invoiceDate"  id="invoiceDate" class="form-control mydatepicker"  placeholder="" value="<?php if (isset($rowData['invoiceDate'])) { echo date('d-m-Y',strtotime($rowData['invoiceDate'])) ; }else{ echo $currentDate; } ?>">
                            </div>
                            
                            <div class="col-sm-12 form-group">
                                <input type="submit" name="submit" value="Save" class="btn btn-info" />
                                <?php if(isset($_GET['branch']) && $_GET['branch'] != ''){
                                    $url = base_url('invoice?branch='.$_GET['branch']);
                                }else{
                                    $url = base_url('invoice');
                                }  ?>
                                <a href="<?=$url; ?>" class="btn btn-danger">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
   
<?php if (!empty($rowData['membershipID'])): ?>

    $('#membershipDiv').show();
    $('.paymentMode').hide();
    $('.Amount').hide();
    $("#duration").attr("disabled", "");
    $("#customerMobile").attr("readonly","");
    $("#membershipID").attr("readonly","");
    $("#customerName").attr("readonly", ""); 
<?php endif; ?>

function checkInvoiceType(){
    var type = $('#invoiceType').val();
    if(type==1){
        $('#membershipDiv').show();
        $('.paymentMode').hide();
        $('.Amount').hide();
    }else{
        $('#membershipDiv').hide();
        $('.paymentMode').show();
        $('.Amount').show();
        $("#customerName").removeAttr("readonly"); 
        $("#customerMobile").removeAttr("readonly"); 
        $("#duration").removeAttr("disabled");
    }
}
function getMembershipData(){
    var membership = $('#membershipID').val();
    if(membership){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('invoice/getMembershipData') ?>",
            data: {membership: membership},
            success: function (response) {
                var memberData = JSON.parse(response);
                if(memberData.name){
                    $('#customerName').val(memberData.name);
                    // $("#customerName").attr("readonly", ""); 
                }
                if(memberData.mobile){
                    $('#customerMobile').val(memberData.mobile);
                    // $("#customerMobile").attr("readonly", "");
                }
                if(memberData.duration){
                    var minite = memberData.duration;
                    if(minite==45){
                        $('.treat_'+60).remove();
                    }else{
                        $('.treat_'+45).remove();
                    }
                    $('#duration').val(memberData.duration);
                    $("#duration").attr("disabled", "");
                    
                    var duration = document.getElementById('duration');
                    for(var i = 0, j = duration.options.length; i < j; ++i) {
                        if(duration.options[i].innerHTML === memberData.duration) {
                            duration.selectedIndex = i;
                            break;
                        }
                    }
                }
            }
        });
    }
}
;(function ($, window, document, undefined) {
"use strict";
$('document').ready(function () {

    $( ".myForm" ).on( "submit", function( event ) {
        $('#duration').removeAttr('disabled');
    });
    $('.datePicker').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD',
        time: false,
        date: true
    });
    $('.timePicker').timepicker();
    
    jQuery('.mydatepicker, #datepicker').datepicker({
        format: 'd-m-yyyy',
        defaultTime : false
    });

    $("#treatmentID").change(function() {
        var treat = $(this).children(":selected").attr("class");
        if(treat){
            var duration= treat.slice(-2);
            $('#duration').val(duration);
        }
    });
   

});
})(jQuery, window, document);
</script>