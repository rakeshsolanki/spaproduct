<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body bg-info">
                <h4 class="text-white card-title">
                <a class="btn btn-info" href="<?= base_url('invoice/add'); ?>"><i class="fa fa-plus"></i> Add</a>
                    &nbsp; &nbsp; Invoice List
                    <div class="pull-right">
                        <h4 class="card-title"></h4>
                    </div>
                </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="myTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="80" >Date</th>
                                <th>Membership</th>
                                <th>Name</th>
                                <th>Contact</th>
                                <th>Products</th>
                                <th>Treatment</th>
                                <th>Block</th>
                                <th>Duration</th>
                                <th>In-Out Time</th>
                                <th>Therapist</th>
                                <th>Slip NO. </th>
                                <th>Amount </th>
                                <th>Branch</th>
                                <?php $userRole = $this->session->userdata('user_type');
                                    if($userRole == 3){ ?>
                                <th>Action</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($resultList)) {
                                foreach ($resultList as $key => $row) : 
                                    $product = (isset($row['product'])) ? $this->Service->getProductName(explode(',',$row['product']),true) : "-";
                                   
                                    $employeeName = $this->Service->getEmployeeName(explode(',',$row['employeeID']),true);
                                    $treament = "Treament";
                                    $inTime = (isset($row['inTime'])) ? $row['inTime'] : "00:00";
                                    $outTime = (isset($row['outTime'])) ? $row['outTime'] : "00:00";
                                    ?>
                                    <tr id="lnr_<?= $row['invoiceID']; ?>">
                                        <td><?= (isset($row['invoiceDate'])) ? date('d-m-Y',strtotime( $row['invoiceDate'])) : "-"; ?></td>
                                        <td><?= (isset($row['membershipID']) && $row['membershipID']!="") ? $row['membershipID'] : "PAYEE"; ?></td>
                                        <td><?= (isset($row['customerName'])) ? $row['customerName'] : "-"; ?></td>
                                        <td><?= (isset($row['customerMobile'])) ? $row['customerMobile'] : "-"; ?></td>
                                        <td><?php foreach($product as $pro){
                                            echo $pro['name'] .'<br>';
                                        } ?></td>
                                        <td class="uppercaseFont"><?= (isset($row['treatmentID'])) ? $this->Service->getTreatmentName($row['treatmentID']) : "-"; ?></td>
                                        
                                        <td><?= (isset($row['block'])) ? $row['block'] : "-"; ?></td>
                                        <td><?= (isset($row['duration'])) ? $row['duration'].' Min' : ""; ?></td>
                                        <td><?= $inTime; ?> - <?= $outTime; ?></td>
                                        <td class="uppercaseFont">
                                            <?php
                                            if(!empty($employeeName)){
                                                foreach($employeeName as $empe){
                                                    echo $empe['name']  .'<br>';
                                                }
                                            }
                                            (isset($row['employeeID'])) ?   : "-"; ?>
                                        </td>
                                        <td>
                                            <?= (isset($row['yslip'])) ? $row['yslip'] : "-"; ?>
                                        </td>
                                        <td><?= (!empty($row['amount']) && $row['amount'] != '0') ? $row['amount'].'<i class="mdi mdi-currency-inr"></i>' : '<span class="label label-danger font-weight-100">MEMBER</span>' ; ?>
                                        <?php if(isset($row['payMode']) && $row['payMode']!="" && $row['amount']!=0){ ?><br><label class="label label-info"><?= (isset($row['payMode'])) ? $row['payMode'] : ""; ?></label><?php } ?></td>
                                        <td><?= (isset($row['managerID'])) ? $this->Service->getBranceName($row['managerID']) : "-"; ?></td>
                                        
                                        <?php $userRole = $this->session->userdata('user_type');
                                        if($userRole == 3){ ?>
                                            <td class="">
                                                <?php  
                                                if(isset($_GET['branch']) && $_GET['branch'] != ''){
                                                    $url = base_url('invoice/edit/' . $row['invoiceID'].'?branch='.$_GET['branch']);
                                                }else{
                                                    $url = base_url('invoice/edit/' . $row['invoiceID']);
                                                }  ?>
                                                <a href="<?= $url; ?>" class="btn btn-info btn-sm "><i class="fa fa-edit"></i></a>
                                                <span  onclick="deleteinvoicedata('<?= $row['invoiceID']; ?>');" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></span>
                                            </td>
                                        <?php } ?>
                                    </tr>
                                <?php endforeach;
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> 

<script>


</script>