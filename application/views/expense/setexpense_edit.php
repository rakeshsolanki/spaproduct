<section class="content">

    <div class="row">

        <div class="col-md-12">

            <div class="card">
                <div class="card-body bg-info">
                    <h4 class="text-white card-title">
                        Expense Value
                    </h4>
                </div>
                <div class="card-body">

                    <div class="box-body my-form-body">
                        
                        <form class="row" action="<?php echo base_url('expense/setExpense'); ?>" class="" method="post" enctype="multipart/form-data">
                            <div class="col-sm-2 form-group">
                                <label for="tea" class=" control-label">RS PER TEA :</label>
                                <input type="number" name="tea" value="<?php if (isset($resultList['tea'])) { echo $resultList['tea'];}else{ echo set_value('tea'); } ?>" class="form-control form-control-line" id="expense" expenseholder="">
                            </div>
                            
                            
                            <div class="col-sm-2 form-group">
                                <label for="waterJug" class=" control-label">RS PER WATER JUG :</label>
                                <input type="number" name="waterJug" value="<?php if (isset($resultList['waterJug'])) { echo $resultList['waterJug'];}else{ echo set_value('waterJug'); } ?>" class="form-control form-control-line" id="lat" expenseholder="">
                             </div>

                            <div class="col-sm-2 form-group">
                                <label for="towel" class=" control-label"> RS PER TOWEL : </label>
                                <input type="number" name="towel" value="<?php if (isset($resultList['towel'])) { echo $resultList['towel'];}else{ echo set_value('towel'); } ?>" class="form-control form-control-line" id="lat" expenseholder="">
                            </div>

                            <div class="col-sm-2 form-group">
                                <label for="napkin" class="control-label"> RS PER NAPKIN : </label>
                                <input type="number" name="napkin" value="<?php if (isset($resultList['napkin'])) { echo $resultList['napkin'];}else{ echo set_value('napkin'); } ?>" class="form-control form-control-line" id="lang" expenseholder="">
                             </div>
                            
                            <div class="col-sm-2 form-group">
                                <label for="dress" class=" control-label">RS PER DRESS :</label>
                                <input type="number" name="dress" value="<?php if (isset($resultList['dress'])) { echo $resultList['dress'];}else{ echo set_value('dress'); } ?>" class="form-control form-control-line" id="dress" expenseholder="">
                             </div>
                            <div class="col-sm-2 form-group">
                                <label for="bedsheet" class=" control-label">RS PER BEDSHEET :</label>
                                <input type="number" name="bedsheet" value="<?php if (isset($resultList['bedsheet'])) { echo $resultList['bedsheet'];}else{ echo set_value('bedsheet'); } ?>" class="form-control form-control-line" id="bedsheet" expenseholder="">
                             </div>
                            <div class="col-sm-2 form-group">
                                <label for="footmate" class=" control-label">RS PER FOOTMATE :</label>
                                <input type="number" name="footmate" value="<?php if (isset($resultList['footmate'])) { echo $resultList['footmate'];}else{ echo set_value('footmate'); } ?>" class="form-control form-control-line" id="footmate" expenseholder="">
                            </div>

                            <div class="col-sm-12 form-group">
                                <div class="">
                                    <input type="submit" name="submit" value="Update" class="btn btn-info" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 



