<div class="row">

    <div class="col-12">

        <div class="card">
            <div class="card-body bg-info">
                <h4 class="text-white card-title">
                    <a class="btn btn-info" href="<?= base_url('expense/monthlyExpense/add'); ?>"><i class="fa fa-plus"></i> Add</a>&nbsp; &nbsp; &nbsp;
                    Monthly  Expenses List
                </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="myTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="40">Sr.No</th>
                                <th width="70">Date</th>
                                <th width="100">Voucher No.</th>
                                <th width="150">Type</th>
                                <th>Other Description</th>
                                <th width="70">Amount</th>
                                <th width="100">Option</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($resultList)) {
                                foreach ($resultList as  $key => $row) : 
                                    $rowData = $this->Service->get_row(TBL_EXPENSE_TYPE,'type',array('typeID'=>$row['type']));
                                ?>
                                    <tr id="lnr_<?= $row['expID']; ?>">
                                        <td><?= $key+1; ?></td>
                                        <td ><?= isset($row['expDate']) ? $this->Service->dateFormating($row['expDate']) :''; ?></td>
                                        <td ><?= isset($row['voucher']) ? $row['voucher'] :''; ?></td>
                                        <td ><?= isset($rowData['type']) ? $rowData['type'] :''; ?></td>
                                        <td ><?= isset($row['desc']) ? $row['desc'] :''; ?></td>
                                        <td ><i class="mdi mdi-currency-inr"></i><?= (isset($row['amount'])) ? $row['amount'] : "-"; ?></td>
                                        <td class="">
                                            <span  onclick="deletedata('<?= $row['expID']; ?>');" class="btn btn-sm btn-danger "><i class="fa fa-trash"></i></span>
                                            <a href="<?= base_url('expense/monthlyExpense/edit/' . $row['expID']); ?>" class="btn btn-sm btn-info "><i class="fa fas fa-pencil-alt"></i></a>
                                            
                                        </td>
                                    </tr>
                                <?php endforeach;
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> 

<script>
;(function ($, window, document, undefined) {
    "use strict";
    $('document').ready(function () {
        $('#mdate').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD',
            time: false,
            date: true
        });
    });
})(jQuery, window, document);

function deletedata(expid) {
//   alert(expid);
  var r = confirm("Are you sure delete this data!");
  if (r == true) {
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('expense/monthlyExpense/delete') ?>",
        data:{expid:expid},
        success: function(data){
            if(data)
            {
                $('#lnr_'+expid).hide();
            }
        }
    });
  } 
}

</script>
