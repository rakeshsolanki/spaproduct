<section class="content">

    <div class="row">

        <div class="col-md-12">

            <div class="card">
                <div class="card-body bg-info">
                    <h4 class="text-white card-title">
                        Employee Salary
                    </h4>
                </div>

                <div class="card-body">

                    <div class="box-body my-form-body">
                        <?php if(!empty($rowData) && $rowData['salID'] !='' ){
                            $action = base_url('expense/employeeSalary/edit/'.$rowData['salID']);
                        }else{
                            $action = base_url('expense/employeeSalary/add');
                        } ?>
                        
                        <form class="row" action="<?php echo $action; ?>" class="" method="post" enctype="multipart/form-data">
                            <div class="col-sm-3 form-group">
                                <label for="employeeID" class=" control-label"> SELECT EMPLOYEE : </label>
                                <select  name="employeeID" class="select2 form-control ">
                                    <option value="">Select Employee..</option>
                                    <?php if(!empty($employeeData)){ 
                                        foreach($employeeData  as $rowsata) { ?>
                                        <option value="<?= isset($rowsata['employeeID']) ? $rowsata['employeeID'] :''; ?>"<?php if(!empty($rowData['employeeID']) && $rowData['employeeID'] == $rowsata['employeeID']){ echo 'selected'; } ?>><?= isset($rowsata['name']) ? $rowsata['name'] :''; ?></option>
                                    <?php } } ?>
                                </select>
                            </div>

                            <div class="col-sm-3 form-group">
                                <label for="payDate" class=" control-label">PAYMENT DATE : </label>
                                <input type="text" name="payDate" class="form-control datePicker" value="<?php if (isset($rowData['payDate'])) { echo $rowData['payDate'];}else{ echo date('d-m-Y'); } ?>" placeholder="" id="mdate">
                            </div>
                            
                            <div class="col-sm-3 form-group">
                                <label for="voucher" class=" control-label">VOUCHER NUMBER:*</label>
                                <input type="number" name="voucher" value="<?php if (isset($rowData['voucher'])) { echo $rowData['voucher'];}else{ echo set_value('voucher'); } ?>" class="form-control " id="voucher" expenseholder="">
                             </div>

                            <div class="col-sm-3 form-group">
                                <label for="amount" class="control-label"> SALARY AMOUNT :  </label>
                                <input type="number" name="amount" value="<?php if (isset($rowData['amount'])) { echo $rowData['amount'];}else{ echo set_value('amount'); } ?>" class="form-control " id="lang" expenseholder="">
                             </div>

                             <div class="col-sm-3 form-group">
                                <label for="salaryMonth" class=" control-label">SELECT MONTH YEAR : </label>
                                <input type="month" name="salaryMonth" class="form-control " value="<?php if (isset($rowData['salaryMonth'])) { echo $rowData['salaryMonth'];}else{ echo date('m-Y'); } ?>" placeholder="" >
                            </div>
                                            
                            <div class="col-sm-3 form-group">
                                <label for="desc" class="control-label"> DESCRIPTION : </label>
                                <textarea rows="3" class="form-control " name="desc" ><?php if (isset($rowData['desc'])) { echo $rowData['desc'];}else{ echo set_value('desc'); } ?></textarea>
                             </div>

                            
                            
                            <div class="col-sm-12 form-group">
                                <div class="">
                                    <input type="submit" name="submit" value="Save" class="btn btn-info" />
                                    <a href="<?= base_url('expense/employeeSalary'); ?>" class="btn btn-danger">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 

<script>
;(function ($, window, document, undefined) {
    $('document').ready(function () {
        "use strict";
        $('.monthpick').bootstrapMaterialDatePicker({
            format: 'MM-YYYY', 
            startView: "months", 
            minViewMode: "months",
            time: false
            });
    });
})(jQuery, window, document);
</script> 

