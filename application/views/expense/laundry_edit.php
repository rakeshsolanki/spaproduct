<section class="content">

    <div class="row">

        <div class="col-md-12">

            <div class="card">
                <div class="card-body bg-info">
                    <h4 class="text-white card-title">
                    Laundry Expense
                    </h4>
                </div>

                <div class="card-body">

                    <div class="box-body my-form-body">
                        <?php if(!empty($rowData) && $rowData['expenseID'] !='' ){
                            $action = base_url('expense/laundry/edit/'.$rowData['expenseID']);
                        }else{
                            $action = base_url('expense/laundry/add');
                        } ?>
                        
                        <form class="row" action="<?php echo $action; ?>" class="" method="post" enctype="multipart/form-data">
                            <div class="col-sm-2 form-group">
                                <label for="date" class=" control-label">DATE:</label>
                                <div class="input-group">
                                    <input type="text" name="date" class="form-control datePicker" value="<?php if (isset($rowData['date'])) { echo $rowData['date'];}else{ echo date('d-m-Y'); } ?>" placeholder="<?= date('d-m-Y'); ?>" id="mdate">
                                    <div class="input-group-append">
                                        <button class="btn btn-info" type="button"><i class="icon-calender"></i></button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-2 form-group">
                                <label for="towel" class=" control-label">TOWEL :</label>
                                <input type="number" name="towel" value="<?php if (isset($rowData['towel'])) { echo $rowData['towel'];}else{ echo set_value('towel'); } ?>" class="form-control form-control-line" id="towel" expenseholder="">
                             </div>

                            <div class="col-sm-2 form-group">
                                <label for="napkin" class=" control-label"> NAPKIN :</label>
                                <input type="number" name="napkin" value="<?php if (isset($rowData['napkin'])) { echo $rowData['napkin'];}else{ echo set_value('napkin'); } ?>" class="form-control form-control-line" id="napkin" expenseholder="">
                            </div>

                            <div class="col-sm-2 form-group">
                                <label for="dress" class="control-label"> DRESS : </label>
                                <input type="number" name="dress" value="<?php if (isset($rowData['dress'])) { echo $rowData['dress'];}else{ echo set_value('dress'); } ?>" class="form-control form-control-line" id="lang" expenseholder="">
                             </div>

                            <div class="col-sm-2 form-group">
                                <label for="bedsheet" class="control-label"> BEDSHEET :  </label>
                                <input type="number" name="bedsheet" value="<?php if (isset($rowData['bedsheet'])) { echo $rowData['bedsheet'];}else{ echo set_value('bedsheet'); } ?>" class="form-control form-control-line" id="lang" expenseholder="">
                             </div>

                            <div class="col-sm-2 form-group">
                                <label for="footmate" class="control-label"> FOOTMATE : </label>
                                <input type="number" name="footmate" value="<?php if (isset($rowData['footmate'])) { echo $rowData['footmate'];}else{ echo set_value('footmate'); } ?>" class="form-control form-control-line" id="lang" expenseholder="">
                             </div>
                            
                            <div class="col-sm-12 form-group">
                                <div class="">
                                    <input type="submit" name="submit" value="Save" class="btn btn-info" />
                                    <a href="<?= base_url('expense/laundry'); ?>" class="btn btn-danger">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 
