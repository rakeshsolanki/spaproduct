<div class="row">

    <div class="col-12">

        <div class="card">
            <div class="card-body bg-info">
                <h4 class="text-white card-title">
                <a class="btn btn-info" href="<?= base_url('expense/employeeSalary/add'); ?>"><i class="fa fa-plus"></i> Add</a>&nbsp; &nbsp; &nbsp;
                Employee Monthly Salary List
                </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="myTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="40">Sr.No</th>
                                <th width="70">Payment Date</th>
                                <th width="100">Name</th>
                                <th width="100">specialist</th>
                                <th width="80">Voucher Number</th>
                                <th width="80">Salary Amount</th>
                                <th width="70">Month Year</th>
                                <th> Description</th>
                                <th width= "100">Option</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($resultList)) {
                                foreach ($resultList as  $key => $row) : 
                                    $rowData = $this->Service->get_row(TBL_EMPLOYEE,'*',array('employeeID'=>$row['employeeID']));
                                ?>
                                    <tr id="lnr_<?= $row['salID']; ?>">
                                        <td><?= $key+1; ?></td>
                                        <td ><?= isset($row['payDate']) ? $this->Service->dateFormating($row['payDate']) :''; ?></td>
                                        <td ><?= isset($rowData['name']) ? $rowData['name'] :''; ?></td>
                                        <td ><?= isset($rowData['specialist']) ? $rowData['specialist'] :''; ?></td>
                                        <td ><?= isset($row['voucher']) ? $row['voucher'] :''; ?></td>
                                        <td ><i class="mdi mdi-currency-inr"></i><?= (isset($row['amount'])) ? $row['amount'] : "-"; ?></td>
                                        <td ><?= isset($row['salaryMonth']) ? date('M-Y',strtotime($row['salaryMonth'])) :''; ?></td>
                                        <td ><?= isset($row['desc']) ? $row['desc'] :''; ?></td>
                                        <td class="">
                                            <span  onclick="deletedata('<?= $row['salID']; ?>');" class="btn btn-sm btn-danger "><i class="fa fa-trash"></i></span>
                                            <a href="<?= base_url('expense/employeeSalary/edit/' . $row['salID']); ?>" class="btn btn-sm btn-info "><i class="fa fas fa-pencil-alt"></i></a>
                                            
                                        </td>
                                    </tr>
                                <?php endforeach;
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> 

<script>
;(function ($, window, document, undefined) {
    $('document').ready(function () {
        "use strict";
        $('#mdate').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD',
            time: false,
            date: true
        });
    });
})(jQuery, window, document);

function deletedata(expid) {
//   alert(expid);
  var r = confirm("Are you sure delete this data!");
  if (r == true) {
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('expense/employeeSalary/delete') ?>",
        data:{expid:expid},
        success: function(data){
            if(data)
            {
                $('#lnr_'+expid).hide();
            }
        }
    });
  } 
}

</script>
