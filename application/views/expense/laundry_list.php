<div class="row">

    <div class="col-12">

        <div class="card">
            <div class="card-body bg-info">
                <h4 class="text-white card-title">
                <a class="btn btn-info" href="<?= base_url('expense/laundry/add'); ?>"><i class="fa fa-plus"></i> Add</a>&nbsp; &nbsp; &nbsp;
                    Laundry Expenses List
                </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="myTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width=" 50;">Sr.No</th>
                                <th>Date<="th>                               <th>TOWEL</th>
                                <th>Napking</th>
                                <th>Dress</th>
                                <th>Bedsheet</th>
                                <th>Footmate</th>
                                <th width=" 70;">Amount</th>
                                <th width=" 100">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($resultList)) {
                                foreach ($resultList as  $key => $row) : ?>
                                    <tr id="lnr_<?= $row['expenseID']; ?>">
                                        <td><?= $key+1; ?></td>
                                        <td ><?= isset($row['date']) ? $this->Service->dateFormating($row['date']) :''; ?></td>
                                        <td class="">
                                            <?= isset($row['towel']) ? "<i class='mdi mdi-currency-inr'></i>".''. ($expValue['towel'] * $row['towel']).' ('.($row["towel"]).')':'' ?>
                                        </td>
                                        <td ><?= isset($row['napkin']) ? "<i class='mdi mdi-currency-inr'></i>".''. ($expValue['napkin'] * $row['napkin']).' ('.($row["napkin"]).')':'' ?></td>
                                        <td ><?= isset($row['dress']) ? "<i class='mdi mdi-currency-inr'></i>".''. ($expValue['dress'] * $row['dress']).' ('.($row["dress"]).')':'' ?></td>
                                        <td ><?= isset($row['bedsheet']) ? "<i class='mdi mdi-currency-inr'></i>".''. ($expValue['bedsheet'] * $row['bedsheet']).' ('.($row["bedsheet"]).')':'' ?></td>
                                        <td ><?= isset($row['footmate']) ? "<i class='mdi mdi-currency-inr'></i>".''. ($expValue['footmate'] * $row['footmate']).' ('.($row["footmate"]).')':'' ?></td>
                                        <td ><i class="mdi mdi-currency-inr"></i><?= (isset($row['total'])) ? $row['total'] : "-"; ?></td>
                                        <td class="">
                                            <span onclick="deleteloundrydata('<?= $row['expenseID']; ?>');" class="btn btn-sm btn-danger "><i class="fa fa-trash"></i></span>
                                            <a href="<?= base_url('expense/laundry/edit/' . $row['expenseID']); ?>" class="btn btn-sm btn-info"><i class="fa fas fa-pencil-alt"></i></a>
                                        </td>
                                    </tr>
                                <?php endforeach;
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> 


