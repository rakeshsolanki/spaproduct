<section class="content">

    <div class="row">

        <div class="col-md-12">

            <div class="card">
                <div class="card-body bg-info">
                    <h4 class="text-white card-title">
                    General Expense
                    </h4>
                </div>

                <div class="card-body">

                    <div class="box-body my-form-body">
                        <?php if(!empty($rowData) && $rowData['genExpID'] !='' ){
                            $action = base_url('expense/general/edit/'.$rowData['genExpID']);
                        }else{
                            $action = base_url('expense/general/add');
                        } ?>
                        
                        <form class="row" action="<?php echo $action; ?>" class="" method="post" enctype="multipart/form-data">
                            <div class="col-sm-3 form-group">
                                <label for="date" class=" control-label">DATE:</label>
                                <div class="input-group">
                                    <input type="text" name="date" class="form-control datePicker" value="<?php if (isset($rowData['date'])) { echo $rowData['date'];}else{ echo date('d-m-Y'); } ?>" placeholder="<?= date('d-m-Y'); ?>" id="mdate">
                                    <div class="input-group-append">
                                        <button class="btn btn-info" type="button"><i class="icon-calender"></i></button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-3 form-group">
                                <label for="mtea" class=" control-label">MORNING TEA :</label>
                                <input type="number" name="mtea" value="<?php if (isset($rowData['mtea'])) { echo $rowData['mtea'];}else{ echo set_value('mtea'); } ?>" class="form-control form-control-line" id="lat" expenseholder="">
                             </div>

                            <div class="col-sm-3 form-group">
                                <label for="etea" class=" control-label"> EVENING TEA :</label>
                                <input type="number" name="etea" value="<?php if (isset($rowData['etea'])) { echo $rowData['etea'];}else{ echo set_value('etea'); } ?>" class="form-control form-control-line" id="lat" expenseholder="">
                            </div>

                            <div class="col-sm-3 form-group">
                                <label for="waterJug" class="control-label"> WATER JUG : </label>
                                <input type="number" name="waterJug" value="<?php if (isset($rowData['waterJug'])) { echo $rowData['waterJug'];}else{ echo set_value('waterJug'); } ?>" class="form-control form-control-line" id="lang" expenseholder="">
                             </div>
                            
                            <div class="col-sm-12 form-group">
                                <div class="">
                                    <input type="submit" name="submit" value="Save" class="btn btn-info" />
                                    <a href="<?= base_url('expense/general'); ?>" class="btn btn-danger">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 



