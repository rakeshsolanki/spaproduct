<div class="row">

    <div class="col-12">

        <div class="card">
            <div class="card-body bg-info">
                <h4 class="text-white card-title">
                <a class="btn btn-info" href="<?= base_url('expense/general/add'); ?>"><i class="fa fa-plus"></i> Add</a> &nbsp; &nbsp; &nbsp;
                General Expenses List
                </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="myTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="50">Sr.No</th>
                                <th>Date</th>
                                <th>Morning</th>
                                <th>Evening</th>
                                <th>Water Jug</th>
                                <th width="70">Amount</th>
                                <th width="100">Option</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($resultList)) {
                                foreach ($resultList as  $key => $row) : ?>
                                    <tr id="lnr_<?= $row['genExpID']; ?>">
                                        <td><?= $key+1; ?></td>
                                        <td ><?= isset($row['date']) ? $this->Service->dateFormating($row['date']) :''; ?></td>
                                        <td class="">
                                            <?= isset($row['mtea']) ?  $row['mtea']:'' ?>
                                        </td>
                                        <td ><?= (isset($row['etea'])) ? $row['etea'] : "-"; ?></td>
                                        <td ><?= (isset($row['waterJug'])) ? $row['waterJug'] : "-"; ?></td>
                                        <td ><i class="mdi mdi-currency-inr"></i><?= (isset($row['total'])) ? $row['total'] : "-"; ?></td>
                                        <td class="">
                                            <span  onclick="deletegeneraldata('<?= $row['genExpID']; ?>');" class="btn btn-sm btn-danger "><i class="fa fa-trash"></i></span>
                                            <a href="<?= base_url('expense/general/edit/' . $row['genExpID']); ?>" class="btn btn-sm btn-info "><i class="fa fas fa-pencil-alt"></i></a>
                                        </td>
                                    </tr>
                                <?php endforeach;
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> 

<script>
;(function ($, window, document, undefined) {
    "use strict";
    $('document').ready(function () {
        $('#mdate').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD',
            time: false,
            date: true
        });
    });
})(jQuery, window, document);


</script>
