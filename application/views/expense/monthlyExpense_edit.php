<section class="content">

    <div class="row">

        <div class="col-md-12">

            <div class="card">
                <div class="card-body bg-info">
                    <h4 class="text-white card-title">
                    Monthly  Expense
                    </h4>
                </div>

                <div class="card-body">

                    <div class="box-body my-form-body">
                        <?php if(!empty($rowData) && $rowData['expID'] !='' ){
                            $action = base_url('expense/monthlyExpense/edit/'.$rowData['expID']);
                        }else{
                            $action = base_url('expense/monthlyExpense/add');
                        } ?>
                        
                        <form class="row" action="<?php echo $action; ?>" class="" method="post" enctype="multipart/form-data">
                            <div class="col-sm-3 form-group">
                                <label for="expDate" class=" control-label">DATE :</label>
                                    <input type="text" name="expDate" class="form-control datePicker" value="<?php if (isset($rowData['expDate'])) { echo $rowData['expDate'];}else{ echo date('d-m-Y'); } ?>" placeholder="" id="">
                            </div>

                            <div class="col-sm-3 form-group">
                                <label for="voucher" class=" control-label">VOUCHER NUMBER:*</label>
                                <input type="number" name="voucher" value="<?php if (isset($rowData['voucher'])) { echo $rowData['voucher'];}else{ echo set_value('voucher'); } ?>" class="form-control " id="voucher" expenseholder="">
                             </div>

                            <div class="col-sm-3 form-group">
                                <label for="type" class=" control-label"> TYPE : </label>
                                <select  name="type" class="select2 form-control ">
                                    <option value="">Select Type..</option>
                                    <?php if(!empty($expType)){ 
                                        foreach($expType  as $rowsata) { ?>
                                        <option value="<?= isset($rowsata['typeID']) ? $rowsata['typeID'] :''; ?>"<?php if(!empty($rowData['type']) && $rowData['type'] == $rowsata['typeID']){ echo 'selected'; } ?>><?= isset($rowsata['type']) ? $rowsata['type'] :''; ?></option>
                                    <?php } } ?>
                                </select>
                                
                            </div>

                            <div class="col-sm-3 form-group">
                                <label for="amount" class="control-label"> AMOUNT :  </label>
                                <input type="number" name="amount" value="<?php if (isset($rowData['amount'])) { echo $rowData['amount'];}else{ echo set_value('amount'); } ?>" class="form-control " id="lang" expenseholder="">
                             </div>

                            <div class="col-sm-3 form-group">
                                <label for="desc" class="control-label"> DESCRIPTION : </label>
                                <textarea rows="3" class="form-control " name="desc" ><?php if (isset($rowData['desc'])) { echo $rowData['desc'];}else{ echo set_value('desc'); } ?></textarea>
                             </div>

                            
                            
                            <div class="col-sm-12 form-group">
                                <div class="">
                                    <input type="submit" name="submit" value="Save" class="btn btn-info" />
                                
                                    <a href="<?= base_url('expense/monthlyExpense'); ?>" class="btn btn-danger">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 
