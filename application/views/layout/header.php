<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo $this->config->item('admin_assets_path').'images/fevicon.png'; ?>" type="image/x-icon" />
    <title><?php echo (isset($title)) ? $title.' | '.$this->config->item('site_title') : $this->config->item('site_title'); ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo $this->config->item('admin_assets_path'); ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $this->config->item('admin_assets_path'); ?>plugins/datatables/media/css/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="<?php echo $this->config->item('admin_assets_path'); ?>plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $this->config->item('admin_assets_path'); ?>plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <!-- chartist CSS -->
    <link href="<?php echo $this->config->item('admin_assets_path'); ?>plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
     <link rel="stylesheet" href="<?php echo $this->config->item('admin_assets_path'); ?>plugins/html5-editor/bootstrap-wysihtml5.css" />

    <link href="<?php echo $this->config->item('admin_assets_path'); ?>plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $this->config->item('admin_assets_path'); ?>plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet">
    <link href="<?php echo $this->config->item('admin_assets_path'); ?>plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="<?php echo $this->config->item('admin_assets_path'); ?>plugins/daterangepicker/daterangepicker.css" rel="stylesheet">
    <!--This page css - Morris CSS -->
    <link href="<?php echo $this->config->item('admin_assets_path'); ?>plugins/c3-master/c3.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    
    <link href="<?php echo $this->config->item('admin_path'); ?>css/style.css" rel="stylesheet">
   
    <!-- You can change the theme colors from here -->
    <link href="<?php echo $this->config->item('admin_path'); ?>css/colors/green.css" id="theme" rel="stylesheet">
    <link href="<?php echo $this->config->item('admin_path'); ?>css/custom.css" rel="stylesheet">
    <!-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet"> -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->

    <script src="<?php echo $this->config->item('admin_assets_path'); ?>plugins/jquery/jquery.min.js"></script>
    <script src="<?php echo $this->config->item('admin_assets_path'); ?>plugins/popper/popper.min.js"></script>
    <script src="<?php echo $this->config->item('admin_assets_path'); ?>plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo $this->config->item('admin_assets_path'); ?>plugins/moment/moment.js"></script>
    <script src="<?php echo $this->config->item('admin_assets_path'); ?>plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
</head>

<body class="fix-header fix-sidebar card-no-border">
 
<div id="main-wrapper">
        
<?php
 $loginuserID = $this->session->userdata('userID');
 $loginuserData = $this->user->get_users(array('userRole'=>1,'userID'=> $loginuserID),true);
 if(!empty($loginuserData) && $loginuserData['isActive'] != 1){
    redirect('manager/logout');
 }
 ?>
 <input type="hidden" id="baseurl" value="<?= base_url(); ?>" >