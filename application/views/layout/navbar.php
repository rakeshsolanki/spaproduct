<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo base_url(); ?>">
                <b>
                    <img src="<?php echo $this->config->item('admin_path') . 'assets/images/navicon.png'; ?>" alt="homepage" class="" width="60"/> 
                </b>
                <span>
                    <img src="<?php echo $this->config->item('admin_path'); ?>assets/images/navlogo.png" class="" alt="homepage" style="width: 75%" />
                </span>
            </a>
        </div>
        <div class="navbar-collapse">
            <ul class="navbar-nav mr-auto mt-md-0">
                <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                <li class="nav-item"> <a id="toggleSidebar" class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                <li class="nav-item hidden-sm-down search-box"> <a class="nav-link hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"></a>
                    <form class="app-search">
                        <input type="text" class="form-control" placeholder="Search & enter"> <a class="srh-btn"><i class="ti-close"></i></a>
                    </form>
                </li>
            </ul>
            <ul class="navbar-nav my-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php echo ucfirst($this->session->userdata('name')); ?>
                        <?php echo ($this->session->has_userdata('branch'))?' ('.$this->session->userdata('branch').')':""; ?>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right scale-up">
                        <ul class="dropdown-user">
                            <?php
                            $userRole = $this->session->userdata('user_type');
                            if($userRole==3){
                                $logoutUrl = base_url('logout');
                            }else{
                                $logoutUrl = base_url('manager/logout');
                            } ?>
                            <li><a href="<?php echo base_url('dashboard/profile'); ?>"><i class="fa fa-user"></i> Profile</a></li>
                            <li><a href="<?php echo base_url('dashboard/changePassword'); ?>"><i class="fa fa-lock"></i> Change Password</a></li>
                            <li><a href="<?php echo $logoutUrl; ?>"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>

</header> 