<?php
$cur_tab = $this->uri->segment(1) == '' ? '' : $this->uri->segment(1);
$curr_tab = $this->uri->segment(2)==''?'': $this->uri->segment(2);
?>
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar" >
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="<?php echo ($cur_tab == 'dashboard') ? "active" : "" ?>" id="menu_dashboard"><a href="<?= base_url('dashboard'); ?>"><i class="mdi mdi-apps"></i><span class="hide-menu"> Dashboard </span></a></li>
                <?php
                $userRole = $this->session->userdata('user_type');
                if($userRole==3){ ?>
                    <li class="<?php echo ($cur_tab == 'users') ? "active" : "" ?>" id="menu_users"><a href="<?= base_url('users'); ?>"><i class="mdi mdi-file-tree"></i><span class="hide-menu"> Branches </span></a></li>
                    <li class="<?php echo ($cur_tab == 'invoice') ? "active" : "" ?>" id="menu_invoice"><a href="<?= base_url('invoice'); ?>"><i class="mdi mdi-file-document"></i><span class="hide-menu"> Invoice </span></a></li>
                    <li class="<?php echo ($cur_tab == 'customers') ? "active" : "" ?>">
                        <a class="has-arrow waves-effect waves-dark <?php echo ($cur_tab == 'customers') ? "active" : "" ?>" href="#" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu"> Membership </span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li class="<?php echo ($curr_tab == '') ? "active-submenu" : "" ?>" ><a href="<?php echo base_url('customers'); ?>"> Active Membership </a></li>
                            <li class="<?php echo ($curr_tab == 'expire') ? "active-submenu" : "" ?>"><a href="<?php echo base_url('customers/expire'); ?>"> Expire Membership </a></li>
                        </ul>
                    </li>
                    <li class="<?php echo ($cur_tab == 'employee') ? "active" : "" ?>" id="menu_employee"><a href="<?= base_url('employee'); ?>"><i class="mdi mdi-account-circle"></i><span class="hide-menu"> Employee </span></a></li>
                    <li class="<?php echo ($cur_tab == 'treatments') ? "active" : "" ?>" id="menu_treatment"><a href="<?= base_url('treatments'); ?>"><i class="mdi mdi-hotel"></i><span class="hide-menu"> Treatments </span></a></li>
                    <li class="<?php echo ($cur_tab == 'expense') ? "active" : "" ?>">
                        <a class="has-arrow waves-effect waves-dark <?php echo ($cur_tab == 'expense') ? "active" : "" ?>" href="#" aria-expanded="false"><i class="mdi mdi-chart-line"></i><span class="hide-menu"> Expenses </span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li class="<?php echo ($curr_tab == 'setExpense') ? "active-submenu" : "" ?>" ><a href="<?php echo base_url('expense/setExpense'); ?>"> Expense Value </a></li>
                            <li class="<?php echo ($curr_tab == 'general') ? "active-submenu" : "" ?>"><a href="<?php echo base_url('expense/general'); ?>"> General Expense </a></li>
                            <li class="<?php echo ($curr_tab == 'laundry') ? "active-submenu" : "" ?>" ><a href="<?php echo base_url('expense/laundry'); ?>"> Laundry Expense </a></li>
                            <li class="<?php echo ($curr_tab == 'monthlyExpense') ? "active-submenu" : "" ?>" ><a href="<?php echo base_url('expense/monthlyExpense'); ?>"> Monthly Expense </a></li>
                            <li class="<?php echo ($curr_tab == 'employeeSalary') ? "active-submenu" : "" ?>"><a href="<?php echo base_url('expense/employeeSalary'); ?>"> Employee Salary </a></li>
                        </ul>
                    </li>
                <?php }else{ ?>
                    <li class="<?php echo ($cur_tab == 'invoice') ? "active" : "" ?>" id="menu_invoice"><a href="<?= base_url('invoice'); ?>"><i class="mdi mdi-file-document"></i><span class="hide-menu"> Invoice </span></a></li>
                    <li class="<?php echo ($cur_tab == 'customers') ? "active" : "" ?>" id="menu_customers"><a href="<?= base_url('customers'); ?>"><i class="mdi mdi-account-multiple"></i><span class="hide-menu"> Membership </span></a></li>
                    <li class="<?php echo ($cur_tab == 'employee') ? "active" : "" ?>" id="menu_employee"><a href="<?= base_url('employee'); ?>"><i class="mdi mdi-account-circle"></i><span class="hide-menu"> Employee </span></a></li>
                    <li class="<?php echo ($cur_tab == 'expense') ? "active" : "" ?>">
                        <a class="has-arrow waves-effect waves-dark <?php echo ($cur_tab == 'expense') ? "active" : "" ?>" href="#" aria-expanded="false"><i class="mdi mdi-chart-line"></i><span class="hide-menu"> Expenses </span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li class="<?php echo ($curr_tab == 'general') ? "active-submenu" : "" ?>"><a href="<?php echo base_url('expense/general'); ?>"> General Expense </a></li>
                            <li class="<?php echo ($curr_tab == 'laundry') ? "active-submenu" : "" ?>" ><a href="<?php echo base_url('expense/laundry'); ?>"> Laundry Expense </a></li>
                            <li class="<?php echo ($curr_tab == 'monthlyExpense') ? "active-submenu" : "" ?>" ><a href="<?php echo base_url('expense/monthlyExpense'); ?>"> Monthly Expense </a></li>
                            <li class="<?php echo ($curr_tab == 'employeeSalary') ? "active-submenu" : "" ?>"><a href="<?php echo base_url('expense/employeeSalary'); ?>"> Employee Salary </a></li>
                        </ul>
                    </li>
                <?php } ?>
                <li class="<?php echo ($cur_tab == 'report') ? "active" : "" ?>" id="menu_report"><a href="<?= base_url('report'); ?>"><i class="mdi mdi-database"></i><span class="hide-menu"> Report </span></a></li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <div class="sidebar-footer"> </div>
</aside>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center">
                <h3 class="text-themecolor"><?php echo isset($title) ? $title : 'Dashboard'; ?></h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Home</li>
                    <?php if(!empty($submenu) && $submenu != ''){ ?>
                        <li class="breadcrumb-item"><?php echo isset($submenu) ? $submenu : ''; ?></li>
                    <?php } ?>
                    <li class="breadcrumb-item active"><?php echo isset($menu) ? $menu : 'Dashboard'; ?></li>
                </ol>
            </div>
        </div>
        <?php $this->load->view('flash_messages'); ?>
