</div>
</div>
<footer class="footer"> © <?php echo date('Y'); ?> <?php echo $this->config->item('site_title'); ?> </footer>
</div>
<script src="<?php echo $this->config->item('admin_path'); ?>js/custom.js"></script>
<!-- This is data table -->
<script src="<?php echo $this->config->item('admin_assets_path'); ?>plugins/datatables/datatables.min.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="<?php echo $this->config->item('admin_path'); ?>js/jquery.slimscroll.js"></script>
<script src="<?php echo $this->config->item('admin_assets_path'); ?>plugins/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
<!--Wave Effects -->
<script src="<?php echo $this->config->item('admin_path'); ?>js/waves.js"></script>
<!--Menu sidebar -->
<script src="<?php echo $this->config->item('admin_path'); ?>js/sidebarmenu.js"></script>
<!--stickey kit -->
<script src="<?php echo $this->config->item('admin_assets_path'); ?>plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
<script src="<?php echo $this->config->item('admin_assets_path'); ?>plugins/sparkline/jquery.sparkline.min.js"></script>
<!--Custom JavaScript -->
<script src="<?php echo $this->config->item('admin_path'); ?>js/custom.min.js"></script>
<script src="<?php echo $this->config->item('admin_assets_path'); ?>plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?php echo $this->config->item('admin_assets_path'); ?>plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo $this->config->item('admin_assets_path'); ?>plugins/daterangepicker/daterangepicker.js"></script>

<!--c3 JavaScript -->
<script src="<?php echo $this->config->item('admin_assets_path'); ?>plugins/d3/d3.min.js"></script>
<script src="<?php echo $this->config->item('admin_assets_path'); ?>plugins/c3-master/c3.min.js"></script>
<script src="<?php echo $this->config->item('admin_assets_path'); ?>plugins/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="<?php echo $this->config->item('admin_assets_path'); ?>plugins/html5-editor/bootstrap-wysihtml5.js"></script>
    <script>
    $(document).ready(function() {
        $('.textarea_editor').wysihtml5();
    });
    </script>
<script>
    ;
    (function ($, window, document, undefined) {
        "use strict";
        $('document').ready(function () {
            $(".select2").select2();
            $('#myTable').dataTable({
                "order": [],
            });
            // $('#toggleSidebar').click();
        });
        $(function() {
            setTimeout(function() {
                $(".alert-dismissible").hide()
            }, 10000);
        });
    })(jQuery, window, document);
</script>
<script>
    $(function() {
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });
        $(".select2").select2();
    });
    </script>
</body>
</html>
