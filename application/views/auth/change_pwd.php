<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body bg-info">
          <h4 class="text-white card-title"> Change Password </h4>
        </div>
        <div class="card-body">
          <div class="box-body my-form-body">
            <?php echo form_open(base_url('dashboard/changePassword'), 'class="form-horizontal"');  ?>
            <div class="row">
              <div class="col-sm-3 form-group">
                <label for="name" class="control-label">Old Password</label>
                <input type="password" name="oldPassword" value="" required class="form-control form-control-line" id="oldPassword" placeholder="Enter old Password">
              </div>
              <div class="col-sm-3 form-group">
                <label for="name" class="control-label">New Password</label>
                <input type="password" name="password" value="" required class="form-control form-control-line" id="password" placeholder="Enter new Password">
              </div>
              <div class="col-sm-3 form-group">
                <label for="name" class="control-label">Confirm Password</label>
                <input type="password" name="confirm_pwd" value="" required class="form-control form-control-line" id="confirm_pwd" placeholder="Enter Password">
              </div>
              <div class="col-md-12 form-group">
                <input type="submit" name="submit" value="Change Password" class="btn btn-info">
              </div>
              <?php echo form_close(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>