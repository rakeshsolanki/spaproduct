<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Report extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $userRole = $this->session->userdata('user_type');
        if($userRole==""){
            redirect('auth/login');
        }
        $this->load->library('excel');
    }

    public function index() {
        $data['title'] = "Report";
        $data['menu'] = "report";
        // $data['all_users'] = $this->user->get_users(array('userRole'=>5,'order_high'=>'userID'));
        $data['view'] = 'dashboard/report';
        $this->renderAdmin($data);
    }

    public function exportInvoice() {
        $this->db->select('*')
            ->from(TBL_INVOICE)
            ->where('isDelete', 0);
        if((isset($_GET['invoiceStartDate']) && $_GET['invoiceStartDate']!="") && (isset($_GET['invoiceEndDate']) && $_GET['invoiceEndDate']!="")){
            $startDate = date('Y-m-d',strtotime($_GET['invoiceStartDate']));
            $endDate = date('Y-m-d',strtotime($_GET['invoiceEndDate']));
            $this->db->where('invoiceDate >= ',$startDate);
            $this->db->where('invoiceDate <= ',$endDate);
        }elseif(isset($_GET['invoiceStartDate']) && $_GET['invoiceStartDate']!=""){
            $startDate = date('Y-m-d',strtotime($_GET['invoiceStartDate']));
            $this->db->where('invoiceDate >= ',$startDate);
        }elseif(isset($_GET['invoiceEndDate']) && $_GET['invoiceEndDate']!=""){
            $endDate = date('Y-m-d',strtotime($_GET['invoiceEndDate']));
            $this->db->where('invoiceDate <= ',$endDate);
        }
        $invoiceList = $this->db->get()->result_array();
        // $invoiceList = $this->Service->get_all(TBL_INVOICE,'*',$postdata);
        
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Date');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'InvoiceID');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'MembershipID');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Client Name');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Contact');
        // $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Validity');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Slip No.');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Treatment');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Duration');
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'In Time');
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Out Time');
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Therapist');
        $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Pay Mode');
        $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Amount');
        $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Product');
        $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Branch');
        
        $rowCount = 2;
        // echo "<pre>"; print_r($invoiceList); die();
        if(!empty($invoiceList)){
            foreach ($invoiceList as $row) {
                $product = $this->Service->getProductName(explode(',',$row['product']),true);
                $Treatment =  $this->Service->getTreatmentName($row['treatmentID']);
                $branchName= $this->Service->getBranceName($row['managerID']);
                $employeeName = $this->Service->getEmployeeName(explode(',',$row['employeeID']),true);
                $Therapist =array();
                if(!empty($employeeName)){
                    foreach($employeeName as $empe){
                        $Therapist[] = $empe['name'];
                    }
                }
                $productName = array();
                if(!empty($product)){
                    foreach($product as $pro){
                       $productName[] = $pro['name'];
                    }
                }
                $therapist = implode(',',$Therapist);
                $productNames = implode(',',$productName);
                $amount = (!empty($row['amount']) && $row['amount'] != '0') ? $row['amount'].'' : 'Member';

                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $row['invoiceDate']);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $row['invoiceID']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $row['membershipID']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $row['customerName']);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $row['customerMobile']);
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $row['yslip']);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $Treatment);
                $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $row['duration']);
                $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $row['inTime']);
                $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $row['outTime']);
                $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $therapist);
                $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $row['payMode']);
                $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $amount);
                $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $productNames);
                $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $branchName);
                $rowCount++;
            }
        }
        $filename = "invoice-". date("YMdHis").".csv";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');  
        $objWriter->save('php://output');
    }

    public function exportMemberShip() {
        $this->db->select('*')
            ->from(TBL_CUSTOMER)
            ->where('isDelete', 0); 
        if((isset($_GET['membershipStartDate']) && $_GET['membershipStartDate']!="") && (isset($_GET['membershipEndDate']) && $_GET['membershipEndDate']!="")){
            $startDate = date('Y-m-d',strtotime($_GET['membershipStartDate']));
            $endDate = date('Y-m-d',strtotime($_GET['membershipEndDate']));
            $this->db->where('membershipDate >= ',$startDate);
            $this->db->where('membershipDate <= ',$endDate);
        }elseif(isset($_GET['membershipStartDate']) && $_GET['membershipStartDate']!=""){
            $startDate = date('Y-m-d',strtotime($_GET['membershipStartDate']));
            $this->db->where('membershipDate >= ',$startDate);
        }elseif(isset($_GET['membershipEndDate']) && $_GET['membershipEndDate']!=""){
            $endDate = date('Y-m-d',strtotime($_GET['membershipEndDate']));
            $this->db->where('membershipDate <= ',$endDate);
        }
        $resultList = $this->db->get()->result_array();
        
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Id');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Name');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Contact');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Joining date');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Expiry date');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Validity');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Duration');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Treatment');
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Balance');
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Slip Number');
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Amount');

        $rowCount = 2;
        // echo "<pre>"; print_r($resultList); die();
        if(!empty($resultList)){
            foreach($resultList as $row) {
                $membershipDate = date('d-m-Y',strtotime( $row['membershipDate']));
                $expireDate = date('d-m-Y',strtotime( $row['expireDate']));
                
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $row['membershipID']);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $row['name']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $row['mobile']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $membershipDate);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $expireDate);
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $row['validity']);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $row['duration']);
                $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $row['totalTreatment']);
                $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $row['remainingTreatment']);
                $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $row['yslip']);
                $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $row['amount']);
                $rowCount++;
            }
        }

        $filename = "membership-". date("YMdHis").".csv";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');  
        $objWriter->save('php://output');
    }

    public function exportGeneralExpense() {
        $this->db->select('*')
            ->from(TBL_GENERAL_EXPENSE)
            ->where('isDelete', 0);
        if((isset($_GET['genExpStartDate']) && $_GET['genExpStartDate']!="") && (isset($_GET['genExpEndDate']) && $_GET['genExpEndDate']!="")){
            $startDate = date('Y-m-d',strtotime($_GET['genExpStartDate']));
            $endDate = date('Y-m-d',strtotime($_GET['genExpEndDate']));
            $this->db->where('date >= ',$startDate);
            $this->db->where('date <= ',$endDate);
        }elseif(isset($_GET['genExpStartDate']) && $_GET['genExpStartDate']!=""){
            $startDate = date('Y-m-d',strtotime($_GET['genExpStartDate']));
            $this->db->where('date >= ',$startDate);
        }elseif(isset($_GET['genExpEndDate']) && $_GET['genExpEndDate']!=""){
            $endDate = date('Y-m-d',strtotime($_GET['genExpEndDate']));
            $this->db->where('date <= ',$endDate);
        }
        $resultList = $this->db->get()->result_array();
        
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Date');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Morning Tea');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Evening tea');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Water Jug');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Total');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Branch');

        $rowCount = 2;
        // echo "<pre>"; print_r($resultList); die();
        if(!empty($resultList)){
            foreach($resultList as $row) {
                $date = date('d-m-Y',strtotime( $row['date']));
                $branchName= $this->Service->getBranceName($row['managerID']);

                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $date);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $row['mtea']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $row['etea']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $row['waterJug']);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $row['total']);
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $branchName);
                $rowCount++;
            }
        }
        
        $filename = "generalExpense-". date("YMdHis").".csv";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');  
        $objWriter->save('php://output');
    }

    public function exportLaundryExpense() {
        $this->db->select('*')
            ->from(TBL_LAUNDRY_EXPENSE)
            ->where('isDelete', 0);
        if((isset($_GET['lanExpStartDate']) && $_GET['lanExpStartDate']!="") && (isset($_GET['lanExpEndDate']) && $_GET['lanExpEndDate']!="")){
            $startDate = date('Y-m-d',strtotime($_GET['lanExpStartDate']));
            $endDate = date('Y-m-d',strtotime($_GET['lanExpEndDate']));
            $this->db->where('date >= ',$startDate);
            $this->db->where('date <= ',$endDate);
        }elseif(isset($_GET['lanExpStartDate']) && $_GET['lanExpStartDate']!=""){
            $startDate = date('Y-m-d',strtotime($_GET['lanExpStartDate']));
            $this->db->where('date >= ',$startDate);
        }elseif(isset($_GET['lanExpEndDate']) && $_GET['lanExpEndDate']!=""){
            $endDate = date('Y-m-d',strtotime($_GET['lanExpEndDate']));
            $this->db->where('date <= ',$endDate);
        }
        $resultList = $this->db->get()->result_array();
        
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Date');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Towel');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Napkin');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Dress');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Bedsheet');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Footmate');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Total');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Branch');

        $rowCount = 2;
        // echo "<pre>"; print_r($resultList); die();
        if(!empty($resultList)){
            foreach($resultList as $row) {
                $date = date('d-m-Y',strtotime( $row['date']));
                $branchName= $this->Service->getBranceName($row['managerID']);

                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $date);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $row['towel']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $row['napkin']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $row['dress']);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $row['bedsheet']);
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $row['footmate']);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $row['total']);
                $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $branchName);
                $rowCount++;
            }
        }
        
        $filename = "laundryExpense-". date("YMdHis").".csv";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');  
        $objWriter->save('php://output');
    }

}
?>