<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        $userRole = $this->session->userdata('user_type');
        if($userRole==""){
            redirect('auth/login');
        }
    }
    
    public function index() {
        $data['title'] = "Members";
        $data['menu'] = "members";
        $loginUserID = $this->session->userdata('userID');
        $userRole = $this->session->userdata('user_type');
        $this->db->select('*');
        $this->db->where('isDelete', 0);
        $this->db->where('isActive', 1);
        if($userRole != 3){ 
            $this->db->where('managerID' , $loginUserID);
        }
        $this->db->order_by('customerID', 'DESC');
        $resultList = $this->db->get(TBL_CUSTOMER)->result_array();
        $data['resultList'] = $resultList;
        $data['view'] = 'users/customer_list';
        $this->renderAdmin($data);
    }

    public function add($id="") {
        $data['title'] = "Add Customer";
        $data['menu'] = "add customer";
        $userID = $this->session->userdata('userID');
        if($id!=""){
            $data['title'] = "Edit Customer";
            $data['menu'] = "edit customer";
            $rowData = $this->Service->get_row(TBL_CUSTOMER,'*', array('customerID'=>$id));
            $data['rowData'] = $rowData;
        }
        if ($this->input->post('submit')) {
            // echo "<pre>"; print_r($_POST); die();
            $this->form_validation->set_rules('name', 'Full name', 'required');
            $this->form_validation->set_rules('mobile', 'Mobile', 'trim|required');
            if ($this->form_validation->run() != FALSE) {
                $saveData = array(
                    'name' => $this->input->post('name'),
                    // 'managerID' => $userID,
                    'email' => (isset($_POST['email']))?$this->input->post('email'):"",
                    'mobile' => $this->input->post('mobile'),
                    'dob' => (isset($_POST['dob']))?$this->input->post('dob'):"",
                    'gender' => $this->input->post('gender'),
                    'address' => (isset($_POST['address']))?$this->input->post('address'):"",
                    'validity' => (isset($_POST['validity']))?$this->input->post('validity'):"",
                    'duration' => (isset($_POST['duration']))?$this->input->post('duration'):"",
                    'totalTreatment' => (isset($_POST['totalTreatment']))?$this->input->post('totalTreatment'):0,
                    'remainingTreatment' => (isset($_POST['totalTreatment']))?$this->input->post('totalTreatment'):0,
                    'yslip' => (isset($_POST['yslip']))?$this->input->post('yslip'):"",
                    'paymentMode' => (isset($_POST['paymentMode']))?$this->input->post('paymentMode'):"",
                    'amount' => (isset($_POST['amount']))?$this->input->post('amount'):"",
                    'isActive' => 1,
                    'updatedTime' => date("Y-m-d h:i:s")
                );
                if(isset($rowData['membershipID']) && $rowData['membershipID']==""){
                    $saveData['membershipID'] = $this->Service->generateMembershipID();
                }
                if(isset($_POST['membershipDate']) && $_POST['membershipDate']!=""){
                    $saveData['membershipDate'] = date('Y-m-d',strtotime($this->input->post('membershipDate'))) ;
                }
                if((isset($_POST['validity']) && $_POST['validity']!="") && (isset($_POST['membershipDate']) && $_POST['membershipDate']!="")){
                    $month = $_POST['validity'];
                    $saveData['expireDate'] = date('Y-m-d', strtotime("+".$month." months", strtotime($saveData['membershipDate'])));
                }
                // echo "<pre>"; print_r($saveData); die();
                if($id!=""){
                    $this->Service->update_row(TBL_CUSTOMER,$saveData, array('customerID'=>$id));
                    $this->session->set_flashdata('success_msg', $this->getNotification('recUpSuc'));
                    redirect(base_url('customers'));
                }else{
                    $saveData['managerID'] =  $userID;
                    $membershipID = $this->Service->generateMembershipID();
                    $saveData['membershipID'] = $membershipID;
                    $saveData['createdTime'] = date("Y-m-d h:i:s");

                    $totalTreatment = $saveData['totalTreatment'];
                    $branchName = (isset($userID))?$this->Service->getBranceName($userID):"";
                    $username = $saveData['name'];
                    $payAmount = (isset($saveData['amount']))?$saveData['amount']:"";
                    $payMode = (isset($saveData['paymentMode']))?$saveData['paymentMode']:"";
                    $smsMobile = (isset($saveData['mobile']))?$saveData['mobile']:"";
                    // send invoice msg to user
                    $usermsg = $branchName." BRANCH\n".$username;
                    $usermsg .= "\n".$saveData['mobile'];
                    $usermsg .= "\nThank You For Buying Membership In NAFIZA SPA.";
                    $usermsg .= "\nYour M.Code : ".$membershipID;
                    $usermsg .= "\nValid Till : ".date('d-m-Y',strtotime($saveData['expireDate']));
                    $usermsg .= "\n".$payAmount."/- ".$payMode;
                    $usermsg .= "\nAC : ".$totalTreatment;
                    
                    $adminNumbner = $this->Service->getAdminNumber();
                    $this->Service->sendSMS($smsMobile, $usermsg);
                    $this->Service->sendSMS($adminNumbner, $usermsg);

                    $this->Service->insert_row(TBL_CUSTOMER,$saveData);
                    $this->session->set_flashdata('success_msg', $this->getNotification('recAddSuc'));
                    redirect(base_url('customers'));
                }
            }
        }
        $data['view'] = 'users/customer_edit';
        $this->renderAdmin($data);
    }
    
    public function delete($id = 0) {
        $customerid = $this->input->post('customerid');
        $result = $this->Service->set_delete(TBL_CUSTOMER,array('customerID'=>$customerid));
        if(!empty($result)){ echo 1; }else{ echo 0; }
        // $this->session->set_flashdata('success_msg', $this->getNotification('recDelSuc'));
        // redirect(base_url('customers'));
    }

    public function viewCustomer($id = 0) {
        // $terr = $this->Service->getRemainingTreatment($id);
        $data['title'] = "View User Detail";
        $data['menu'] = "view user";
        $data['userID'] = $id;
        $data['user'] = $this->user->get_users(array('userID'=>$id), true);
        $data['userTransactions'] = '';
        $data['chocoFlouzTransactions'] = '';
        $data['view'] = 'users/employee_view';
        $this->renderAdmin($data);
    }

    public function expire() {
        $data['title'] = "Expire Members";
        $data['menu'] = "expire members";
        $today = date('Y-m-d');
        $loginUserID = $this->session->userdata('userID');
        $userRole = $this->session->userdata('user_type');
        
        $this->db->select('*');
        $this->db->where('isDelete', 0);
        $this->db->where('isActive', 1);
        if($userRole != 3){
            $this->db->where('managerID' , $loginUserID);
        }
        $this->db->order_by('customerID', 'DESC');
        $resultList = $this->db->get(TBL_CUSTOMER)->result_array();
        $data['resultList'] = $resultList;
        $data['view'] = 'users/exp_customer_list';
        $this->renderAdmin($data);
    }
    
}
?>