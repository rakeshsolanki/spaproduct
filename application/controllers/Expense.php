<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Expense extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $userRole = $this->session->userdata('user_type');
        if($userRole==""){
            redirect('auth/login');
        }
    }

    public function setExpense() {
        $data['title'] = "Expense Value";
        $data['menu'] = "expense value";
        $data['submenu'] = "Expense";
        $loginUserID = $this->session->userdata('userID');
        $resultList= $this->Service->get_row(TBL_EXPENSE_VALUE,'*');
        $data['resultList'] = $resultList;
        if(!empty($_POST['submit'])){
            $this->form_validation->set_rules('tea', 'Tea price', 'trim|required');
            $this->form_validation->set_rules('waterJug', 'WaterJug price', 'trim|required');
            $this->form_validation->set_rules('towel', 'Towel price', 'trim|required');
            $this->form_validation->set_rules('napkin', 'Napkin  price', 'trim|required');
            $this->form_validation->set_rules('dress', 'Dress  price', 'trim|required');
            $this->form_validation->set_rules('bedsheet', 'Bedsheet price', 'trim|required');
            $this->form_validation->set_rules('footmate', 'Footmate  price', 'trim|required');
            if ($this->form_validation->run() != FALSE) {
                $data = array(
                    'managerID' => $loginUserID,
                    'tea' => $this->input->post('tea'),
                    'waterJug' => $this->input->post('waterJug'),
                    'towel' => $this->input->post('towel'),
                    'napkin' => $this->input->post('napkin'),
                    'dress' => $this->input->post('dress'),
                    'bedsheet' => $this->input->post('bedsheet'),
                    'footmate' => $this->input->post('footmate'), 
                    // 'isActive' => $this->input->post('isActive'),
                );
                $data['updatedTime'] = date("Y-m-d h:i:s");
                $this->Service->update_row(TBL_EXPENSE_VALUE,$data, array('expID'=>$resultList['expID']));
                $this->session->set_flashdata('success_msg', $this->getNotification('recUpSuc'));
            }
        }
        $data['view'] = 'expense/setexpense_edit';
        $this->renderAdmin($data);
    }

    public function general(){
        $data['title'] = "General Expense";
        $data['menu'] = "General Expense";
        $data['submenu'] = "Expense";
        $loginUserID = $this->session->userdata('userID');
        
        $userRole = $this->session->userdata('user_type');
        if($userRole == 3){
            $resultList= $this->Service->get_all(TBL_GENERAL_EXPENSE,'*',[],'date','DESC');
        }else{
            $resultList= $this->Service->get_all(TBL_GENERAL_EXPENSE,'*',array('managerID'=>$loginUserID),'date');
        }
        $data['resultList'] = $resultList;
        $data['view'] = 'expense/general_list';
        $this->renderAdmin($data);
    }

    public function general_edit($id=""){
        $data['title'] = "General Expense";
        $data['menu'] = "General Expense";
        $data['submenu'] = "Expense";
        $loginUserID = $this->session->userdata('userID');
        if($id!=""){
            // $data['generalList'] = $this->Service->get_all(TBL_GENERAL_EXPENSE,'*',array('genExpID'=>'0'));
            $data['title'] = "Edit General Expense";
            $data['menu'] = "Edit General Expense";
            $rowData = $this->Service->get_row(TBL_GENERAL_EXPENSE,'*',array('genExpID'=>$id));
            if(empty($rowData)){
                redirect(base_url('expense/general'));
            }
            $data['rowData'] = $rowData;
        }
        if(!empty($_POST['submit'])){
            $this->form_validation->set_rules('date', 'Date', 'trim|required');
            $this->form_validation->set_rules('mtea', 'Morning tea', 'trim|required');
            $this->form_validation->set_rules('etea', 'Evening tea', 'trim|required');
            $this->form_validation->set_rules('waterJug', 'Water Jug', 'trim|required');
            if ($this->form_validation->run() != FALSE) {
                $expanseValData= $this->Service->get_row(TBL_EXPENSE_VALUE,'*');
                $mtea = $this->input->post('mtea');
                $etea = $this->input->post('etea');
                $waterJug = $this->input->post('waterJug');
                $total = ($mtea * $expanseValData['tea']) + ($etea * $expanseValData['tea']) + ($waterJug * $expanseValData['waterJug']);
                
                $data = array(
                    'managerID' => $loginUserID,
                    'date' => date('Y-m-d',strtotime($this->input->post('date'))),
                    'mtea' => $mtea,
                    'etea' => $etea,
                    'waterJug' => $waterJug,
                    'total' => $total,
                );
                if($id!=""){
                    $data['updatedTime'] = date("Y-m-d h:i:s");
                    $this->Service->update_row(TBL_GENERAL_EXPENSE,$data, array('genExpID'=>$id));
                    $this->session->set_flashdata('success_msg', $this->getNotification('recUpSuc'));
                }else{
                    $data['createdTime'] = date("Y-m-d h:i:s");
                    $data['updatedTime'] = date("Y-m-d h:i:s");
                    $result = $this->Service->insert_row(TBL_GENERAL_EXPENSE,$data);
                    $this->session->set_flashdata('success_msg', $this->getNotification('recAddSuc'));
                }
                redirect(base_url('expense/general'));
            }
        }
        $data['view'] = 'expense/general_edit';
        $this->renderAdmin($data);
    }

    public function laundry(){
        $data['title'] = "Laundry Expense";
        $data['menu'] = "Laundry Expense";
        $data['submenu'] = "Expense";
        $loginUserID = $this->session->userdata('userID');
        $data['expValue'] = $this->Service->get_row(TBL_EXPENSE_VALUE,'*');
        $userRole = $this->session->userdata('user_type');
        if($userRole == 3){
            $resultList= $this->Service->get_all(TBL_LAUNDRY_EXPENSE,'*',[]);
        }else{
            $resultList= $this->Service->get_all(TBL_LAUNDRY_EXPENSE,'*',array('managerID'=>$loginUserID));
        }
        $data['resultList'] = $resultList;
        $data['view'] = 'expense/laundry_list';
        $this->renderAdmin($data);
    }

    public function laundry_edit($id=""){
        $data['title'] = "Laundry Expense";
        $data['menu'] = "Laundry Expense";
        $data['submenu'] = "Expense";
        $loginUserID = $this->session->userdata('userID');
        if($id!=""){
            // $data['generalList'] = $this->Service->get_all(TBL_GENERAL_EXPENSE,'*',array('genExpID'=>'0'));
            $data['title'] = "Edit Laundry Expense";
            $data['menu'] = "Edit Laundry Expense";
            $rowData = $this->Service->get_row(TBL_LAUNDRY_EXPENSE,'*',array('expenseID'=>$id));
            if(empty($rowData)){
                redirect(base_url('expense/laundry'));
            }
            $data['rowData'] = $rowData;
        }
        if(!empty($_POST['submit'])){
            $this->form_validation->set_rules('date', 'Date', 'trim|required');
            $this->form_validation->set_rules('towel', 'towel', 'trim|required');
            $this->form_validation->set_rules('napkin', 'napkin', 'trim|required');
            $this->form_validation->set_rules('dress', 'dress', 'trim|required');
            $this->form_validation->set_rules('bedsheet', 'bedsheet', 'trim|required');
            $this->form_validation->set_rules('footmate', 'footmate', 'trim|required');
            if ($this->form_validation->run() != FALSE) {
                $expanseValData= $this->Service->get_row(TBL_EXPENSE_VALUE,'*');
                $towel = $this->input->post('towel');
                $napkin = $this->input->post('napkin');
                $dress = $this->input->post('dress');
                $bedsheet = $this->input->post('bedsheet');
                $footmate = $this->input->post('footmate');
                $total = ($towel * $expanseValData['towel']) + ($napkin * $expanseValData['napkin']) + ($dress * $expanseValData['dress']) + ($bedsheet * $expanseValData['bedsheet']) + ($footmate * $expanseValData['footmate']);
                
                $data = array(
                    'date' => date('Y-m-d',strtotime($this->input->post('date'))),
                    'managerID' => $loginUserID,
                    'towel' => $towel,
                    'napkin' => $napkin,
                    'dress' => $dress,
                    'bedsheet' => $bedsheet,
                    'footmate' => $footmate,
                    'total' => $total,
                );
                if($id!=""){
                    $data['updatedTime'] = date("Y-m-d h:i:s");
                    $this->Service->update_row(TBL_LAUNDRY_EXPENSE,$data, array('expenseID'=>$id));
                    $this->session->set_flashdata('success_msg', $this->getNotification('recUpSuc'));
                }else{
                    $data['createdTime'] = date("Y-m-d h:i:s");
                    $data['updatedTime'] = date("Y-m-d h:i:s");
                    $result = $this->Service->insert_row(TBL_LAUNDRY_EXPENSE,$data);
                    $this->session->set_flashdata('success_msg', $this->getNotification('recAddSuc'));
                }
                redirect(base_url('expense/laundry'));
            }
        }
        $data['view'] = 'expense/laundry_edit';
        $this->renderAdmin($data);
    }
    
    public function laundry_delete($id = 0) {
        $id = $this->input->post('expid');
        $data = $this->Service->set_delete(TBL_LAUNDRY_EXPENSE,array('expenseID'=>$id));
        if(!empty($data)){ echo 1;}else{ echo 0;};
    }

    public function general_delete($id = 0) {
        $id = $this->input->post('expid');
        $data = $this->Service->set_delete(TBL_GENERAL_EXPENSE,array('genExpID'=>$id));
        if(!empty($data)){ echo 1;}else{ echo 0;};
    }

    //monthly expenses 
    public function monthlyExpense(){
        $data['title'] = "Monthly Expense";
        $data['menu'] = "Monthly Expense";
        $data['submenu'] = "Expense";
        $loginUserID = $this->session->userdata('userID');
        $userRole = $this->session->userdata('user_type');
        if($userRole == 3){
            $resultList= $this->Service->get_all(TBL_OFFICE_EXPENSE,'*',[]);
        }else{
            $resultList= $this->Service->get_all(TBL_OFFICE_EXPENSE,'*',array('managerID'=>$loginUserID));
        }
        $data['resultList'] = $resultList;
        $data['view'] = 'expense/monthlyExpense_list';
        $this->renderAdmin($data);
    }
    
    public function monthlyExpense_edit($id=""){
        $data['title'] = "Monthly Expense";
        $data['menu'] = "Monthly Expense";
        $data['submenu'] = "Expense";
        $loginUserID = $this->session->userdata('userID');
        $data['expType'] = $this->Service->get_all(TBL_EXPENSE_TYPE,'*',[]);
        if($id!=""){
            // $data['generalList'] = $this->Service->get_all(TBL_GENERAL_EXPENSE,'*',array('genExpID'=>'0'));
            $data['title'] = "Edit Monthly Expense";
            $data['menu'] = "Edit Monthly Expense";
            $rowData = $this->Service->get_row(TBL_OFFICE_EXPENSE,'*',array('expID'=>$id));
            if(empty($rowData)){
                redirect(base_url('expense/monthlyExpense'));
            }
            $data['rowData'] = $rowData;
        }
        if(!empty($_POST['submit'])){
            $this->form_validation->set_rules('expDate', 'Date', 'trim|required');
            $this->form_validation->set_rules('voucher', 'Voucher', 'trim|required');
            $this->form_validation->set_rules('type', 'Type', 'trim|required');
            $this->form_validation->set_rules('amount', 'amount', 'trim|required');
            $this->form_validation->set_rules('desc', 'Description', 'trim|required');
            if ($this->form_validation->run() != FALSE) {
                
                $data = array(
                    'managerID' => $loginUserID,
                    'expDate' => date('Y-m-d',strtotime($this->input->post('expDate'))),
                    'voucher' => $this->input->post('voucher'),
                    'type' => $this->input->post('type'),
                    'amount' => $this->input->post('amount'),
                    'desc' => $this->input->post('desc'),
                );
                if($id!=""){
                    $data['updatedTime'] = date("Y-m-d h:i:s");
                    $this->Service->update_row(TBL_OFFICE_EXPENSE,$data, array('expID'=>$id));
                    $this->session->set_flashdata('success_msg', $this->getNotification('recUpSuc'));
                }else{
                    $data['createdTime'] = date("Y-m-d h:i:s");
                    $data['updatedTime'] = date("Y-m-d h:i:s");
                    $result = $this->Service->insert_row(TBL_OFFICE_EXPENSE,$data);
                    $this->session->set_flashdata('success_msg', $this->getNotification('recAddSuc'));
                }
                redirect(base_url('expense/monthlyExpense'));
            }
        }
        $data['view'] = 'expense/monthlyExpense_edit';
        $this->renderAdmin($data);
    }

    public function monthlyExpense_delete($id = 0) {
        $id = $this->input->post('expid');
        $data = $this->Service->set_delete(TBL_OFFICE_EXPENSE,array('expID'=>$id));
        if(!empty($data)){ echo 1;}else{ echo 0;};
    }

     //Employee salary  
     public function employeeSalary(){
        $data['title'] = "Employee Salary";
        $data['menu'] = "employee salary";
        $data['submenu'] = "Expense";
        $loginUserID = $this->session->userdata('userID');
        $userRole = $this->session->userdata('user_type');
        if($userRole == 3){
            $resultList= $this->Service->get_all(TBL_EMPLOYEE_SALARY,'*',[]);
        }else{
            $resultList= $this->Service->get_all(TBL_EMPLOYEE_SALARY,'*',array('managerID'=>$loginUserID));
        }
        $data['resultList'] = $resultList;
        $data['view'] = 'expense/employeeSalary_list';
        $this->renderAdmin($data);
    }
    
    public function employeeSalary_edit($id=""){
        $data['title'] = "Employee Salary";
        $data['menu'] = "Employee Salary";
        $data['submenu'] = "Expense";
        $loginUserID = $this->session->userdata('userID');
        $data['employeeData'] = $this->Service->get_all(TBL_EMPLOYEE,'*',[]);
        if($id!=""){
            // $data['generalList'] = $this->Service->get_all(TBL_GENERAL_EXPENSE,'*',array('genExpID'=>'0'));
            $data['title'] = "Edit Employee Salary";
            $data['menu'] = "Edit Employee Salary";
            $rowData = $this->Service->get_row(TBL_EMPLOYEE_SALARY,'*',array('salID'=>$id));
            if(empty($rowData)){
                redirect(base_url('expense/employeeSalary'));
            }
            $data['rowData'] = $rowData;
        }
        if(!empty($_POST['submit'])){
            $this->form_validation->set_rules('employeeID', 'Employee', 'trim|required');
            $this->form_validation->set_rules('payDate', 'Date', 'trim|required');
            $this->form_validation->set_rules('voucher', 'voucher', 'trim|required');
            $this->form_validation->set_rules('salaryMonth', 'Salary Month', 'trim|required');
            $this->form_validation->set_rules('amount', 'Amount', 'trim|required');
            $this->form_validation->set_rules('desc', 'Description', 'trim|required');
            if ($this->form_validation->run() != FALSE) {
                // date('Y-m-d',strtotime($this->input->post('salaryMonth'))),
                $data = array(
                    'managerID' => $loginUserID,
                    'employeeID' => $this->input->post('employeeID'),
                    'payDate' => $this->input->post('payDate'),
                    'voucher' => $this->input->post('voucher'),
                    'salaryMonth' => $this->input->post('salaryMonth'),
                    'amount' => $this->input->post('amount'),
                    'desc' => $this->input->post('desc'),
                );
                if($id!=""){
                    $data['updatedTime'] = date("Y-m-d h:i:s");
                    $this->Service->update_row(TBL_EMPLOYEE_SALARY,$data, array('salID'=>$id));
                    $this->session->set_flashdata('success_msg', $this->getNotification('recUpSuc'));
                }else{
                    $data['createdTime'] = date("Y-m-d h:i:s");
                    $data['updatedTime'] = date("Y-m-d h:i:s");
                    $result = $this->Service->insert_row(TBL_EMPLOYEE_SALARY,$data);
                    $this->session->set_flashdata('success_msg', $this->getNotification('recAddSuc'));
                }
                redirect(base_url('expense/employeeSalary'));
            }
        }
        $data['view'] = 'expense/employeeSalary_edit';
        $this->renderAdmin($data);
    }

    public function employeeSalary_delete($id = 0) {
        $id = $this->input->post('expid');
        $data = $this->Service->set_delete(TBL_EMPLOYEE_SALARY,array('salID'=>$id));
        if(!empty($data)){ echo 1;}else{ echo 0;};
    }
    
}
?>

