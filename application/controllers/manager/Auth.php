<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        if ($this->session->has_userdata('is_manager_login')) {
            redirect('dashboard');
        }
        $data = array();
        //echo '<pre>';print_r($_POST);die;
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules('username', 'Username', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            if ($this->form_validation->run() != FALSE) {
                $username = $this->input->post('username');
                $password = $this->input->post('password');
                $result = $this->user->managerLogin($username, md5($password));
                // echo "<pre>"; print_r($result); die();
                if (!empty($result)) {
                    $admin_data = array(
                        'userID' => $result->userID,
                        'name' => $result->name,
                        'branch' => $result->branch,
                        'is_manager_login' => TRUE,
                        'user_type' => 1,
                    );
                    $this->session->set_userdata($admin_data);
                    redirect(base_url('dashboard'), 'refresh');
                } else {
                    $data['msg'] = $this->getNotification('invalidLogin');
                }
            }
        }
        $this->load->view('manager/login', $data);
    }
    
    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url('manager'), 'refresh');
    }
    
}
?>