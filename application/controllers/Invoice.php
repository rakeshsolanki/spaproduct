<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $userRole = $this->session->userdata('user_type');
        if($userRole==""){
            redirect('auth/login');
        }
    }
    
    public function index() {
        $data['title'] = "Invoice";
        $data['menu'] = "invoice";
        $loginUserID = $this->session->userdata('userID');
        $userRole = $this->session->userdata('user_type');
        if($userRole == 3){
            if(isset($_GET['branch'])){
                $data['resultList'] = $this->Service->get_all(TBL_INVOICE,'*',array('managerID'=>$_GET['branch']),'invoiceID','DESC');
            }else{
                $data['resultList'] = $this->Service->get_all(TBL_INVOICE,'*',[],'invoiceID','DESC');
            }
        }else{
            $data['resultList'] = $this->Service->get_all(TBL_INVOICE,'*',array('managerID'=>$loginUserID),'invoiceID','DESC');
        }
        // $data['resultList'] = $this->Service->get_all(TBL_INVOICE,'*',array('managerID'=>$loginUserID),'invoiceID','DESC');
        $data['view'] = 'invoice/page_list';
        $this->renderAdmin($data);
    }
    
    public function add($id="") {
        $data['title'] = "Add Invoice";
        $data['menu'] = "add invoice";
        $loginuserID = $this->session->userdata('userID');
        $data['treatmentList'] = $this->Service->get_all(TBL_TREATMENT,'*',array('isActive'=>1),'treatmentID','DESC');
        $data['productList'] = $this->Service->get_all(TBL_PRODUCT,'*',array(),'productID','ASC');
        $data['employeeList'] = $this->Service->get_all(TBL_EMPLOYEE,'*',array(),'employeeID','ASC');
        if($id!=""){
            $data['title'] = "Edit Invoice";
            $data['menu'] = "edit invoice";
            $rowData = $this->Service->get_row(TBL_INVOICE,'*', array('invoiceID'=>$id));
            $data['rowData'] = $rowData;
        }
        if ($this->input->post('submit')) {
            // echo "<pre>"; print_r($_POST); die();
            $this->form_validation->set_rules('customerName', 'Full name', 'required');
            $this->form_validation->set_rules('invoiceDate', 'Invoice Date', 'trim|required');
            $this->form_validation->set_rules('customerMobile', 'Mobile', 'trim|required');
            $this->form_validation->set_rules('inTime', 'In Time', 'trim|required');
            $this->form_validation->set_rules('duration', 'Duration', 'required');
            $this->form_validation->set_rules('treatmentID[]', 'Treatment', 'required');
            $this->form_validation->set_rules('employeeID[]', 'Therapist', 'required');
            if ($this->form_validation->run() != FALSE) {
                $duration = $this->input->post('duration');
                $saveData = array(
                    'membershipID' => (isset($_POST['membershipID']))?$this->input->post('membershipID'):"",
                    'customerName' => $this->input->post('customerName'),
                    'customerMobile' => $this->input->post('customerMobile'),
                    'payMode' => (isset($_POST['payMode'])) ? implode(',',$this->input->post('payMode')):'',
                    'amount' => (isset($_POST['amount']))?$this->input->post('amount'):0,
                    'yslip' => (isset($_POST['yslip']))?$this->input->post('yslip'):"",
                    'invoiceDate' => date('Y-m-d',strtotime($this->input->post('invoiceDate'))),
                    'duration' => $duration,
                    'inTime' => $this->input->post('inTime'),
                    'block' => (isset($_POST['block']))?$this->input->post('block'):"",
                    'treatmentID' => (isset($_POST['treatmentID'])) ? implode(',',$this->input->post('treatmentID')):'' ,
                    'product' => (isset($_POST['product'])) ? implode(',',$this->input->post('product')):'' ,
                    'employeeID' => (isset($_POST['employeeID'])) ? implode(',',$this->input->post('employeeID')):'' ,
                    'isActive' => 1,
                    'updatedTime' => date("Y-m-d h:i:s")
                );
                
                if(isset($_POST['inTime']) && $_POST['inTime']!=""){
                    $endTime = strtotime("+$duration minutes", strtotime($_POST['inTime']));
                    $outtime = date('h:i A', $endTime);
                    $saveData['outTime'] = $outtime;
                }
                if($id!=""){
                    $this->Service->update_row(TBL_INVOICE,$saveData, array('invoiceID'=>$id));
                    $this->session->set_flashdata('success_msg', $this->getNotification('recUpSuc'));
                    if(isset($_POST['branch']) && $_POST['branch'] != ''){  
                        redirect(base_url('invoice?branch='.$_POST['branch']));
                     }else{
                        redirect(base_url('invoice'));
                     }
                    
                }else{
                    $membershipID = "";
                    $branchName = (isset($loginuserID))?$this->Service->getBranceName($loginuserID):"";
                    $smsMobile = (isset($saveData['customerMobile']))?$saveData['customerMobile']:"";
                    $username = (isset($saveData['customerName']))?$saveData['customerName']:"";
                    $treatName = (isset($saveData['treatmentID']))?$this->Service->getTreatmentName($saveData['treatmentID']):"";
                    $startTime = (isset($saveData['inTime']))?$saveData['inTime']:"";
                    $endTime = (isset($saveData['outTime']))?$saveData['outTime']:"";
                    $payAmount = (isset($saveData['amount']))?$saveData['amount']:"";
                    $payMode = (isset($saveData['payMode']))?$saveData['payMode']:"";
                    
                    if(!empty($_POST['membershipID'])){
                        $membershipID = $_POST['membershipID'];
                        $memberData = $this->Service->get_row(TBL_CUSTOMER,'*', array('membershipID'=>$membershipID));
                        $smsMobile = (isset($memberData['mobile']))?$memberData['mobile']:"";
                        $username = (isset($memberData['name']))?$memberData['name']:"";
                        $totalTreatment = (isset($memberData['totalTreatment']))?$memberData['totalTreatment']:"";
                        $remaiTreatment = $this->Service->getRemainingTreatment($membershipID);
                        if($remaiTreatment == 0){
                            $this->session->set_flashdata('success_msg', $this->getNotification('memberShipOver'));
                            redirect(base_url('invoice/add'));
                        }elseif($memberData['expireDate'] == date('Y-m-d')){
                            $this->session->set_flashdata('success_msg', $this->getNotification('memberShipExpire'));
                            redirect(base_url('invoice/add'));
                        }
                        $remainingTreatment = ($memberData['remainingTreatment'] - 1);
                        $this->Service->update_row(TBL_CUSTOMER,array('remainingTreatment'=>$remainingTreatment), array('membershipID'=>$membershipID));
                    }
                    $saveData['managerID'] = $loginuserID;
                    $saveData['createdTime'] = date("Y-m-d h:i:s");
                    // echo '<pre>';print_r($remainingTreatment);die;
                    $this->Service->insert_row(TBL_INVOICE,$saveData);

                    // send invoice msg to user

                    $usermsg = $branchName." BRANCH \n".$username;
                    $usermsg .= "\n".$saveData['customerMobile'];
                    if($membershipID!=""){
                        $usermsg .= "\nA/C TRTMNT-".$totalTreatment."\nAVAIL. TRTMNT-".$remainingTreatment;
                    }
                    $usermsg .= "\n".$treatName."\n".$startTime."-".$endTime;
                    if($membershipID!=""){
                        $usermsg .= "\nM.ID : ".$membershipID;
                    }else{
                        $usermsg .= "\nAmount : ".$payAmount."/- ".$payMode;
                    }
                    $usermsg .= "\nTHANKS FOR VISIT";
                    // $adminNumbner = "9173171094";
                    $adminNumbner = $this->Service->getAdminNumber();
                    if($membershipID!=""){
                        if(isset($memberData['msgStatus']) && $memberData['msgStatus']==1){
                            $this->Service->sendSMS($smsMobile, $usermsg);
                        }
                        $this->Service->sendSMS($adminNumbner, $usermsg);
                    }else{
                        $this->Service->sendSMS($smsMobile, $usermsg);
                        $this->Service->sendSMS($adminNumbner, $usermsg);
                    }
                    $this->session->set_flashdata('success_msg', $this->getNotification('recAddSuc'));
                    redirect(base_url('invoice'));
                }
            }
        }
        $data['view'] = 'invoice/page_edit';
        $this->renderAdmin($data);
    }
    
    public function delete($id = 0) {
        $id= $this->input->post('expid');
        if($id){
            $result= $this->Service->set_delete(TBL_INVOICE,array('invoiceID'=>$id));
            if(!empty($result)){
                echo 1;
            }else{
                echo 0;
            }
        }
    }
    
    public function view($id = 0) {
        $data['title'] = "View User Detail";
        $data['menu'] = "view user";
        $data['userID'] = $id;
        $data['user'] = $this->user->get_users(array('userID'=>$id), true);
        $data['userTransactions'] = '';
        $data['chocoFlouzTransactions'] = '';
        $data['view'] = 'users/page_view';
        $this->renderAdmin($data);
    }
    
    public function getMembershipData() {
        $result = "";
        if(isset($_POST['membership']) && $_POST['membership']!=""){
            $memberID = $_POST['membership'];
            $result = $this->Service->get_row(TBL_CUSTOMER,'*', array('membershipID'=>$memberID));
        }
        echo json_encode($result); exit();
    }
}
?>