<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $userRole = $this->session->userdata('user_type');
        if($userRole==""){
            redirect('auth/login');
        }
    }

    public function index() {
        $data['title'] = "Managers";
        $data['menu'] = "managers";
        $data['all_users'] = $this->user->get_users(array('userRole'=>1,'order_high'=>'userID'));
        $data['view'] = 'users/user_list';
        $this->renderAdmin($data);
    }
    
    public function add($id="") {
        $data['title'] = "Add User";
        $data['menu'] = "add user";
        if($id!=""){
            $data['title'] = "Edit User";
            $data['menu'] = "edit user";
            $userData = $this->user->get_users(array('userID'=>$id), true);
            $data['user'] = $userData;
        }
        if ($this->input->post('submit')) {
            //echo "<pre>"; print_r($this->input->post()); die();
            $this->form_validation->set_rules('name', 'Full name', 'required');
            $this->form_validation->set_rules('branch', 'Branch name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('mobile', 'Mobile', 'trim');
            if($id==""){
                $this->form_validation->set_rules('password', 'Password', 'trim|required');
            }
            //$this->form_validation->set_rules('userRole', 'User Role', 'trim|required');
            if ($this->form_validation->run() != FALSE) {
                $saveData = array(
                    'name' => $this->input->post('name'),
                    'branch' => $this->input->post('branch'),
                    // 'gender' => $this->input->post('gender'),
                    'email' => $this->input->post('email'),
                    'dob' => $this->input->post('dob'),
                    'mobile' => $this->input->post('mobile'),
                    'userRole' => 1,
                    'updatedTime' => date("Y-m-d h:i:s")
                );
                // if(!isset($userData['username']) && ($userData['username']=="" || $userData['username']==NULL)){
                //     $saveData['username'] = $this->Service->generateMembershipID();
                // }
                /*if (isset($_FILES['picture']['name']) && $_FILES['picture']['error'] == 0) {
                    $temp_file = $_FILES['picture']['tmp_name'];
                    $img_name = "tray_img" . mt_rand(10000, 999999999) . time();
                    $path = $_FILES['picture']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    $data['picture'] = $img_name . "." . $ext;
                    $fileurl = PROFILE . $data['picture'];
                    if (isset($userData['picture']) && $userData['picture'] != "" && file_exists(PROFILE . $userData['picture'])) {
                        unlink(PROFILE . $userData['picture']);
                    }
                    move_uploaded_file($temp_file, $fileurl);
                }*/
                if($id!=""){
                    if($this->input->post('password')!=""){
                        $saveData['password'] = md5($this->input->post('password'));
                        $saveData['visible_pass'] = $this->input->post('password');
                    }
                    //$data = $this->security->xss_clean($saveData);
                    $result = $this->user->edit_user($saveData, $id);
                    $this->session->set_flashdata('success_msg', $this->getNotification('recUpSuc'));
                    redirect(base_url('users'));
                }else{
                    $saveData['username'] = $this->Service->generateMembershipID();
                    $saveData['password'] = md5($this->input->post('password'));
                    $saveData['visible_pass'] = $this->input->post('password');
                    $saveData['createdTime'] = date("Y-m-d h:i:s");
                    $result = $this->user->add_user($saveData);
                    $this->session->set_flashdata('success_msg', $this->getNotification('recAddSuc'));
                    redirect(base_url('users'));
                }
            }
        }
        $data['view'] = 'users/user_edit';
        $this->renderAdmin($data);
    }

    public function view($id = 0) {
        $data['title'] = "View User Detail";
        $data['menu'] = "view user";
        $data['userID'] = $id;
        $data['user'] = $this->user->get_users(array('userID'=>$id), true);
        $data['userTransactions'] = '';
        $data['chocoFlouzTransactions'] = '';
        $data['view'] = 'users/user_view';
        $this->renderAdmin($data);
    }
    
    public function delete() {
        $id = $this->input->post('userid');
        $resul = $this->user->delete_user($id);
        // $resul = $this->session->set_flashdata('success_msg', $this->getNotification('recDelSuc'));
        if(!empty($resul)){
            echo 1;
        }else{
            echo 0;
        }
        // redirect(base_url('users'));

    }

    public function userIsactive() {
        $userID = $this->input->post('userid');
        $status = $this->input->post('status');
        if($this->user->edit_user(array('isActive'=> $status),$userID)){
            echo '1';
        }else{ echo '0';}
    }

    public function userSmsPermision() {
        $customerID = $this->input->post('userid');
        $status = $this->input->post('status');
        if($this->Service->update_row(TBL_CUSTOMER,array('msgStatus'=> $status), array('customerID'=>$customerID))){
            echo '1';
        }else{ echo '0';}
    }

    public function employeeList() {
        $data['title'] = "Employee";
        $data['menu'] = "employee";
        $data['resultList'] = $this->Service->get_all(TBL_EMPLOYEE,'*',array(),'employeeID','ASC');
        $data['view'] = 'users/employee_list';
        $this->renderAdmin($data);
    }
    
    public function addEmployee($id="") {
        $data['title'] = "Add Employee";
        $data['menu'] = "add employee";
        if($id!=""){
            $data['title'] = "Edit Employee";
            $data['menu'] = "edit employee";
            $rowData = $this->Service->get_row(TBL_EMPLOYEE,'*', array('employeeID'=>$id));
            $data['rowData'] = $rowData;
        }
        if ($this->input->post('submit')) {
            //echo "<pre>"; print_r($this->input->post()); die();
            $this->form_validation->set_rules('name', 'Full name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('mobile', 'Mobile', 'trim|required');
            $this->form_validation->set_rules('address', 'Address', 'trim');
            $this->form_validation->set_rules('specialist', 'Specialist', 'trim|required');
            if ($this->form_validation->run() != FALSE) {
                $saveData = array(
                    'name' => $this->input->post('name'),
                    'email' => $this->input->post('email'),
                    'mobile' => $this->input->post('mobile'),
                    'altMobile' => $this->input->post('altMobile'),
                    'dob' => date('Y-m-d',strtotime($this->input->post('dob'))),
                    'gender' => $this->input->post('gender'),
                    'specialist' => $this->input->post('specialist'),
                    'address' => $this->input->post('address'),
                    'joinDate' => date('Y-m-d',strtotime($this->input->post('joinDate'))),
                    // 'isActive' => $this->input->post('isActive'),
                    'updatedTime' => date("Y-m-d h:i:s")
                );
                if($id!=""){
                    $this->Service->update_row(TBL_EMPLOYEE,$saveData, array('employeeID'=>$id));
                    $this->session->set_flashdata('success_msg', $this->getNotification('recUpSuc'));
                    redirect(base_url('employee'));
                }else{
                    $saveData['createdTime'] = date("Y-m-d h:i:s");
                    $this->Service->insert_row(TBL_EMPLOYEE,$saveData);
                    $this->session->set_flashdata('success_msg', $this->getNotification('recAddSuc'));
                    redirect(base_url('employee'));
                }
            }
        }
        $data['view'] = 'users/employee_edit';
        $this->renderAdmin($data);
    }
    
    public function deleteEmployee() {
        $employeeid = $this->input->post('employeeid');
        $result = $this->Service->set_delete(TBL_EMPLOYEE,array('employeeID'=>$employeeid));
        // $this->session->set_flashdata('success_msg', $this->getNotification('recDelSuc'));
        // redirect(base_url('employee'));
        if(!empty($result)){ echo 1; }else{ echo 0; }
    }

    public function viewEmployee($id = 0) {
        $data['title'] = "View User Detail";
        $data['menu'] = "view user";
        $data['userID'] = $id;
        $data['user'] = $this->user->get_users(array('userID'=>$id), true);
        $data['userTransactions'] = '';
        $data['chocoFlouzTransactions'] = '';
        $data['view'] = 'users/employee_view';
        $this->renderAdmin($data);
    }
    
}
?>

