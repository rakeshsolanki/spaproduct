<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    public function __construct() {
        parent::__construct();
        // if (!$this->session->has_userdata('is_admin_login')) {
        //     redirect('auth/login');
        // }
        $userRole = $this->session->userdata('user_type');
        if($userRole==""){
            redirect('auth/login');
        }
    }
    
    public function index() {
        $userID = $this->session->userdata('userID');
        $userRole = $this->session->userdata('user_type');
        if($userRole==1){
            $data['totalManagerCount'] = $this->user->get_users(array('userRole' => 1,'isActive' => 1), FALSE, TRUE);
            $data['totalEmployeeCount'] = count($this->Service->get_all(TBL_EMPLOYEE,'*',array()));
            $data['totalCustomersCount'] = count($this->Service->get_all(TBL_CUSTOMER,'*',array('managerID'=>$userID)));
            $data['totalInvoiceCount'] = count($this->Service->get_all(TBL_INVOICE,'*',array('managerID'=>$userID)));
            $data['view'] = 'manager/dashboard';
            $this->renderAdmin($data);
        }else{
            $managerList = $this->user->get_users(array('userRole' => 1,'isActive' => 1));
            $data['managerList'] = $managerList;
            $data['totalManagerCount'] = count($managerList);
            $data['totalEmployeeCount'] = count($this->Service->get_all(TBL_EMPLOYEE,'*',array()));
            $data['totalCustomersCount'] = count($this->Service->get_all(TBL_CUSTOMER,'*',array()));
            $data['totalInvoiceCount'] = count($this->Service->get_all(TBL_INVOICE,'*',array()));
            $data['view'] = 'dashboard/dashboard';
            $this->renderAdmin($data);
        }
    }

    //for change password
    public function changePassword() {
        $loginuserID = $this->session->userdata('userID');
        $userdata = $this->user->get_users(array('userID' => $loginuserID), TRUE);
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules('oldPassword', 'Old Password', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            $this->form_validation->set_rules('confirm_pwd', 'Confirm Password', 'trim|required|matches[password]');
            if ($this->form_validation->run() != FALSE) {
                
                if(isset($userdata['password']) && $userdata['password']==md5($this->input->post('oldPassword'))){
                    $data = array(
                        'password' => md5($this->input->post('password')),
                        'visible_pass' => $this->input->post('password')
                    );
                    $result = $this->user->change_pwd($data, $loginuserID);
                    if ($result) {
                        $this->session->set_flashdata('success_msg', $this->getNotification('pwdChangeSuc'));
                        redirect(base_url('dashboard/changePassword'));
                    }
                }else{
                    $this->session->set_flashdata('error_msg', $this->getNotification('passwordNotMatch'));
                    redirect(base_url('dashboard/changePassword'));
                }
            }
        }
        $data['view'] = 'auth/change_pwd';
        $this->renderAdmin($data);
    }
    
    public function profile() {
        $data['title'] = "Profile";
        $data['menu'] = "Profile";
        $loginuserID = $this->session->userdata('userID');
        $userRole = $this->session->userdata('user_type');
        $data['userRole'] = $userRole;
        $userdata = $this->user->get_users(array('userID' => $loginuserID), TRUE);
        $year = date('Y');
        if(isset($_GET['graphYear']) && $_GET['graphYear']!=""){
            $year = $_GET['graphYear'];
        }
        if($userRole==3){
            $data['yearIncomeJson'] = $this->user->getYearIncomeJson('',$year);
        }else{
            $data['yearIncomeJson'] = $this->user->getYearIncomeJson($loginuserID,$year);
        }
        $data['user'] = $userdata;
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('mobile', 'Mobile', 'trim|required');
            if ($this->form_validation->run() != FALSE) {
                $data = array(
                    'name' => $this->input->post('name'),
                    'email' => $this->input->post('email'),
                    'mobile' => $this->input->post('mobile'),
                    'branch' => isset($_POST['branch'])?$this->input->post('branch'):"",
                    'dob' => isset($_POST['dob'])?$this->input->post('dob'):"",
                    'address' => isset($_POST['address'])?$this->input->post('address'):"",
                );
                
                $admin_data = array('name'=>$this->input->post('name'));
                $this->session->set_userdata($admin_data);
                $result = $this->user->change_pwd($data, $loginuserID);
                if ($result) {
                    $this->session->set_flashdata('success_msg', $this->getNotification('recUpSuc'));
                    redirect(base_url('dashboard/profile'));
                }
            }
        }
        $data['view'] = 'dashboard/profile';
        $this->renderAdmin($data);
    }

    public function treatments() {
        $data['title'] = "Treatments";
        $data['menu'] = "treatments";
        $data['resultList'] = $this->Service->get_all(TBL_TREATMENT,'*',array(),'treatmentID','ASC');
        $data['view'] = 'dashboard/treatment_list';
        $this->renderAdmin($data);
    }
    
    public function addTreatment($id="") {
        $data['title'] = "Add Treatment";
        $data['menu'] = "add Treatment";
        if($id!=""){
            $data['title'] = "Edit Treatment";
            $data['menu'] = "edit Treatment";
            $rowData = $this->Service->get_row(TBL_TREATMENT,'*', array('treatmentID'=>$id));
            $data['rowData'] = $rowData;
        }
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules('name', 'Full name', 'required');
            $this->form_validation->set_rules('duration', 'Duration', 'trim|required');
            if ($this->form_validation->run() != FALSE) {
                $saveData = array(
                    'name' => $this->input->post('name'),
                    'duration' => $this->input->post('duration'),
                    'updatedTime' => date("Y-m-d h:i:s")
                );
                if($id!=""){
                    $this->Service->update_row(TBL_TREATMENT,$saveData, array('treatmentID'=>$id));
                    $this->session->set_flashdata('success_msg', $this->getNotification('recUpSuc'));
                    redirect(base_url('treatments'));
                }else{
                    $saveData['createdTime'] = date("Y-m-d h:i:s");
                    $this->Service->insert_row(TBL_TREATMENT,$saveData);
                    $this->session->set_flashdata('success_msg', $this->getNotification('recAddSuc'));
                    redirect(base_url('treatments'));
                }
            }
        }
        $data['view'] = 'dashboard/treatment_edit';
        $this->renderAdmin($data);
    }
    
    public function deleteTreatment($id = 0) {
        $treatmentid = $this->input->post('treatmentid');
        $result = $this->Service->set_delete(TBL_TREATMENT,array('treatmentID'=>$treatmentid));
        if(!empty($result)){ echo 1; }else{ echo 0; }
    }

    public function treatmentIsactive() {
        $treatmentid = $this->input->post('treatmentid');
        $status = $this->input->post('status');
        if($this->Service->update_row(TBL_TREATMENT,array('isActive'=> $status), array('treatmentID'=>$treatmentid))){ echo '1'; }else{ echo '0';}
    }

    public function searchUserData(){
        $mobile = $this->input->post('mobile');
        $uu = $this->Service->searchClientData($mobile);
        echo '<pre>'; print_r($uu);die;
    }



}
?>

