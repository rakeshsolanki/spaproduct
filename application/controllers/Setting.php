<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $userRole = $this->session->userdata('user_type');
        if($userRole==""){
            redirect('auth/login');
        }
    }

    public function slider() {
        $data['title'] = "Slider";
        $data['menu'] = "Slider";
        $data['resultList'] = $this->Service->get_all(TBL_SLIDER,'*',[],'sliderID');
        $data['view'] = 'slider/page_list';
        $this->renderAdmin($data);
    }

    public function slider_add($id="") {
        $data['title'] = "Add Slider";
        $data['menu'] = "add slider";
        $data['categoryList'] = $this->Service->get_all(TBL_CATEGORY,'*',array('parentCategoryID'=>'0'));
        $data['stateList'] = $this->Service->get_all(TBL_STATE,'*');
        if($id!=""){
            $data['categoryList'] = $this->Service->get_all(TBL_CATEGORY,'*',array('parentCategoryID'=>'0'));
            $data['title'] = "Edit Slider";
            $data['menu'] = "edit slider";
            $rowData = $this->Service->get_row(TBL_SLIDER,'*',array('sliderID'=>$id));
            if(empty($rowData)){
                redirect(base_url('setting/slider'));
            }
            $data['rowData'] = $rowData;
        }
        if ($this->input->post('submit')) {
            // echo "<pre>"; print_r($this->input->post()); die();
            $this->form_validation->set_rules('title', 'Slider name', 'trim|required');
            if ($this->form_validation->run() != FALSE) {
                $data = array(
                    'title' => $this->input->post('title'),
                    'isActive' => $this->input->post('isActive'),
                );
                
                if (isset($_FILES['picture']['name']) && $_FILES['picture']['error'] == 0) {
                    $temp_file = $_FILES['picture']['tmp_name'];
                    $img_name = "tray_img" . mt_rand(10000, 999999999) . time();
                    $path = $_FILES['picture']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    $data['picture'] = $img_name . "." . $ext;
                    $fileurl = SLIDER . $data['picture'];
                    if (isset($rowData['picture']) && $rowData['picture'] != "" && file_exists(SLIDER . $rowData['picture'])) {
                        unlink(SLIDER . $rowData['picture']);
                    }
                    move_uploaded_file($temp_file, $fileurl);
                }

                if($id!=""){
                    $data['updatedTime'] = date("Y-m-d h:i:s");
                    //$data = $this->security->xss_clean($data);
                    $this->Service->update_row(TBL_SLIDER,$data, array('sliderID'=>$id));
                    $this->session->set_flashdata('success_msg', $this->getNotification('recUpSuc'));
                }else{
                    $data['createdTime'] = date("Y-m-d h:i:s");
                    $data['updatedTime'] = date("Y-m-d h:i:s");
                    $result = $this->Service->insert_row(TBL_SLIDER,$data);
                    $this->session->set_flashdata('success_msg', $this->getNotification('recAddSuc'));
                }
                redirect(base_url('setting/slider'));
            }
        }
        $data['view'] = 'slider/page_edit';
        $this->renderAdmin($data);
    }

    public function delete($id = 0) {
        $this->Service->set_delete(TBL_SLIDER,array('sliderID'=>$id));
        $this->session->set_flashdata('success_msg', $this->getNotification('recDelSuc'));
        redirect(base_url('setting/slider'));
    }
    
    public function userIsactive() {
        $userID = $this->input->post('userid');
        $status = $this->input->post('status');
        if($this->user->edit_user(array('isActive'=> $status),$userID)){
            echo '1';
        }else{
            echo '0';
        }
    }

}

?>

