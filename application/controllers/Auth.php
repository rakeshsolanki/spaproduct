<?php



defined('BASEPATH') OR exit('No direct script access allowed');



class Auth extends MY_Controller {



    public function __construct() {

        parent::__construct();

        //$this->load->model('auth_model', 'auth_model');
    }



    public function index() {

        if ($this->session->has_userdata('is_admin_login')) {
            redirect('dashboard');
        } else {
            redirect('auth/login');
        }
    }



    //for admin login
    public function login() {
        if ($this->session->has_userdata('is_admin_login')) {
            redirect('dashboard');
        }

        //echo '<pre>';print_r($_POST);die;

        if ($this->input->post('submit')) {

            $this->form_validation->set_rules('email', 'Email', 'trim|required');

            $this->form_validation->set_rules('password', 'Password', 'trim|required');

            if ($this->form_validation->run() == FALSE) {

                $this->load->view('auth/login');

            } else {

                $data = array(

                    'email' => $this->input->post('email'),

                    'password' => $this->input->post('password')

                );
                $result = $this->user->admin_login($data);
                //echo "<pre>"; print_r($result); die();
                if ($result == TRUE) {
                    $admin_data = array(
                        'userID' => $result['userID'],
                        'admin_id' => $result['userID'],
                        'name' => $result['name'],
                        'is_admin_login' => TRUE,
                        'user_type' => 3,
                    );
                    $this->session->set_userdata($admin_data);
                    redirect(base_url('dashboard'), 'refresh');
                } else {

                    //$data['msg'] = 'Invalid Email or Password!';

                    $data['msg'] = $this->getNotification('invalidLogin');

                    $this->load->view('auth/login', $data);

                }

            }

        } else {

            $this->load->view('auth/login');

        }

    }
    
    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url('auth/login'), 'refresh');
    }
    

}

// end class

?>