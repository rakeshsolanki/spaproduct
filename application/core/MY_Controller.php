<?php
class MY_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('upload');
        // $this->front_db = $this->load->database('frontdb', TRUE);
    }

    public function getNotification($key=""){

        $result = "No notification found";

        if(!empty($key)){

            $query = $this->db->get_where(TBL_LANGUAGE, array('status' => 1,'language_key' => $key));

            $notificationData = $query->row_array();

            if(!empty($notificationData)){

                $result = $notificationData['language_value'];

            }

        }
        return $result;

    }

    public function generateLog($response=""){

        //$requestParameter = file_get_contents('php://input');

        $requestParameter = $_REQUEST;

        $requestUrl = base_url(uri_string());

        //echo "<pre>"; print_r($requestParameter); die();

        $log  = "User: ".$_SERVER['REMOTE_ADDR'].' - '.date("F j, Y, g:i a").PHP_EOL;

        $log .= "URL: ".$requestUrl.PHP_EOL;

        $log .= "Parameter: ".json_encode($requestParameter).PHP_EOL;

        if(!empty($response)){

            //$log .= "Response: ".json_encode($response).PHP_EOL;

        }

        $log .= "--------------------------------------------------".PHP_EOL.PHP_EOL;

        file_put_contents('application/logs/log_'.date("d_M_Y").'.log', $log, FILE_APPEND);

    }

    public function response($response) {
        if(empty($response['data'])){
            $response['data'] = null;
        }
        if(empty($response['status'])){
            $response['status'] = 0;
        }
        if(empty($response['message'])){
            $response['message'] = "No Message found.";
        }
        header('Content-Type:application/json');
        echo json_encode($response);
        $this->generateLog($response);
        exit();
    }

    //for render front template

    public function render($data=""){

        $this->load->view('layout/header', $data);

        if(!empty($data['view'])){

            if(is_array($data['view'])){

                foreach($data['view'] as $view){

                    $this->load->view($view);

                }

            }else{

                $this->load->view($data['view']);

            }

        }

        $this->load->view('layout/footer', $data);

    }



    //for render admin template
    public function renderAdmin($data=""){
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        if(!empty($data['view'])){
            if(is_array($data['view'])){
                foreach($data['view'] as $view){
                    $this->load->view($view);
                }
            }else{
                $this->load->view($data['view']);
           }
        }
        $this->load->view('layout/footer', $data);

    }
    //for render employee template
    public function renderEmployee($data=""){
        $this->load->view('employee/layout/header', $data);
        $this->load->view('employee/layout/navbar', $data);
        $this->load->view('employee/layout/sidebar', $data);
        if(!empty($data['view'])){
            if(is_array($data['view'])){
                foreach($data['view'] as $view){
                    $this->load->view($view);
                }
            }else{
                $this->load->view($data['view']);
            }
        }
        $this->load->view('employee/layout/footer', $data);
    }



    //for get site setting value

    public function getSiteSetting($key=""){

        $result = "";

        if(!empty($key)){

            $query = $this->db->get_where(TBL_SITE_SETTING, array('isActive' => 1,'isDelete' => 0,'settingKey' => $key));

            $resultData = $query->row_array();

            if(!empty($resultData)){

                $result = $resultData['settingValue'];

            }

        }

        return $result;

    }



    //for check user login or not

    public function checkUserLogin(){

        $result = "";

        if ($this->session->has_userdata('is_user_login') && $this->session->has_userdata('userID')) {

            $result = $this->session->userdata('userID');

        }

        return $result;

    }

    

}

?>

