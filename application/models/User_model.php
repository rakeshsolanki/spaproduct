<?php

class User_model extends MY_Model {
    
    public function admin_login($data) {
        $result = array();
        $query = $this->db->get_where(TBL_USERS, array('email' => $data['email'],'userRole' => 3,'isActive' => 1,'isDelete' => 0));
        if ($query->num_rows() != 0) {
            $userData = $query->row_array();
            //echo password_hash($data['password'], PASSWORD_BCRYPT)." - ".$userData['password']; die();
            if(md5($data['password']) == $userData['password']){
                $result = $userData;
            }
        }
        return $result;
    }
    
    public function managerLogin($username, $password) {
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $this->db->where('userRole', 1);
        return $this->db->get(TBL_USERS)->row();
    }

    //for check user login detail
    public function user_login($data) {
        $this->db->where('email', $data['email']);
        $this->db->where('password', $data['password']);
        $this->db->where('userRole', 1);
        return $this->db->get(TBL_USERS)->row();
    }

    public function change_pwd($data, $id) {
        $this->db->where('userID', $id);
        $this->db->update(TBL_USERS, $data);
        return true;
    }

    public function add_user($data) {
        $this->db->insert(TBL_USERS, $data);
        return $this->db->insert_id();
    }

    public function get_users($data = array(),$row = false,$no_of_row = false) {
        //echo '<pre>';print_r($data);die;
        $result = array();
        $this->db->select('*');
        $this->db->from(TBL_USERS);
        if(!empty($data)){
            if(!empty($data['email'])){
                $this->db->where('email', $data['email']);
            }
            if(!empty($data['name'])){
                $this->db->like('name', $data['name']);
            }
            if(!empty($data['search'])){
                $this->db->group_start();
                $this->db->or_like('ID', $data['search']);
                $this->db->or_like('email', $data['search']);
                $this->db->group_end();
            }
            if(!empty($data['mobile'])){
                $this->db->where('mobile', $data['mobile']);
            }
            if(!empty($data['password'])){
                $this->db->where('password', $data['password']);
            }
            if(!empty($data['authProvider'])){
                $this->db->where('authProvider', $data['authProvider']);
            }
            if(!empty($data['authID'])){
                $this->db->where('authID', $data['authID']);
            }
            if(isset($data['isDefaultEmployee']) && $data['isDefaultEmployee'] != ''){
                $this->db->where('isDefaultEmployee', $data['isDefaultEmployee']);
            }
            if(isset($data['isSocial']) && $data['isSocial'] != ''){
                $this->db->where('isSocial', $data['isSocial']);
            }
            if(!empty($data['otp'])){
                $this->db->where('otp', $data['otp']);
            }
            if(!empty($data['userRole'])){
                if(is_array($data['userRole'])){
                    $this->db->where_in('userRole', $data['userRole']);
                }else{
                    $this->db->where('userRole', $data['userRole']);
                }
            }
            if(!empty($data['userID'])){
                if(is_array($data['userID'])){
                    $this->db->where_in('userID', $data['userID']);
                }else{
                    $this->db->where('userID', $data['userID']);
                }
            }
            if(!empty($data['notUserID'])){
                if(is_array($data['notUserID'])){
                    $this->db->where_not_in('userID', $data['notUserID']);
                }else{
                    $this->db->where('userID !=', $data['notUserID']);
                }
            }
            if(!empty($data['accessToken'])){
                $this->db->where('accessToken', $data['accessToken']);
            }
            if(!empty($data['isVerified'])){
                $this->db->where('isVerified', $data['isVerified']);
            }
            
            if(!empty($data['isActive'])){
                $this->db->where('isActive', $data['isActive']);
            }
            if(!empty($data['isDelete'])){
                $this->db->where('isDelete', $data['isDelete']);
            }else{
                $this->db->where('isDelete', 0);
            }
            if(!empty($data['limit'])){
                $this->db->limit($data['limit']);
            }elseif(!empty($data['limit']) && !empty($data['offset'])){
                $this->db->limit($data['limit'], $data['offset']);
            }
            if(!empty($data['order'])){
                $this->db->order_by($data['order'], 'ASC');
            }
            if(!empty($data['order_high'])){
                $this->db->order_by($data['order_high'], 'DESC');
            }
            if(!empty($data['order_rand'])){
                $this->db->order_by($data['order_rand'], 'RANDOM');
            }
        }
        $query = $this->db->get();
        if($row){
            $result = $query->row_array();
        }else{
            $result = $query->result_array();
        }
        if($no_of_row){
            $result = count($result);
        }
        return $result;
    }

    public function edit_user($data, $id) {
        $this->db->where('userID', $id);
        $this->db->update(TBL_USERS, $data);
        return $id;
    }

    public function delete_user($id) {
        $this->db->where('userID', $id);
        $this->db->update(TBL_USERS, array('isDelete'=>1));
        return $id;
    }

    public function get_user_role($id) {
        $role = "User";
        if(!empty($id)){
            if($id == 2){
                $role = "Other User";
            }elseif($id == 3){
                $role = "Admin";
            }
        }
        return $role;
    }

    //for get user full name by UserId
    public function getUsername($id) {
        $result = "";
        $this->db->where('userID', $id);
        $rowData = $this->db->get(TBL_USERS)->row();
        if(!empty($rowData)){
            $result = $rowData->name;
        }
        return $result;
    }

    //for get user unique username by UserId
    public function getUserUniqueName($id) {
        $result = "";
        $this->db->where('userID', $id);
        $rowData = $this->db->get(TBL_USERS)->row();
        if(!empty($rowData)){
            $result = $rowData->username;
        }
        return $result;
    }

    //for check email exist or not
    public function check_email($email) {
        $this->db->where('email', $email);
        $this->db->where('isSocial', '0');
        $this->db->where('isDelete', 0);
        return $this->db->get(TBL_USERS)->row();
    }

    //for check mobile exist or not
    public function check_mobile($mobile) {
        $this->db->where('mobile', $mobile);
        $this->db->where('isDelete', 0);
        return $this->db->get(TBL_USERS)->row();
    }

    //for check email id when user update
    public function check_update_email($email ,$id) {
        $this->db->where('userID !=', $id);
        $this->db->where('email', $email);
        $this->db->where('isSocial', '0');
        return $this->db->get(TBL_USERS)->row();
    }

    //for check mobile when user update
    public function check_update_mobile($mobile ,$id) {
        $this->db->where('userID !=', $id);
        $this->db->where('mobile', $mobile);
        return $this->db->get(TBL_USERS)->row();
    }

    //for get user response
    public function getUserResponse($userID, $isFull=false,$isanother=false){
        $response = null;
        if(isset($userID) && $userID!=""){
            $userDetail = $this->user->get_users(array('userID'=>$userID),true);
            if(!empty($userDetail)){
                $response['userID'] = $userID."";
                $response['name'] = (isset($userDetail['name'])) ? $userDetail['name']."" : "";
                $response['username'] = (isset($userDetail['username'])) ? $userDetail['username']."" : "";
                $response['mobile'] = (isset($userDetail['mobile'])) ? $userDetail['mobile']."" : "";
                $response['email'] = (isset($userDetail['email'])) ? $userDetail['email']."" : ""; 
                $response['picture'] = (isset($userDetail['picture']) && $userDetail['picture']!="") ? base_url().PROFILE.$userDetail['picture']."" : "";
                $response['cover_picture'] = (isset($userDetail['cover_picture']) && $userDetail['cover_picture']!="") ? base_url().PROFILE.$userDetail['cover_picture']."" : "";
                $response['gender'] = (isset($userDetail['gender'])) ? $userDetail['gender']."" : "";
                $response['address'] = (isset($userDetail['address'])) ? $userDetail['address']."" : "";
                $response['createdTime'] = (isset($userDetail['createdTime'])) ? $userDetail['createdTime']."" : "";
                $response['category'] = (isset($userDetail['category'])) ? $userDetail['category']."" : "";
                if(isset($userDetail['category']) && $userDetail['category']!=""){
                    // getCategoryname
                }
                $response['followersCount'] = "0";
                $response['followingCount'] = "0";
                if($isanother==TRUE){
                    $response['followingStatus'] = "0";
                }
                if($isFull==true){
                    $response['dialCode'] = (isset($userDetail['dialCode'])) ? $userDetail['dialCode']."" : "";
                    $response['dob'] = (isset($userDetail['dob'])) ? $userDetail['dob']."" : "";
                    $response['bio'] = (isset($userDetail['bio'])) ? $userDetail['bio']."" : "";
                    $response['authProvider'] = (isset($userDetail['authProvider'])) ? $userDetail['authProvider']."" : "";
                    $response['authID'] = (isset($userDetail['authID'])) ? $userDetail['authID']."" : "";
                    $response['accessToken'] = (isset($userDetail['accessToken'])) ? $userDetail['accessToken']."" : "";
                    $response['deviceType'] = (isset($userDetail['deviceType'])) ? $userDetail['deviceType']."" : "";
                    $response['deviceToken'] = (isset($userDetail['deviceToken'])) ? $userDetail['deviceToken']."" : "";
                    $response['isActive'] = (isset($userDetail['isActive'])) ? $userDetail['isActive']."" : "";
                }
            }
        }
        return $response;
    }

    //for get user Notification data
    public function getUserNotification($data = array(),$row = false,$no_of_row = false) {
        $result = array();
        $this->db->select('*');
        $this->db->from(TBL_NOTIFICATION);
        if(!empty($data)){
            if(!empty($data['notifyID'])){
                $this->db->where('notifyID', $data['notifyID']);
            }
            if(!empty($data['userID'])){
                $this->db->where('userID', $data['userID']);
            }
            if(!empty($data['isActive'])){
                $this->db->where('isActive', $data['isActive']);
            }
            if(!empty($data['isDelete'])){
                $this->db->where('isDelete', $data['isDelete']);
            }else{
                $this->db->where('isDelete', 0);
            }
            if(!empty($data['limit']) && !empty($data['offset'])){
                $this->db->limit($data['limit'], $data['offset']);
            }elseif(!empty($data['limit'])){
                $this->db->limit($data['limit']);
            }
            if(!empty($data['order'])){
                $this->db->order_by($data['order'], 'ASC');
            }
            if(!empty($data['order_high'])){
                $this->db->order_by($data['order_high'], 'DESC');
            }
        }

        $query = $this->db->get();

        if($row){

            $result = $query->row_array();

        }else{

            $result = $query->result_array();

        }

        if($no_of_row){

            $result = count($result);

        }

        return $result;

    }

    //for save user Notification data
    public function saveUserNotification($data, $notifyID="") {
        if(!empty($notifyID)){
            $this->db->where('notifyID', $notifyID);
            $this->db->update(TBL_NOTIFICATION, $data);
            return $notifyID;
        }else{
            $this->db->insert(TBL_NOTIFICATION, $data);
            return $this->db->insert_id();
        }
    }

    //for remove user Notification
    public function removeUserNotification($notifyID="") {
        if(!empty($notifyID)){
            $this->db->where('notifyID', $notifyID);
            $this->db->update(TBL_NOTIFICATION, array('isDelete'=>1));
        }
        return $notifyID;
    }

    //for save user AccessToken data
    public function saveUserAccessToken($userID="", $deviceToken="", $data) {
        $checkRow = $this->Service->get_row(TBL_ACCESS_TOKEN,'*', array('userID'=>$userID,'deviceToken'=>$deviceToken));
        if(!empty($checkRow)){
            $data['updatedTime'] = date("Y-m-d h:i:s");
            $this->db->where('userID', $userID);
            $this->db->where('deviceToken', $deviceToken);
            $this->db->update(TBL_ACCESS_TOKEN, $data);
            return $checkRow['accessTokenID'];
        }else{
            $data['createdTime'] = date("Y-m-d h:i:s");
            $data['updatedTime'] = date("Y-m-d h:i:s");
            $this->db->insert(TBL_ACCESS_TOKEN, $data);
            return $this->db->insert_id();
        }
    }

    //for remove user AccessToken
    public function removeUserAccessToken($notifyID="") {
        if(!empty($notifyID)){
            $this->db->where('notifyID', $notifyID);
            $this->db->update(TBL_NOTIFICATION, array('isDelete'=>1));
        }
        return $notifyID;
    }
    //for income
    public function getTotalIncome($yearly ='',$today='',$branch=''){
        $this->db->select('sum(amount) as totalAmount');
        $this->db->from(TBL_CUSTOMER);
        $this->db->where('isDelete ',0);
        if($today != ''){
            $this->db->where('DAY(membershipDate)',date('d'));
        }
        if($branch != ''){
            $this->db->where('managerID ',$branch);
        }
        
        if($yearly != ''){
            $this->db->where('YEAR(membershipDate)',$yearly);
        }
        $CustIncome = $this->db->get()->row_array();

        $this->db->select('sum(amount) as totalAmount');
        $this->db->from(TBL_INVOICE);
        $this->db->where('isDelete ',0);
        if($today != ''){
            $this->db->where('DAY(invoiceDate)',date('d'));
        }
        if($yearly != ''){
            $this->db->where('YEAR(invoiceDate)',$yearly);
        }
        if($branch != ''){
            $this->db->where('managerID  ',$branch);
        }
        $income = $this->db->get()->row_array();
        $totalIncome = ($CustIncome['totalAmount'] + $income['totalAmount']);
        return $totalIncome;

    }

    // for get total income
    public function getMonthlyCustomerIncome($month="",$managerID="",$year=""){
        $currentYear = date('Y');
        $this->db->select('sum(amount) as totalAmount,MONTH(membershipDate) as month');
        $this->db->from(TBL_CUSTOMER);
        $this->db->where('isDelete','0');
        if($managerID!=""){
            $this->db->where('managerID',$managerID);
        }
        if($year!=""){
            $this->db->like('membershipDate', $year);
        }else{
            $this->db->like('membershipDate', date('Y'));
        }
        if($month!=""){
            $this->db->like('membershipDate', date('Y-'.$month));
        }else{
            $this->db->group_by('MONTH(membershipDate)');
        }
        $this->db->order_by('membershipDate');
        $customers = $this->db->get()->result_array();
        
        $totalMonthCustomer = array();
        if(!empty($customers)){
            foreach($customers as $row){
                $totalMonthCustomer[$row['month']] = $row['totalAmount'];
            }
        }
        // echo "<pre>"; print_r($totalMonthCustomer); die();
        return $totalMonthCustomer;
    }
    
    public function getMonthlyInvoiceIncome($month="",$managerID="",$year=""){
        $this->db->select('sum(amount) as totalAmount,MONTH(invoiceDate) as month');
        $this->db->from(TBL_INVOICE);
        $this->db->where('isDelete',0);
        if($year!=""){
            $this->db->like('invoiceDate', $year);
        }else{
            $this->db->like('invoiceDate', date('Y'));
        }
        if($managerID!=""){
            $this->db->where('managerID',$managerID);
        }
        if($month!=""){
            $this->db->like('invoiceDate', date('Y-'.$month));
        }else{
            $this->db->group_by('MONTH(invoiceDate)');
        }
        $this->db->order_by('invoiceDate');
        $invoice = $this->db->get()->result_array();
        $totalMonthInvoice = array();
        if(!empty($invoice)){
            foreach($invoice as $row){
                if($row['month']!=""){
                    $totalMonthInvoice[$row['month']] = $row['totalAmount'];
                }
            }
        }
        // echo "<pre>"; print_r($totalMonthInvoice); die();
        return $totalMonthInvoice;
    }

    public function modifyArr(&$arr,$basearray) {
        $array1 = $arr;
        $array2 = $basearray;
        if(empty($arr)){
            $array1 = $basearray;
            $array2 = $arr;
        }
        foreach($array1 as $k=>$v) {
            if(array_search($k,$array2)!==null) {
                if(isset($array2[$k]) && isset($array1[$k])){
                    $array1[$k]=$array2[$k]+$array1[$k];
                }
            }
        }
        return $array1;
    }
    // for get yearly json data
    public function getYearIncomeJson($managerID="", $year=""){
        $managerID ='';
        if($year==""){
            $year = date('Y');
        }
        $monthCustomerIncome = $this->getMonthlyCustomerIncome('',$managerID, $year);
        $monthInvoiceIncome = $this->getMonthlyInvoiceIncome('',$managerID, $year);
        $combineArray = $this->modifyArr($monthInvoiceIncome, $monthCustomerIncome);
        // echo "<pre>"; print_r($combineArray); die();
        $result = array();
        //if(!empty($combineArray)){
            for($i=1;$i<=12;$i++){
                $p = ($i-1);
                if(array_key_exists($i,$combineArray)){
                    $result[$p] = (float)$combineArray[$i];
                }else{
                    $result[$p] = 0;
                }
            }
        //}
        // echo "<pre>"; print_r($result); die();
        return json_encode($result);
    }
   
}
?>