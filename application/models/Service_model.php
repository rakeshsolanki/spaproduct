<?php
class Service_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }
    
    public function getNotification($key = "") {
        $result = "No notification found";
        if (!empty($key)) {
            $query = $this->db->get_where(TBL_LANGUAGE, array('status' => 1, 'language_key' => $key));
            $notificationData = $query->row_array();
            if (!empty($notificationData)) {
                $result = $notificationData['language_value'];
            }
        }
        return $result;
    }
    
    //for get site setting value
    public function getSiteSetting($key = "") {
        $result = "";
        if (!empty($key)) {
            $query = $this->db->get_where(TBL_SITE_SETTING, array('isActive' => 1, 'isDelete' => 0, 'settingKey' => $key));
            $resultData = $query->row_array();
            if (!empty($resultData)) {
                $result = $resultData['settingValue'];
            }
        }
        return $result;
    }

    //for get current currency data

    public function getCurrency() {
        return $this->getSiteSetting('currency');
    }

    public function generateLog($response=""){

        //$requestParameter = file_get_contents('php://input');

        $requestParameter = $_REQUEST;

        $requestUrl = base_url(uri_string());

        //echo "<pre>"; print_r($requestParameter); die();

        $log  = "User: ".$_SERVER['REMOTE_ADDR'].' - '.date("F j, Y, g:i a").PHP_EOL;

        $log .= "URL: ".$requestUrl.PHP_EOL;

        $log .= "Parameter: ".json_encode($requestParameter).PHP_EOL;

        if(!empty($response)){

            //$log .= "Response: ".json_encode($response).PHP_EOL;

        }

        $log .= "--------------------------------------------------".PHP_EOL.PHP_EOL;

        file_put_contents('application/logs/log_'.date("d_M_Y").'.log', $log, FILE_APPEND);

    }

    public function response($response) {
        if(empty($response['data'])){
            $response['data'] = null;
        }
        if(empty($response['status'])){
            $response['status'] = 0;
        }
        if(empty($response['message'])){
            $response['message'] = "No Message found.";
        }
        header('Content-Type:application/json');
        echo json_encode($response);
        $this->generateLog($response);
        exit();
    }

    public function rand_string($length = 8) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        return substr(str_shuffle($chars), 0, $length);
    }

    public function rand_code($length = 4) {
        $chars = "0123456789";
        return substr(str_shuffle($chars), 0, $length);
    }

    //for generate unique acces accessToken for user
    public function generateToken($uniqval = '', $len = 30) {
        if ($uniqval == "" || $uniqval == null) {
            $uniqval = time();
        }

        $hex = md5($uniqval . uniqid("", true));

        $pack = pack('H*', $hex);

        $uid = base64_encode($pack);

        $uid = str_replace("==", "", $uid);

        $uid = str_replace("+", "", $uid);

        $uid = str_replace("/", "", $uid);

        $uid = preg_replace("[^A-Z0-9]", "", strtoupper($uid));

        if ($len < 4)

            $len = 4;

        if ($len > 128)

            $len = 128;

        while (strlen($uid) < $len)

            $uid = $uid . $this->generateToken($uniqval, 20);

        return substr($uid, 0, $len);

    }
    
    //check user authentication by user id and access accessToken
    public function checkUserAuth($userID, $accessToken,$userRole = '') {
        // echo "<pre>"; print_r($accessToken); die();
        $response = null;
        $result = array();
        if ($userID != "" && $accessToken != "") {
            if(!empty($userRole)){
                $checkUserAvailable = $this->user->get_users(array('userID' => $userID,'userRole'=>$userRole), TRUE);
            }else{
                $checkUserAvailable = $this->user->get_users(array('userID' => $userID), TRUE);
            }
            if (empty($checkUserAvailable)) {
                $response['status'] = 0;
                $response['message'] = $this->getNotification('userNotFound');
                $this->response($response);
            }
            //  $checkUserVerify = $this->user->get_users(array('userID'=>$userID,'isVerified'=>1),true);
            //  if (empty($checkUserVerify)) {
            //      $response['message'] = $this->getNotification('verifyAccount');
            //      $this->response($response);
            //  }
            
            // $checkLogin = $this->user->get_users(array('userID' => $userID, 'accessToken' => $accessToken), TRUE);
            $checkLogin = $this->Service->get_row(TBL_ACCESS_TOKEN,'*', array('userID'=>$userID,'accessToken'=>$accessToken));
            if (empty($checkLogin)) {
                $response['status'] = 2;
                $response['message'] = $this->getNotification('tokenExpire');
                $this->response($response);
            } else {
                $result = $this->user->get_users(array('userID' => $userID), TRUE);
            }
        }
        return $result;
    }
    
    //for send email
    public function mailsend($recipient, $subject, $body, $from = NULL, $file = NULL, $bcc = NULL) {

        $this->load->library('email');

        $this->email->initialize($this->config->item('mail'));

        $from = empty($from) ? $this->config->item('from_email') : $from;

        $this->email->from($from, $this->config->item('website_name'));

        $this->email->subject($subject);

        $this->email->message($body);

        //Send mail

        if (!empty($recipient) && is_array($recipient)) {

            foreach ($recipient as $key => $value) {

                $this->email->to($value);

            }

        } elseif (!empty($recipient)) {

            $this->email->to($recipient);

        }

        if (!empty($file) && is_array($file)) {

            foreach ($file as $key => $value) {

                $this->email->attach($value);

            }

        } elseif (!empty($file)) {

            $this->email->attach($file);

        }

        $systemBCC = $this->config->item('bccmail');

        if (!empty($systemBCC) && is_array($systemBCC)) {

            foreach ($systemBCC as $key => $value) {

                $this->email->bcc($value);

            }

        } elseif (!empty($systemBCC)) {

            $this->email->bcc($systemBCC);

        }

        if (!empty($bcc) && is_array($bcc)) {

            foreach ($bcc as $key => $value) {

                $this->email->bcc($value);

            }

        } elseif (!empty($bcc)) {

            $this->email->bcc($bcc);

        }

        /* if($this->email->send()){

          echo "email sent";

          }else{

          $to = $this->input->post('email');

          mail($to, 'test', 'Other sent option failed');

          echo $this->input->post('email');

          show_error($this->email->print_debugger());

          } */

        return $this->email->send();
    }

    //for get getCountry list with options
    public function getCountry($data = array(), $row = false, $no_of_row = false) {

        $result = array();

        $this->db->select(TBL_COUNTRIES . '.*');

        $this->db->from(TBL_COUNTRIES);

        if (!empty($data)) {

            if (!empty($data['countryID']) || isset($data['countryID'])) {

                if (is_array($data['countryID'])) {

                    $this->db->where_in(TBL_COUNTRIES . '.countryID', $data['countryID']);

                } else {

                    $this->db->where(TBL_COUNTRIES . '.countryID', $data['countryID']);

                }

            }

            if (!empty($data['countryCode'])) {

                $this->db->where(TBL_COUNTRIES . '.countryCode', $data['countryCode']);

            }

            if (!empty($data['countryName'])) {

                $this->db->like(TBL_PROJECTS . '.countryName', $data['countryName']);

            }

            if (!empty($data['isActive'])) {

                $this->db->where(TBL_COUNTRIES . '.isActive', $data['isActive']);

            }

            if (!empty($data['limit']) && !empty($data['offset'])) {

                $this->db->limit($data['limit'], $data['offset']);

            } elseif (!empty($data['limit'])) {

                $this->db->limit($data['limit']);

            }

            if (!empty($data['order'])) {

                $this->db->order_by($data['order'], 'ASC');

            }

            if (!empty($data['order_high'])) {

                $this->db->order_by($data['order_high'], 'DESC');

            }

        }

        $query = $this->db->get();

        if ($row) {

            $result = $query->row_array();

        } else {

            $result = $query->result_array();

        }

        if ($no_of_row) {

            $result = count($result);

        }

        return $result;

    }

    //for get cms page value
    public function getCMSPage($data = array(), $row = false, $no_of_row = false) {

        $result = array();

        $this->db->select(TBL_PAGES . '.*');

        $this->db->from(TBL_PAGES);

        if (!empty($data)) {

            if (!empty($data['pageID'])) {

                $this->db->like(TBL_PAGES . '.pageID', $data['pageID']);

            }

            if (!empty($data['pageKey'])) {

                $this->db->like(TBL_PAGES . '.pageKey', $data['pageKey']);

            }

            if (!empty($data['isActive'])) {

                $this->db->where(TBL_PAGES . '.isActive', $data['isActive']);

            }

            if (!empty($data['isDelete'])) {

                $this->db->where(TBL_PAGES . '.isDelete', $data['isDelete']);

            }else{

                $this->db->where(TBL_PAGES . '.isDelete', 0);

            }

            if (!empty($data['limit']) && !empty($data['offset'])) {

                $this->db->limit($data['limit'], $data['offset']);

            } elseif (!empty($data['limit'])) {

                $this->db->limit($data['limit']);

            }

            if (!empty($data['order'])) {

                $this->db->order_by($data['order'], 'ASC');

            }

            if (!empty($data['order_high'])) {

                $this->db->order_by($data['order_high'], 'DESC');

            }

        }

        $query = $this->db->get();

        if ($row) {

            $result = $query->row_array();

        } else {

            $result = $query->result_array();

        }

        if ($no_of_row) {

            $result = count($result);

        }

        return $result;

    }

    //for send push notification to User
    public function sendPushNotification($deviceId, $title="", $message="",$userID="",$betID="") {
        // code
    }

    //for send SMS
    public function sendSMS($number, $msg="") {
        /*if($number!="" && $msg!=""){
            $url = "smsUrl&number=".$number."&message=".urlencode($msg);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_POST, 1);
            // curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
            $response = curl_exec($ch);
            curl_close($ch);
        }*/
        return true;
    }
    
    public function get_row($table, $columns = "*", $where = '1 = 1') {
        $this->db->select($columns)
        ->from($table)
        ->where($where)
        ->where('isDelete', 0)
        // ->where('isActive', 1)
        ->limit(1);
        return $this->db->get()->row_array();
    }

    public function searchClientData($mobile){
        $this->db->select('*');
        $this->db->where('customerMobile' ,$mobile);
        $this->db->where_or('membershipID' ,$mobile);
        $this->db->group_by('customerMobile');
        $clientData = $this->db->get()->row_array();
        return $clientData;
    }
    
    public function get_all($table, $columns = '*', $where = '1 = 1', $order_by = null, $sort_by = 'DESC', $limit="",$offset="",$group_by = '',$notequel='') {
        $this->db->select($columns)
        ->from($table)
        ->where($where)
        // ->where('isActive', 1)
        ->where('isDelete', 0);
        if(!empty($notequel)) {
            $this->db->where($notequel.'!=', '');
        }
        if($order_by != null) {
            $this->db->order_by($order_by, $sort_by);
        }
        
        if(!empty($limit)){
            $this->db->limit($limit);
        }elseif(!empty($limit) && !empty($offset)){
            $this->db->limit($limit, $offset);
        }
        if($group_by != "") {
            $this->db->group_by($group_by);
        }
        return $this->db->get()->result_array();
    }
    
    public function insert_row($table, $data) {
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    
    public function update_row($table, $data, $where) {
        $this->db->where($where);
        return $this->db->update($table, $data);
    }
    
    public function set_delete($table, $where) {
        $this->db->where($where)
        ->update($table, ['isDelete' => 1]);
        return $this->db->affected_rows();
    }

    public function checkExists($table,$where = '1=1'){
        if($this->db->select("*")->from($table)->where($where)->get()->num_rows() > 0){
            return true;
        }
        return false;
    }
    
    public function getStatename($id) {
        $result = "";
        $this->db->where('stateID', $id);
        $rowData = $this->db->get(TBL_STATE)->row();
        if(!empty($rowData)){
            $result = $rowData->state_name;
        } 
        return $result;
    }

    public function dateFormating($date) {
        $newformat = date('d-m-Y',strtotime($date));
        return $newformat;
    }

    // for generate membership ID
    public function generateMembershipID(){
        $membershipID = "NFS".rand(10000,99999);
        $checkLogin = $this->Service->get_row(TBL_CUSTOMER,'*', array('membershipID'=>$membershipID));
        if (!empty($checkLogin)) {
            $membershipID = $this->generateMembershipID();
        }
        return $membershipID;
    }

    public function getBranceName($managerID){
        $result = "Admin";
        $this->db->select('branch');
        $this->db->where('userID', $managerID);
        $rowData = $this->db->get(TBL_USERS)->row_array();
        if(!empty($rowData)){
            $result = $rowData['branch'];
        } 
        return ($result) ? $result :'Admin';
    }

    public function getTreatmentName($treatmentID){
        $result = "";
        $this->db->select('name');
        $this->db->where('treatmentID', $treatmentID);
        $rowData = $this->db->get(TBL_TREATMENT)->row_array();
        if(!empty($rowData)){
            $result = $rowData['name'];
        } 
        return ($result) ? $result :'';
    }

    public function getProductName($productID,$array =false){
        $result = "";
        $this->db->select('name');
        $this->db->where('isDelete', 0);
        if($array == true){
            $this->db->where_in(TBL_PRODUCT . '.productID', $productID);
            $resultData = $this->db->get(TBL_PRODUCT)->result_array();
        }else{
            $this->db->where('productID', $productID);
            $rowData = $this->db->get(TBL_PRODUCT)->row_array();
        }
        if(!empty($rowData)){
            $result = $rowData['name'];
        }elseif(!empty($resultData)){
            $result= $resultData;
        } 
        return ($result) ? $result :'';
    }

    public function getEmployeeName($employeeID,$array =false){
        $result = "";
        $this->db->select('name');
        $this->db->where('isDelete', 0);
        if($array == true){
            $this->db->where_in(TBL_EMPLOYEE . '.employeeID', $employeeID);
            $resultData = $this->db->get(TBL_EMPLOYEE)->result_array();
        }else{
            $this->db->where('employeeID', $employeeID);
            $rowData = $this->db->get(TBL_EMPLOYEE)->row_array();
        }
        if(!empty($rowData)){
            $result = $rowData['name'];
        }elseif(!empty($resultData)){
            $result= $resultData;
        } 
        return ($result) ? $result :'';
    }

    public function getMembershipinvoice($managerID,$date =''){
        $this->db->select('*');
        $this->db->where('managerID', $managerID);
        $this->db->where('isDelete', 0);
        $this->db->where('membershipID != ' , '');
        if(!empty($date)){
            $this->db->where('invoiceDate' , $date);
        }
        $resultData = $this->db->get(TBL_INVOICE)->result_array();
        return $resultData;
    }

    public function getAdminNumber(){
        $result = "";
        $this->db->select('mobile');
        $this->db->where('isDelete', 0);
        $this->db->where('isActive', 1);
        $this->db->where('userRole' , 3);
        $resultData = $this->db->get(TBL_USERS)->row_array();
        if(!empty($resultData['mobile']) && $resultData['mobile']!=""){
            $result = $resultData['mobile'];
        }
        return $result;
    }

    public function getRemainingTreatment($membershipID){
        $result = "0";
        $this->db->select('*');
        $this->db->where('isDelete', 0);
        $this->db->where('isActive' , 1);
        $this->db->where('membershipID', $membershipID);
        $customerData = $this->db->get(TBL_CUSTOMER)->row_array();

        $this->db->select('*');
        $this->db->where('isDelete', 0);
        $this->db->where('isActive' , 1);
        $this->db->where('membershipID' , $membershipID);
        $invoiceData = $this->db->get(TBL_INVOICE)->num_rows();
        $result = ($customerData['totalTreatment'] - $invoiceData);
        return $result;
    }

}
?>